﻿namespace CSICPR
{
    partial class Hoist_Checklist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.line = new System.Windows.Forms.Label();
        	this.lineicon = new System.Windows.Forms.Label();
        	this.HoistChecklist = new System.Windows.Forms.Label();
        	this.OperatorIniatial = new System.Windows.Forms.Label();
        	this.EquipmentLabel = new System.Windows.Forms.Label();
        	this.Equipment = new System.Windows.Forms.ComboBox();
        	this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
        	this.groupBox4 = new System.Windows.Forms.GroupBox();
        	this.ng4 = new System.Windows.Forms.RadioButton();
        	this.good4 = new System.Windows.Forms.RadioButton();
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.ng1 = new System.Windows.Forms.RadioButton();
        	this.good1 = new System.Windows.Forms.RadioButton();
        	this.groupBox2 = new System.Windows.Forms.GroupBox();
        	this.ng2 = new System.Windows.Forms.RadioButton();
        	this.good2 = new System.Windows.Forms.RadioButton();
        	this.groupBox3 = new System.Windows.Forms.GroupBox();
        	this.ng3 = new System.Windows.Forms.RadioButton();
        	this.good3 = new System.Windows.Forms.RadioButton();
        	this.groupBox5 = new System.Windows.Forms.GroupBox();
        	this.ng5 = new System.Windows.Forms.RadioButton();
        	this.good5 = new System.Windows.Forms.RadioButton();
        	this.groupBox6 = new System.Windows.Forms.GroupBox();
        	this.ng6 = new System.Windows.Forms.RadioButton();
        	this.good6 = new System.Windows.Forms.RadioButton();
        	this.groupBox7 = new System.Windows.Forms.GroupBox();
        	this.ng7 = new System.Windows.Forms.RadioButton();
        	this.good7 = new System.Windows.Forms.RadioButton();
        	this.Component = new System.Windows.Forms.Label();
        	this.Inspection = new System.Windows.Forms.Label();
        	this.Isgood = new System.Windows.Forms.Label();
        	this.Component1 = new System.Windows.Forms.Label();
        	this.Component2 = new System.Windows.Forms.Label();
        	this.Component3 = new System.Windows.Forms.Label();
        	this.Component4 = new System.Windows.Forms.Label();
        	this.Component5 = new System.Windows.Forms.Label();
        	this.Component6 = new System.Windows.Forms.Label();
        	this.Component7 = new System.Windows.Forms.Label();
        	this.Inspection1 = new System.Windows.Forms.Label();
        	this.Inspection2 = new System.Windows.Forms.Label();
        	this.Inspection3 = new System.Windows.Forms.Label();
        	this.Inspection4 = new System.Windows.Forms.Label();
        	this.Inspection5 = new System.Windows.Forms.Label();
        	this.Inspection6 = new System.Windows.Forms.Label();
        	this.Inspection7 = new System.Windows.Forms.Label();
        	this.initial = new System.Windows.Forms.ComboBox();
        	this.submit = new System.Windows.Forms.Button();
        	this.ShiftLabel = new System.Windows.Forms.Label();
        	this.Shift = new System.Windows.Forms.ComboBox();
        	this.commentsTB = new System.Windows.Forms.TextBox();
        	this.label1 = new System.Windows.Forms.Label();
        	this.tableLayoutPanel1.SuspendLayout();
        	this.groupBox4.SuspendLayout();
        	this.groupBox1.SuspendLayout();
        	this.groupBox2.SuspendLayout();
        	this.groupBox3.SuspendLayout();
        	this.groupBox5.SuspendLayout();
        	this.groupBox6.SuspendLayout();
        	this.groupBox7.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// line
        	// 
        	this.line.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.line.AutoSize = true;
        	this.line.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.line.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.line.Location = new System.Drawing.Point(175, 35);
        	this.line.Name = "line";
        	this.line.Size = new System.Drawing.Size(41, 39);
        	this.line.TabIndex = 8;
        	this.line.Text = "X";
        	// 
        	// lineicon
        	// 
        	this.lineicon.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.lineicon.AutoSize = true;
        	this.lineicon.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.lineicon.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.lineicon.Location = new System.Drawing.Point(57, 35);
        	this.lineicon.Name = "lineicon";
        	this.lineicon.Size = new System.Drawing.Size(107, 39);
        	this.lineicon.TabIndex = 7;
        	this.lineicon.Text = "Line :";
        	// 
        	// HoistChecklist
        	// 
        	this.HoistChecklist.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.HoistChecklist.AutoSize = true;
        	this.HoistChecklist.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.HoistChecklist.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.HoistChecklist.Location = new System.Drawing.Point(284, 35);
        	this.HoistChecklist.Name = "HoistChecklist";
        	this.HoistChecklist.Size = new System.Drawing.Size(435, 39);
        	this.HoistChecklist.TabIndex = 7;
        	this.HoistChecklist.Text = "Hoist Pre-Shift Inspection";
        	// 
        	// OperatorIniatial
        	// 
        	this.OperatorIniatial.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.OperatorIniatial.AutoSize = true;
        	this.OperatorIniatial.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.OperatorIniatial.ForeColor = System.Drawing.SystemColors.Control;
        	this.OperatorIniatial.Location = new System.Drawing.Point(24, 153);
        	this.OperatorIniatial.Name = "OperatorIniatial";
        	this.OperatorIniatial.Size = new System.Drawing.Size(229, 31);
        	this.OperatorIniatial.TabIndex = 11;
        	this.OperatorIniatial.Text = "Operator\'s Initial";
        	// 
        	// EquipmentLabel
        	// 
        	this.EquipmentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.EquipmentLabel.AutoSize = true;
        	this.EquipmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.EquipmentLabel.ForeColor = System.Drawing.SystemColors.Control;
        	this.EquipmentLabel.Location = new System.Drawing.Point(101, 89);
        	this.EquipmentLabel.Name = "EquipmentLabel";
        	this.EquipmentLabel.Size = new System.Drawing.Size(152, 31);
        	this.EquipmentLabel.TabIndex = 12;
        	this.EquipmentLabel.Text = "Equipment";
        	// 
        	// Equipment
        	// 
        	this.Equipment.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.Equipment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.Equipment.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Equipment.FormattingEnabled = true;
        	this.Equipment.Items.AddRange(new object[] {
			"First EVA",
			"2nd EVA",
			"Backsheet",
			"Maintenance",
			"End of Line"});
        	this.Equipment.Location = new System.Drawing.Point(264, 88);
        	this.Equipment.Name = "Equipment";
        	this.Equipment.Size = new System.Drawing.Size(202, 33);
        	this.Equipment.TabIndex = 9;
        	// 
        	// tableLayoutPanel1
        	// 
        	this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.tableLayoutPanel1.ColumnCount = 3;
        	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.64935F));
        	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.35065F));
        	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
        	this.tableLayoutPanel1.Controls.Add(this.groupBox4, 2, 4);
        	this.tableLayoutPanel1.Controls.Add(this.groupBox1, 2, 1);
        	this.tableLayoutPanel1.Controls.Add(this.groupBox2, 2, 2);
        	this.tableLayoutPanel1.Controls.Add(this.groupBox3, 2, 3);
        	this.tableLayoutPanel1.Controls.Add(this.groupBox5, 2, 5);
        	this.tableLayoutPanel1.Controls.Add(this.groupBox6, 2, 6);
        	this.tableLayoutPanel1.Controls.Add(this.groupBox7, 2, 7);
        	this.tableLayoutPanel1.Controls.Add(this.Component, 0, 0);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection, 1, 0);
        	this.tableLayoutPanel1.Controls.Add(this.Isgood, 2, 0);
        	this.tableLayoutPanel1.Controls.Add(this.Component1, 0, 1);
        	this.tableLayoutPanel1.Controls.Add(this.Component2, 0, 2);
        	this.tableLayoutPanel1.Controls.Add(this.Component3, 0, 3);
        	this.tableLayoutPanel1.Controls.Add(this.Component4, 0, 4);
        	this.tableLayoutPanel1.Controls.Add(this.Component5, 0, 5);
        	this.tableLayoutPanel1.Controls.Add(this.Component6, 0, 6);
        	this.tableLayoutPanel1.Controls.Add(this.Component7, 0, 7);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection1, 1, 1);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection2, 1, 2);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection3, 1, 3);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection4, 1, 4);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection5, 1, 5);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection6, 1, 6);
        	this.tableLayoutPanel1.Controls.Add(this.Inspection7, 1, 7);
        	this.tableLayoutPanel1.Location = new System.Drawing.Point(30, 218);
        	this.tableLayoutPanel1.Name = "tableLayoutPanel1";
        	this.tableLayoutPanel1.RowCount = 8;
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
        	this.tableLayoutPanel1.Size = new System.Drawing.Size(876, 305);
        	this.tableLayoutPanel1.TabIndex = 13;
        	this.tableLayoutPanel1.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tableLayoutPanel1_CellPaint);
        	// 
        	// groupBox4
        	// 
        	this.groupBox4.Controls.Add(this.ng4);
        	this.groupBox4.Controls.Add(this.good4);
        	this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
        	this.groupBox4.Location = new System.Drawing.Point(768, 159);
        	this.groupBox4.Name = "groupBox4";
        	this.groupBox4.Size = new System.Drawing.Size(98, 30);
        	this.groupBox4.TabIndex = 14;
        	this.groupBox4.TabStop = false;
        	this.groupBox4.Enter += new System.EventHandler(this.groupBox1_Enter);
        	// 
        	// ng4
        	// 
        	this.ng4.AutoSize = true;
        	this.ng4.Location = new System.Drawing.Point(54, 12);
        	this.ng4.Name = "ng4";
        	this.ng4.Size = new System.Drawing.Size(41, 17);
        	this.ng4.TabIndex = 1;
        	this.ng4.TabStop = true;
        	this.ng4.Text = "NG";
        	this.ng4.UseVisualStyleBackColor = true;
        	this.ng4.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// good4
        	// 
        	this.good4.AutoSize = true;
        	this.good4.Location = new System.Drawing.Point(6, 12);
        	this.good4.Name = "good4";
        	this.good4.Size = new System.Drawing.Size(51, 17);
        	this.good4.TabIndex = 0;
        	this.good4.TabStop = true;
        	this.good4.Text = "Good";
        	this.good4.UseVisualStyleBackColor = true;
        	this.good4.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Controls.Add(this.ng1);
        	this.groupBox1.Controls.Add(this.good1);
        	this.groupBox1.Cursor = System.Windows.Forms.Cursors.Hand;
        	this.groupBox1.Location = new System.Drawing.Point(768, 48);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Size = new System.Drawing.Size(98, 30);
        	this.groupBox1.TabIndex = 14;
        	this.groupBox1.TabStop = false;
        	this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
        	// 
        	// ng1
        	// 
        	this.ng1.AutoSize = true;
        	this.ng1.Location = new System.Drawing.Point(54, 12);
        	this.ng1.Name = "ng1";
        	this.ng1.Size = new System.Drawing.Size(41, 17);
        	this.ng1.TabIndex = 1;
        	this.ng1.TabStop = true;
        	this.ng1.Text = "NG";
        	this.ng1.UseVisualStyleBackColor = true;
        	this.ng1.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// good1
        	// 
        	this.good1.AutoSize = true;
        	this.good1.Location = new System.Drawing.Point(6, 12);
        	this.good1.Name = "good1";
        	this.good1.Size = new System.Drawing.Size(51, 17);
        	this.good1.TabIndex = 0;
        	this.good1.TabStop = true;
        	this.good1.Text = "Good";
        	this.good1.UseVisualStyleBackColor = true;
        	this.good1.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// groupBox2
        	// 
        	this.groupBox2.Controls.Add(this.ng2);
        	this.groupBox2.Controls.Add(this.good2);
        	this.groupBox2.Cursor = System.Windows.Forms.Cursors.Hand;
        	this.groupBox2.Location = new System.Drawing.Point(768, 85);
        	this.groupBox2.Name = "groupBox2";
        	this.groupBox2.Size = new System.Drawing.Size(98, 30);
        	this.groupBox2.TabIndex = 14;
        	this.groupBox2.TabStop = false;
        	this.groupBox2.Enter += new System.EventHandler(this.groupBox1_Enter);
        	// 
        	// ng2
        	// 
        	this.ng2.AutoSize = true;
        	this.ng2.Location = new System.Drawing.Point(54, 12);
        	this.ng2.Name = "ng2";
        	this.ng2.Size = new System.Drawing.Size(41, 17);
        	this.ng2.TabIndex = 1;
        	this.ng2.TabStop = true;
        	this.ng2.Text = "NG";
        	this.ng2.UseVisualStyleBackColor = true;
        	this.ng2.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// good2
        	// 
        	this.good2.AutoSize = true;
        	this.good2.Location = new System.Drawing.Point(6, 12);
        	this.good2.Name = "good2";
        	this.good2.Size = new System.Drawing.Size(51, 17);
        	this.good2.TabIndex = 0;
        	this.good2.TabStop = true;
        	this.good2.Text = "Good";
        	this.good2.UseVisualStyleBackColor = true;
        	this.good2.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// groupBox3
        	// 
        	this.groupBox3.Controls.Add(this.ng3);
        	this.groupBox3.Controls.Add(this.good3);
        	this.groupBox3.Cursor = System.Windows.Forms.Cursors.Hand;
        	this.groupBox3.Location = new System.Drawing.Point(768, 122);
        	this.groupBox3.Name = "groupBox3";
        	this.groupBox3.Size = new System.Drawing.Size(98, 30);
        	this.groupBox3.TabIndex = 14;
        	this.groupBox3.TabStop = false;
        	this.groupBox3.Enter += new System.EventHandler(this.groupBox1_Enter);
        	// 
        	// ng3
        	// 
        	this.ng3.AutoSize = true;
        	this.ng3.Location = new System.Drawing.Point(54, 12);
        	this.ng3.Name = "ng3";
        	this.ng3.Size = new System.Drawing.Size(41, 17);
        	this.ng3.TabIndex = 1;
        	this.ng3.TabStop = true;
        	this.ng3.Text = "NG";
        	this.ng3.UseVisualStyleBackColor = true;
        	this.ng3.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// good3
        	// 
        	this.good3.AutoSize = true;
        	this.good3.Location = new System.Drawing.Point(6, 12);
        	this.good3.Name = "good3";
        	this.good3.Size = new System.Drawing.Size(51, 17);
        	this.good3.TabIndex = 0;
        	this.good3.TabStop = true;
        	this.good3.Text = "Good";
        	this.good3.UseVisualStyleBackColor = true;
        	this.good3.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// groupBox5
        	// 
        	this.groupBox5.Controls.Add(this.ng5);
        	this.groupBox5.Controls.Add(this.good5);
        	this.groupBox5.Cursor = System.Windows.Forms.Cursors.Hand;
        	this.groupBox5.Location = new System.Drawing.Point(768, 196);
        	this.groupBox5.Name = "groupBox5";
        	this.groupBox5.Size = new System.Drawing.Size(98, 30);
        	this.groupBox5.TabIndex = 14;
        	this.groupBox5.TabStop = false;
        	this.groupBox5.Enter += new System.EventHandler(this.groupBox1_Enter);
        	// 
        	// ng5
        	// 
        	this.ng5.AutoSize = true;
        	this.ng5.Location = new System.Drawing.Point(54, 12);
        	this.ng5.Name = "ng5";
        	this.ng5.Size = new System.Drawing.Size(41, 17);
        	this.ng5.TabIndex = 1;
        	this.ng5.TabStop = true;
        	this.ng5.Text = "NG";
        	this.ng5.UseVisualStyleBackColor = true;
        	this.ng5.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// good5
        	// 
        	this.good5.AutoSize = true;
        	this.good5.Location = new System.Drawing.Point(6, 12);
        	this.good5.Name = "good5";
        	this.good5.Size = new System.Drawing.Size(51, 17);
        	this.good5.TabIndex = 0;
        	this.good5.TabStop = true;
        	this.good5.Text = "Good";
        	this.good5.UseVisualStyleBackColor = true;
        	this.good5.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// groupBox6
        	// 
        	this.groupBox6.Controls.Add(this.ng6);
        	this.groupBox6.Controls.Add(this.good6);
        	this.groupBox6.Cursor = System.Windows.Forms.Cursors.Hand;
        	this.groupBox6.Location = new System.Drawing.Point(768, 233);
        	this.groupBox6.Name = "groupBox6";
        	this.groupBox6.Size = new System.Drawing.Size(98, 30);
        	this.groupBox6.TabIndex = 14;
        	this.groupBox6.TabStop = false;
        	this.groupBox6.Enter += new System.EventHandler(this.groupBox1_Enter);
        	// 
        	// ng6
        	// 
        	this.ng6.AutoSize = true;
        	this.ng6.Location = new System.Drawing.Point(54, 12);
        	this.ng6.Name = "ng6";
        	this.ng6.Size = new System.Drawing.Size(41, 17);
        	this.ng6.TabIndex = 1;
        	this.ng6.TabStop = true;
        	this.ng6.Text = "NG";
        	this.ng6.UseVisualStyleBackColor = true;
        	this.ng6.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// good6
        	// 
        	this.good6.AutoSize = true;
        	this.good6.Location = new System.Drawing.Point(6, 12);
        	this.good6.Name = "good6";
        	this.good6.Size = new System.Drawing.Size(51, 17);
        	this.good6.TabIndex = 0;
        	this.good6.TabStop = true;
        	this.good6.Text = "Good";
        	this.good6.UseVisualStyleBackColor = true;
        	this.good6.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// groupBox7
        	// 
        	this.groupBox7.Controls.Add(this.ng7);
        	this.groupBox7.Controls.Add(this.good7);
        	this.groupBox7.Cursor = System.Windows.Forms.Cursors.Hand;
        	this.groupBox7.Location = new System.Drawing.Point(768, 270);
        	this.groupBox7.Name = "groupBox7";
        	this.groupBox7.Size = new System.Drawing.Size(98, 31);
        	this.groupBox7.TabIndex = 14;
        	this.groupBox7.TabStop = false;
        	this.groupBox7.Enter += new System.EventHandler(this.groupBox1_Enter);
        	// 
        	// ng7
        	// 
        	this.ng7.AutoSize = true;
        	this.ng7.Location = new System.Drawing.Point(54, 12);
        	this.ng7.Name = "ng7";
        	this.ng7.Size = new System.Drawing.Size(41, 17);
        	this.ng7.TabIndex = 1;
        	this.ng7.TabStop = true;
        	this.ng7.Text = "NG";
        	this.ng7.UseVisualStyleBackColor = true;
        	this.ng7.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// good7
        	// 
        	this.good7.AutoSize = true;
        	this.good7.Location = new System.Drawing.Point(6, 12);
        	this.good7.Name = "good7";
        	this.good7.Size = new System.Drawing.Size(51, 17);
        	this.good7.TabIndex = 0;
        	this.good7.TabStop = true;
        	this.good7.Text = "Good";
        	this.good7.UseVisualStyleBackColor = true;
        	this.good7.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
        	// 
        	// Component
        	// 
        	this.Component.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component.AutoSize = true;
        	this.Component.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component.ForeColor = System.Drawing.SystemColors.HotTrack;
        	this.Component.Location = new System.Drawing.Point(58, 10);
        	this.Component.Name = "Component";
        	this.Component.Size = new System.Drawing.Size(118, 24);
        	this.Component.TabIndex = 15;
        	this.Component.Text = "Component";
        	// 
        	// Inspection
        	// 
        	this.Inspection.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection.AutoSize = true;
        	this.Inspection.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection.ForeColor = System.Drawing.SystemColors.HotTrack;
        	this.Inspection.Location = new System.Drawing.Point(446, 10);
        	this.Inspection.Name = "Inspection";
        	this.Inspection.Size = new System.Drawing.Size(106, 24);
        	this.Inspection.TabIndex = 15;
        	this.Inspection.Text = "Inspection";
        	// 
        	// Isgood
        	// 
        	this.Isgood.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Isgood.AutoSize = true;
        	this.Isgood.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Isgood.ForeColor = System.Drawing.SystemColors.HotTrack;
        	this.Isgood.Location = new System.Drawing.Point(772, 10);
        	this.Isgood.Name = "Isgood";
        	this.Isgood.Size = new System.Drawing.Size(97, 24);
        	this.Isgood.TabIndex = 15;
        	this.Isgood.Text = "Good/NG";
        	// 
        	// Component1
        	// 
        	this.Component1.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component1.AutoSize = true;
        	this.Component1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component1.ForeColor = System.Drawing.SystemColors.Control;
        	this.Component1.Location = new System.Drawing.Point(39, 51);
        	this.Component1.Name = "Component1";
        	this.Component1.Size = new System.Drawing.Size(155, 24);
        	this.Component1.TabIndex = 15;
        	this.Component1.Text = "All components";
        	// 
        	// Component2
        	// 
        	this.Component2.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component2.AutoSize = true;
        	this.Component2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component2.ForeColor = System.Drawing.SystemColors.Control;
        	this.Component2.Location = new System.Drawing.Point(33, 88);
        	this.Component2.Name = "Component2";
        	this.Component2.Size = new System.Drawing.Size(168, 24);
        	this.Component2.TabIndex = 15;
        	this.Component2.Text = "Hoist equipment ";
        	// 
        	// Component3
        	// 
        	this.Component3.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component3.AutoSize = true;
        	this.Component3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component3.ForeColor = System.Drawing.SystemColors.Control;
        	this.Component3.Location = new System.Drawing.Point(82, 125);
        	this.Component3.Name = "Component3";
        	this.Component3.Size = new System.Drawing.Size(69, 24);
        	this.Component3.TabIndex = 15;
        	this.Component3.Text = "Hooks";
        	// 
        	// Component4
        	// 
        	this.Component4.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component4.AutoSize = true;
        	this.Component4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component4.ForeColor = System.Drawing.SystemColors.Control;
        	this.Component4.Location = new System.Drawing.Point(80, 162);
        	this.Component4.Name = "Component4";
        	this.Component4.Size = new System.Drawing.Size(74, 24);
        	this.Component4.TabIndex = 15;
        	this.Component4.Text = "Chains";
        	// 
        	// Component5
        	// 
        	this.Component5.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component5.AutoSize = true;
        	this.Component5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component5.ForeColor = System.Drawing.SystemColors.Control;
        	this.Component5.Location = new System.Drawing.Point(17, 199);
        	this.Component5.Name = "Component5";
        	this.Component5.Size = new System.Drawing.Size(199, 24);
        	this.Component5.TabIndex = 15;
        	this.Component5.Text = "End stops on bridge";
        	// 
        	// Component6
        	// 
        	this.Component6.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component6.AutoSize = true;
        	this.Component6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component6.ForeColor = System.Drawing.SystemColors.Control;
        	this.Component6.Location = new System.Drawing.Point(4, 236);
        	this.Component6.Name = "Component6";
        	this.Component6.Size = new System.Drawing.Size(225, 24);
        	this.Component6.TabIndex = 15;
        	this.Component6.Text = "Operating Mechanisms";
        	// 
        	// Component7
        	// 
        	this.Component7.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Component7.AutoSize = true;
        	this.Component7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Component7.ForeColor = System.Drawing.SystemColors.Control;
        	this.Component7.Location = new System.Drawing.Point(73, 274);
        	this.Component7.Name = "Component7";
        	this.Component7.Size = new System.Drawing.Size(87, 24);
        	this.Component7.TabIndex = 15;
        	this.Component7.Text = "Controls";
        	// 
        	// Inspection1
        	// 
        	this.Inspection1.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection1.AutoSize = true;
        	this.Inspection1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection1.ForeColor = System.Drawing.SystemColors.Control;
        	this.Inspection1.Location = new System.Drawing.Point(258, 51);
        	this.Inspection1.Name = "Inspection1";
        	this.Inspection1.Size = new System.Drawing.Size(482, 24);
        	this.Inspection1.TabIndex = 15;
        	this.Inspection1.Text = "Loose Objects Grease, oil or Contaminants on rails";
        	// 
        	// Inspection2
        	// 
        	this.Inspection2.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection2.AutoSize = true;
        	this.Inspection2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection2.ForeColor = System.Drawing.SystemColors.Control;
        	this.Inspection2.Location = new System.Drawing.Point(332, 88);
        	this.Inspection2.Name = "Inspection2";
        	this.Inspection2.Size = new System.Drawing.Size(334, 24);
        	this.Inspection2.TabIndex = 15;
        	this.Inspection2.Text = "Check guards (Visually – Present?)";
        	// 
        	// Inspection3
        	// 
        	this.Inspection3.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection3.AutoSize = true;
        	this.Inspection3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection3.ForeColor = System.Drawing.SystemColors.Control;
        	this.Inspection3.Location = new System.Drawing.Point(347, 125);
        	this.Inspection3.Name = "Inspection3";
        	this.Inspection3.Size = new System.Drawing.Size(304, 24);
        	this.Inspection3.TabIndex = 15;
        	this.Inspection3.Text = "Visual for deformation or cracks";
        	// 
        	// Inspection4
        	// 
        	this.Inspection4.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection4.AutoSize = true;
        	this.Inspection4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection4.ForeColor = System.Drawing.SystemColors.Control;
        	this.Inspection4.Location = new System.Drawing.Point(327, 162);
        	this.Inspection4.Name = "Inspection4";
        	this.Inspection4.Size = new System.Drawing.Size(345, 24);
        	this.Inspection4.TabIndex = 15;
        	this.Inspection4.Text = "Visual for wear, elongation and twist";
        	// 
        	// Inspection5
        	// 
        	this.Inspection5.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection5.AutoSize = true;
        	this.Inspection5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection5.ForeColor = System.Drawing.SystemColors.Control;
        	this.Inspection5.Location = new System.Drawing.Point(338, 199);
        	this.Inspection5.Name = "Inspection5";
        	this.Inspection5.Size = new System.Drawing.Size(322, 24);
        	this.Inspection5.TabIndex = 15;
        	this.Inspection5.Text = "Visual to ensure they are in place";
        	// 
        	// Inspection6
        	// 
        	this.Inspection6.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection6.AutoSize = true;
        	this.Inspection6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection6.ForeColor = System.Drawing.SystemColors.Control;
        	this.Inspection6.Location = new System.Drawing.Point(411, 236);
        	this.Inspection6.Name = "Inspection6";
        	this.Inspection6.Size = new System.Drawing.Size(177, 24);
        	this.Inspection6.TabIndex = 15;
        	this.Inspection6.Text = "Visual for function";
        	// 
        	// Inspection7
        	// 
        	this.Inspection7.Anchor = System.Windows.Forms.AnchorStyles.None;
        	this.Inspection7.AutoSize = true;
        	this.Inspection7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Inspection7.ForeColor = System.Drawing.SystemColors.Control;
        	this.Inspection7.Location = new System.Drawing.Point(389, 274);
        	this.Inspection7.Name = "Inspection7";
        	this.Inspection7.Size = new System.Drawing.Size(221, 24);
        	this.Inspection7.TabIndex = 15;
        	this.Inspection7.Text = "Push button operation ";
        	// 
        	// initial
        	// 
        	this.initial.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.initial.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.initial.FormattingEnabled = true;
        	this.initial.Location = new System.Drawing.Point(264, 153);
        	this.initial.Name = "initial";
        	this.initial.Size = new System.Drawing.Size(202, 33);
        	this.initial.TabIndex = 9;
        	this.initial.TextUpdate += new System.EventHandler(this.initial_TextUpdate);
        	// 
        	// submit
        	// 
        	this.submit.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.submit.Location = new System.Drawing.Point(375, 590);
        	this.submit.Name = "submit";
        	this.submit.Size = new System.Drawing.Size(134, 35);
        	this.submit.TabIndex = 14;
        	this.submit.Text = "SUBMIT";
        	this.submit.UseVisualStyleBackColor = true;
        	this.submit.Click += new System.EventHandler(this.submit_Click);
        	// 
        	// ShiftLabel
        	// 
        	this.ShiftLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.ShiftLabel.AutoSize = true;
        	this.ShiftLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.ShiftLabel.ForeColor = System.Drawing.SystemColors.Control;
        	this.ShiftLabel.Location = new System.Drawing.Point(512, 89);
        	this.ShiftLabel.Name = "ShiftLabel";
        	this.ShiftLabel.Size = new System.Drawing.Size(74, 31);
        	this.ShiftLabel.TabIndex = 15;
        	this.ShiftLabel.Text = "Shift";
        	// 
        	// Shift
        	// 
        	this.Shift.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.Shift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.Shift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Shift.FormattingEnabled = true;
        	this.Shift.Items.AddRange(new object[] {
			"D1",
			"N1",
			"D2",
			"N2"});
        	this.Shift.Location = new System.Drawing.Point(601, 88);
        	this.Shift.Name = "Shift";
        	this.Shift.Size = new System.Drawing.Size(102, 33);
        	this.Shift.TabIndex = 16;
        	this.Shift.SelectedIndexChanged += new System.EventHandler(this.ShiftSelectedIndexChanged);
        	// 
        	// commentsTB
        	// 
        	this.commentsTB.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.commentsTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.commentsTB.Location = new System.Drawing.Point(214, 546);
        	this.commentsTB.Name = "commentsTB";
        	this.commentsTB.Size = new System.Drawing.Size(457, 29);
        	this.commentsTB.TabIndex = 17;
        	// 
        	// label1
        	// 
        	this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
        	this.label1.AutoSize = true;
        	this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label1.ForeColor = System.Drawing.SystemColors.Control;
        	this.label1.Location = new System.Drawing.Point(30, 545);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(153, 31);
        	this.label1.TabIndex = 18;
        	this.label1.Text = "Comments";
        	// 
        	// Hoist_Checklist
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackColor = System.Drawing.SystemColors.ActiveBorder;
        	this.ClientSize = new System.Drawing.Size(946, 659);
        	this.Controls.Add(this.label1);
        	this.Controls.Add(this.commentsTB);
        	this.Controls.Add(this.Shift);
        	this.Controls.Add(this.ShiftLabel);
        	this.Controls.Add(this.submit);
        	this.Controls.Add(this.tableLayoutPanel1);
        	this.Controls.Add(this.OperatorIniatial);
        	this.Controls.Add(this.EquipmentLabel);
        	this.Controls.Add(this.initial);
        	this.Controls.Add(this.Equipment);
        	this.Controls.Add(this.line);
        	this.Controls.Add(this.HoistChecklist);
        	this.Controls.Add(this.lineicon);
        	this.Name = "Hoist_Checklist";
        	this.Text = "Hoist Checklist";
        	this.Load += new System.EventHandler(this.Hoist_Checklist_Load);
        	this.tableLayoutPanel1.ResumeLayout(false);
        	this.tableLayoutPanel1.PerformLayout();
        	this.groupBox4.ResumeLayout(false);
        	this.groupBox4.PerformLayout();
        	this.groupBox1.ResumeLayout(false);
        	this.groupBox1.PerformLayout();
        	this.groupBox2.ResumeLayout(false);
        	this.groupBox2.PerformLayout();
        	this.groupBox3.ResumeLayout(false);
        	this.groupBox3.PerformLayout();
        	this.groupBox5.ResumeLayout(false);
        	this.groupBox5.PerformLayout();
        	this.groupBox6.ResumeLayout(false);
        	this.groupBox6.PerformLayout();
        	this.groupBox7.ResumeLayout(false);
        	this.groupBox7.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label line;
        private System.Windows.Forms.Label lineicon;
        private System.Windows.Forms.Label HoistChecklist;
        private System.Windows.Forms.Label OperatorIniatial;
        private System.Windows.Forms.Label EquipmentLabel;
        private System.Windows.Forms.ComboBox Equipment;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RadioButton ng1;
        private System.Windows.Forms.RadioButton good1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton ng4;
        private System.Windows.Forms.RadioButton good4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton ng2;
        private System.Windows.Forms.RadioButton good2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton ng3;
        private System.Windows.Forms.RadioButton good3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton ng5;
        private System.Windows.Forms.RadioButton good5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton ng6;
        private System.Windows.Forms.RadioButton good6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton ng7;
        private System.Windows.Forms.RadioButton good7;
        private System.Windows.Forms.Label Component;
        private System.Windows.Forms.Label Inspection;
        private System.Windows.Forms.Label Isgood;
        private System.Windows.Forms.Label Component1;
        private System.Windows.Forms.Label Component2;
        private System.Windows.Forms.Label Component3;
        private System.Windows.Forms.Label Component4;
        private System.Windows.Forms.Label Component5;
        private System.Windows.Forms.Label Component6;
        private System.Windows.Forms.Label Component7;
        private System.Windows.Forms.Label Inspection1;
        private System.Windows.Forms.Label Inspection2;
        private System.Windows.Forms.Label Inspection3;
        private System.Windows.Forms.Label Inspection4;
        private System.Windows.Forms.Label Inspection5;
        private System.Windows.Forms.Label Inspection6;
        private System.Windows.Forms.Label Inspection7;
        private System.Windows.Forms.ComboBox initial;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Label ShiftLabel;
        private System.Windows.Forms.ComboBox Shift;
        private System.Windows.Forms.TextBox commentsTB;
        private System.Windows.Forms.Label label1;
    }
}
﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class EVA_BS_Rolls_Log : Form
    {
        public EVA_BS_Rolls_Log(string lne)
        {
            InitializeComponent();
            line.Text = lne;
        }

        private void EVA_BS_Rolls_Log_Load(object sender, EventArgs e)
        {
            // Check if the directory exists, if not it's created!  

            if (Directory.Exists("C:\\initial\\"))
            {
                //nothing  
            }
            else
            {
                Directory.CreateDirectory("C:\\initial\\");
            }

            // Check if the file exists, if not it's created!  

            if (File.Exists("C:\\initial\\initial.txt"))
            {
                //nothing  
            }
            else
            {
                File.Create("C:\\initial\\initial.txt");
            }


            StreamReader sr = new StreamReader("C:\\initial\\initial.txt", true);

            while (sr.Peek() >= 0)

            {
                initial.Items.Add(sr.ReadLine());
            }

            sr.Close();
        }
        private DataTable Executenonequery(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void submit_Click(object sender, EventArgs e)
        {
            submit.Enabled = false;
            //checks if it already is in the combobox
            if (!initial.Items.Contains(initial.Text.ToUpper()) && initial.Text != "")
            {
                //adds to combobox
                StreamWriter writor = new StreamWriter("C:\\initial\\initial.txt", true);
                writor.WriteLine(initial.Text.ToUpper());
                writor.Flush();
                writor.Dispose();
                initial.Items.Add(initial.Text.ToUpper());
            }
            else
            {
                //nothing
            }
            if (string.IsNullOrEmpty(batch.Text.ToString()) || string.IsNullOrEmpty(initial.Text.ToString()) || string.IsNullOrEmpty(roll.Text.ToString())||gettext[0]==""||gettext[1]=="")
            {
                MessageBox.Show("Some Field Is Blank.");
            }
            else
            {
                string sql = "Insert into [CAPA01DB01].[dbo].[Eva_Backsheet_Loading_Log] values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',getdate())";
                sql = string.Format(sql, line.Text.ToString(), gettext[0], gettext[1], batch.Text.ToString(), su.Text.ToString(), roll.Text.ToString(), initial.Text.ToString(), comments.Text.ToString());
                Executenonequery(sql);
                batch.Clear();
                su.Clear();
                roll.Clear();
                comments.Clear();
                MessageBox.Show("Done");
            }
            submit.Enabled = true;
        }
        public string[] gettext = { "", "" };
        private void radiobutton_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < 2; i++)
            {
                GroupBox[] gb = { Type, station};

                foreach (Control control in gb[i].Controls)
                {
                    if (control is RadioButton)
                    {
                        RadioButton radio = control as RadioButton;
                        if (radio.Checked)
                        {
                            gettext[i] = radio.Text;
                        }
                    }
                }


            }
        }

        private void comments_TextChanged(object sender, EventArgs e)
        {
            if (comments.Text.Count() > 50)
            {
                comments.Text = comments.Text.Substring(0, comments.Text.Length - 1);
                MessageBox.Show("Comments' Characters couldn't be greater than 50.");
            }
                
        }

        private void initial_TextUpdate(object sender, EventArgs e)
        {
            if (initial.Text.Count() > 3)
            {
                initial.Text = initial.Text.Substring(0, initial.Text.Length - 1);
                MessageBox.Show("Initial's Characters couldn't be greater than 3.");
            }
        }
    }
}

﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 10-03-16
 * Time: 18:00
 * 
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace CSICPR
{
	/// <summary>
	/// Description of ELDefects.
	/// </summary>
	public partial class ELDefects : Form
	{
		
		
		public ELDefects()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();			
			cbMod.SelectedIndex = 0;
			//populateComboBox();			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		private void populateComboBox()
		{
			const string sql = @"select [DefectCode] from [DefectMatchUp] where category = 1";
			
			dt1CB.Items.Add("");			
			DataTable dt = ToolsClass.PullData(sql);
			foreach(DataRow row in dt.Rows) {
				dt1CB.Items.Add(row[0]);
			}
			dt2CB.Items.Add("");
			foreach(DataRow row in dt.Rows) {
				dt2CB.Items.Add(row[0]);
			}
		}
		
		//--------------This is to ensure only one instance of this form is loaded
		private static ELDefects theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new ELDefects();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		void snTBTextChanged(object sender, EventArgs e)
		{
			tbLine.Clear();
			tbStr.Clear();
            string SN = "";
            if (string.IsNullOrEmpty(snTB.Text))
                return;



            if (snTB.Text.Trim().Substring(0, 1) == "e")
            {
                if (snTB.Text.Trim().Length < 22)
                    return;
                else
                {
                    SN = ToolsClass.CsiToFlexSN(snTB.Text.Trim());
                    tBFlexMatch.Text = snTB.Text;
                    snTB.Text = SN;
                }
                tBFlexMatch.Visible = true;
            }
            else if (snTB.Text.Trim().Substring(0, 1) == "G")
            {
                if (snTB.Text.Trim().Length < 21)
                    return;
                else
                {
                    SN = ToolsClass.CsiToGrape(snTB.Text.Trim());
                    tBFlexMatch.Text = snTB.Text;
                    snTB.Text = SN;
                }
                tBFlexMatch.Visible = true;
            }
            else
            {
                tBFlexMatch.Visible = false;
                SN = this.snTB.Text.Trim();
            }




            //search for stringer and line
            if (snTB.Text.Length > 3){
				findStringer(snTB.Text.Trim().ToString());
			}
		}
		
		private void snTB_KeyPress(object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == '\r')
			{
				dt1CB.Select();
				dt1CB.Focus();
			}
		}
		
		void findStringer(string sn){
			if(snTB.Text.Substring(0,1) != "S"){			
				string sql = "	SELECT TOP 1	pu.PU_Desc FROM dbo.Events e WITH (NOLOCK) JOIN	dbo.Prod_Units pu WITH (NOLOCK) " +
					"ON	e.PU_Id	= pu.PU_Id JOIN	dbo.Prod_Lines pl WITH (NOLOCK)	ON	pu.PL_Id = pl.PL_Id	WHERE e.Event_Num =	'" + sn + "' " +
				"AND pu.PU_Id >	0 AND e.TimeStamp <> ''	AND	pu.Equipment_Type = 'Stringer'";	
				SqlDataReader sdr = ToolsClass.GetDataReaderMES(sql);
				
				while(sdr != null && sdr.Read())
				{
					tbLine.Text = sdr.GetString(0).Substring(2,1).ToString();					
					tbStr.Text = sdr.GetString(0).Substring(20,1).ToString();
				}
			}
			else{
				tbLine.Text = snTB.Text.Substring(1,1).ToString();
				tbStr.Text = snTB.Text.Substring(2,2).ToString();
			}
			if(tbLine.Text == "" && snTB.Text.Length > 6){
                
				string testsd = snTB.Text.Substring(5,1).ToString();
                if (sn.Substring(0, 1) == "3")
                {
                    if (snTB.Text.Substring(5, 1).ToString() == "1")
                        tbLine.Text = "A";
                    else if (snTB.Text.Substring(5, 1).ToString() == "2")
                        tbLine.Text = "B";
                    else if (snTB.Text.Substring(5, 1).ToString() == "4")
                        tbLine.Text = "D";
                }
                else if (sn.Substring(0, 1) == "1")
                {
                    if (snTB.Text.Substring(5, 2).ToString() == "40" || snTB.Text.Substring(5, 2).ToString() == "41")
                        tbLine.Text = "A";
                    else if (snTB.Text.Substring(5, 2).ToString() == "42" || snTB.Text.Substring(5, 2).ToString() == "43")
                        tbLine.Text = "B";
                    else if (snTB.Text.Substring(5, 2).ToString() == "44" || snTB.Text.Substring(5, 2).ToString() == "45")
                        tbLine.Text = "NA";
                }

			}
		}
		
		void btClearClick(object sender, EventArgs e)
		{
			clearText();
		}
		
		void btSubmitClick(object sender, EventArgs e)
		{
			string busTable = "";
			string busEasy = "";
			string busHard = "";
			String sql = "";
			
			if(snTB.Text.Trim() != "" && dt1CB.Text.Trim() != "" && cbStr1.Text.Trim() != "" && tbCell1.Text.Trim() != "")
			{
				//UpdateDefectPanel(dt1CB, dnPanel1);
				//UpdateDefectPanel(dt2CB, dnPanel2);
				
				if(snTB.Text.Substring(0,1) != "S"){
					sql = "SELECT TOP 1 pu.PU_Desc FROM dbo.Events e WITH (NOLOCK) JOIN dbo.Prod_Units pu WITH (NOLOCK) " +
					"ON	e.PU_Id	= pu.PU_Id JOIN	dbo.Prod_Lines pl WITH (NOLOCK)	ON	pu.PL_Id = pl.PL_Id	WHERE e.Event_Num =	'" + snTB.Text.Trim() + "' " +
					"AND pu.PU_Id >	0 AND e.TimeStamp <> ''	AND	pu.Equipment_Type = 'Bussing' AND pl.PL_Desc = 'Bussing System'";
					SqlDataReader sdr = ToolsClass.GetDataReaderMES(sql);
				
					while(sdr != null && sdr.Read())
					{
						busTable = sdr.GetString(0).ToString();
					}
		
					sql = "SELECT TOP 1 s.Name FROM dbo.Events e WITH (NOLOCK) JOIN dbo.Prod_Units pu WITH (NOLOCK) " +
						"ON	e.PU_Id	= pu.PU_Id JOIN dbo.Tests t WITH (NOLOCK) ON e.Event_Id = t.Event_Id JOIN dbo.Variables v WITH (NOLOCK) ON t.Var_Id = v.Var_ID " +
						"JOIN dbo.Solderers s ON	s.id = t.result WHERE e.Event_Num =	'" + snTB.Text.Trim() + "' " +
					"AND pu.PU_Id >	0 AND e.TimeStamp <> ''	AND	v.Test_Name = 'OperatorID' AND pu.Equipment_Type = 'Bussing'";
					sdr = ToolsClass.GetDataReaderMES(sql);
					
					while(sdr != null && sdr.Read())
					{
						busEasy = sdr.GetString(0).ToString();
					}	
		
					sql = "SELECT TOP 1 s.Name FROM dbo.Events e WITH (NOLOCK) JOIN	dbo.Prod_Units pu WITH (NOLOCK) " +
						"ON	e.PU_Id	= pu.PU_Id JOIN dbo.Tests t WITH (NOLOCK) ON e.Event_Id = t.Event_Id JOIN dbo.Variables v WITH (NOLOCK) ON t.Var_Id = v.Var_ID " +
						"JOIN dbo.Solderers s ON	s.id = t.result WHERE e.Event_Num =	'" + snTB.Text.Trim() + "' " +
					"AND pu.PU_Id >	0 AND e.TimeStamp <> ''	AND	pu.Equipment_Type = 'Bussing' AND v.Test_Name = 'HSOperator'";
					sdr = ToolsClass.GetDataReaderMES(sql);
					
					while(sdr != null && sdr.Read())
					{
						busHard = sdr.GetString(0).ToString();
					}
				}
				
				insertSQL(dt1CB.Text.Trim(), cbStr1.Text.Trim(), cbStr2.Text.Trim(), tbCell1.Text.Trim(), tbCell2.Text.Trim(), busTable, busHard, busEasy);
				insertSQL(dt2CB.Text.Trim(), cbStr3.Text.Trim(), cbStr2.Text.Trim(), tbCell3.Text.Trim(), tbCell4.Text.Trim(), busTable, busHard, busEasy);
				insertSQL(dt3CB.Text.Trim(), cbStr5.Text.Trim(), cbStr6.Text.Trim(), tbCell5.Text.Trim(), tbCell6.Text.Trim(), busTable, busHard, busEasy);
				
				clearText();
				refreshTablesNew();
				
			}
			else				
				MessageBox.Show("Missing information, please try again.");
		}
		
		void insertSQL(String defect, String string1A, String string1B, string cell1A, string cell1B, String busTable, String busEasy, String busHard){
			
			if(defect != "" && string1A != "" && cell1A != "" && textBox1.Text == ""){
				String sql = "Insert into NPCDefects (SN, DefectFound, Defect1, String1A, String1B, cell1A, cell1B, Line, Stringer, BusTable, EasyOP, HardOP, Comments, DefectTime, ModType)" +
						"Values ('"+snTB.Text.Trim()+"', '"+"EL"+"','"+defect+"', '"+string1A+"', '"+string1B+
						"', '"+cell1A+"','"+cell1B+"', '"+tbLine.Text.Trim()+"', '"+tbStr.Text.Trim()+"', '"+busTable +
						"', '"+busEasy+"', '"+busHard+"', '"+textBox2.Text.Trim()+"',getdate(), '" + cbMod.Text.Trim() + "')";
				ToolsClass.ExecuteNonQuery(sql);
			}
			else if(defect != "" && string1A != "" && cell1A != ""){
				String sql = "Update NPCDefects Set SN = '"+snTB.Text.Trim()+"', Defect1 = '"+defect+"', String1A = '"+string1A+"'" +
					", String1B = '"+string1B+ "', cell1A = '"+cell1A+"', cell1B = '"+cell1B+"', Line = '"+tbLine.Text.Trim()+"', Stringer = '"+tbStr.Text.Trim()+"'" +
					", BusTable = '"+busTable + "', EasyOP = '"+busEasy+"', HardOP = '"+busHard+"', Comments = '"+textBox2.Text.Trim()+"', ModType = '" + 
					cbMod.Text.Trim() + "' Where id = '"+ textBox1.Text.Trim() + "'";
				ToolsClass.ExecuteNonQuery(sql);
			}
			
		}
		
		void clearText(){
			tbLine.Clear();
			tbStr.Clear();
			snTB.Clear();
			cbDefectF.SelectedItem = null;
			cbStr1.SelectedItem = null;
			cbStr2.SelectedItem = null;
			cbStr3.SelectedItem = null;
			cbStr4.SelectedItem = null;			
			cbStr5.SelectedItem = null;
			cbStr6.SelectedItem = null;
			tbCell1.Text = "";
			tbCell2.Text = "";
			tbCell3.Text = "";
			tbCell4.Text = "";
			tbCell5.Text = "";
			tbCell6.Text = "";
			dt1CB.SelectedItem = null;
			dt2CB.SelectedItem = null;			
			dt3CB.SelectedItem = null;
			textBox2.Clear();
			textBox1.Clear();
			dnPanel1.Text = "";			
			dnPanel2.Text = "";
		}
		void Dt1CBSelectedIndexChanged(object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == '\r' || e.KeyChar == '\t'){
				string DefectName = "";
				string sql = "Select [DefectName] from [DefectMatchUp] where category = 1 and defectcode ='"+dt1CB.Text.ToString()+"'";
				SqlDataReader rd= ToolsClass.GetDataReader(sql);
				if(rd.Read() && rd!=null)
				{
					DefectName = rd.GetString(0);
				}
				dnPanel1.Text = DefectName;
				cbStr1.Select();
				cbStr1.Focus();
			}
		}
		void Dt2CBSelectedIndexChanged(object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == '\r' || e.KeyChar == '\t'){
				string DefectName = "";
				string sql = "Select [DefectName] from [DefectMatchUp] where category = 1 and defectcode ='"+dt2CB.Text.ToString()+"'";
				SqlDataReader rd= ToolsClass.GetDataReader(sql);
				if(rd.Read() && rd!=null)
				{
					DefectName = rd.GetString(0);
				}
				dnPanel2.Text = DefectName;
				cbStr2.Select();
				cbStr2.Focus();
			}
		}
		
		void UpdateDefectPanel(ComboBox defect, Label defectPanel){
			string DefectName = "";
			string sql = "Select [DefectName] from [DefectMatchUp] where category = 1 and defectcode ='"+defect.Text.ToString()+"'";
			SqlDataReader rd= ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd!=null)
			{
				DefectName = rd.GetString(0);
			}
			defectPanel.Text = DefectName;
		}
		
		void BtRefreshClick(object sender, EventArgs e)
		{
			refreshTablesNew();
		}

		void refreshTables(){
			#region SQL Query datagridview1
			string sql = @"Select DefectHour As Hour,
							[Busbar Showing],
							[Change Back Sheet EVA],
							[Cracked/Chipped Cell Handling],
							[Cracked/Chipped Cell Machine],
							[Dark Spot],
							[Flux],
							[Dead Cell],
							[Dirty Square or Strip],
							[String to String Gap],
							[Cell to Cell Gap],
							[Glass Defect],
							[Hair],
							[Half Image],
							[Low Efficiency],
							[Missing or Extra Barcode],
							[Missing Ribbon],
							[No Image],
							[Other],
							[Other Machine Failure],
							[Paper],
							[Piece of Cell],
							[Piece of Ribbon],
							[Polarity],
							[Reject No Repair],
							[Ribbon Misalignment],
							[Offset Insulator Strip],
							[Unsoldered Busbar Easy Side],
							[Unsoldered Busbar Hard Side]
							From(
								Select Defect1, DATEPART(hh, DefectTime) as ordering, Cast(Cast((DATEPART(hh, DefectTime)) as nvarchar) + ' - ' + Cast((DATEPART(hh, DefectTime)+1) As nvarchar) As nvarchar) As DefectHour,
								Cast(DefectTime as Date) as DD, DATEPART(hh, DefectTime) As DH from NPCDefects
								Where DefectTime > DateAdd(hh, - 12, Getdate()) and DefectFound = 'EL' and line = '" + cbLine.Text.ToString() + @"' 								
								) tb1 
								Pivot 
								( 
									count(Defect1) for Defect1 in 
									([Busbar Showing],
									[Change Back Sheet EVA],
									[Cracked/Chipped Cell Handling],
									[Cracked/Chipped Cell Machine],
									[Dark Spot],
									[Flux],
									[Dead Cell],
									[Dirty Square or Strip],
									[String to String Gap],
									[Cell to Cell Gap],
									[Glass Defect],
									[Hair],
									[Half Image],
									[Low Efficiency],
									[Missing or Extra Barcode],
									[Missing Ribbon],
									[No Image],
									[Other],
									[Other Machine Failure],
									[Paper],
									[Piece of Cell],
									[Piece of Ribbon],
									[Polarity],
									[Reject No Repair],
									[Ribbon Misalignment],
									[Offset Insulator Strip],
									[Unsoldered Busbar Easy Side],
									[Unsoldered Busbar Hard Side]							 							 
									) 
								) piv order by DD Desc, DH Desc;";
			#endregion
			
			DataTable dt = ToolsClass.GetDataTable(sql);
			
			//SqlDataReader reader = ToolsClass.GetDataReader(sql);
			if(dt != null && dt.Rows.Count > 0)
			{
				dataGridView1.DataSource = dt;
				//dataGridView1.Rows.Insert(dataGridView1.RowCount, new object[] { reader.GetValue(0), reader.GetValue(1), reader.GetValue(2), reader.GetValue(3), reader.GetValue(4), reader.GetValue(5)});
			}
			
			int i = 0;
			int num = 0;
			
			while(i < dataGridView1.Columns.Count){
				dataGridView1.Columns[i].Width = 70;
				i++;
			}
			
			foreach(DataGridViewRow row in dataGridView1.Rows)
			{
			    foreach(DataGridViewCell cell in row.Cells)
			    {
			    	bool result = Int32.TryParse(cell.Value.ToString(), out num);
			    	if(num > 0 && result)
			    		cell.Style.BackColor = Color.Yellow;			    	
			    }
			}
			
			#region SQL Query datagridview1
			sql = @"Select Top 5 ID, SN, Line, Stringer, Defect1, String1A, String1B, Cell1A, Cell1B, BusTable, EasyOP, HardOP, ModType, DefectTime From NPCDefects Where Line = '" + cbLine2.Text.Trim() + "' Order by ID Desc";
			#endregion			
			dt = ToolsClass.GetDataTable(sql);
			if(dt != null && dt.Rows.Count > 0)
			{
				dataGridView2.DataSource = dt;				
			}
			
			if(dataGridView2.Columns.Contains("Modify Entry"))
				dataGridView2.Columns.Remove("Modify Entry");
			var col = new DataGridViewButtonColumn();
			col.UseColumnTextForButtonValue = true;
			col.Text = "Modify";
			col.Name = "Modify Entry";
			dataGridView2.Columns.Add(col);
			
		}
		
		void refreshTablesNew(){
			string sql = @" Select top 5 Line, DefectFound, Defect1, count(SN) as Qty from NPCDefects 
							Where DefectTime > DateAdd(hh, - 12, Getdate()) and DefectFound = 'EL' and line = '" + cbLine.Text.ToString() + @"'
							Group By Defect1, Line, DefectFound
							Order By count(SN) Desc";
			
			sql = @"Select top 5 Line, DefectFound, Defect1, count(SN) as Qty from NPCDefects 
					Where DefectTime >= DateAdd(hh, - 12, Getdate()) And (Cast(DefectTime as Time) Between 
					Case When CAST(GetDate() as Time) Between '7:00:00'  And '19:00:00' Then '7:00:00'
					Else '19:00:00' End
					And 
					Case When CAST(GetDate() as Time) Between '07:00:00'  And '19:00:00' Then '19:00:00'
					Else '23:59:59' End
					OR Cast(DefectTime as Time) Between 
					CASE WHEN CAST(GetDate() as Time) < '07:00:00' Then '0:00:00' End And '07:00:00') 
					And DefectFound = 'EL' and line = '" + cbLine.Text.ToString() + @"'
					Group By Defect1, Line, DefectFound
					Order By count(SN) Desc";
			
			DataTable dt = ToolsClass.GetDataTable(sql);
			
			//SqlDataReader reader = ToolsClass.GetDataReader(sql);
			if(dt != null && dt.Rows.Count > 0)
			{
				dataGridView1.DataSource = dt;
				//dataGridView1.Rows.Insert(dataGridView1.RowCount, new object[] { reader.GetValue(0), reader.GetValue(1), reader.GetValue(2), reader.GetValue(3), reader.GetValue(4), reader.GetValue(5)});
			}
			
			int i = 0;
			int num = 0;
			
			while(i < dataGridView1.Columns.Count){
				dataGridView1.Columns[i].Width = 80;
				i++;
			}
			
			dataGridView1.Columns[2].Width = 200;
			
			foreach(DataGridViewRow row in dataGridView1.Rows)
			{
			    foreach(DataGridViewCell cell in row.Cells)
			    {
			    	bool result = Int32.TryParse(cell.Value.ToString(), out num);
			    	if(num > 0 && result)
			    		cell.Style.BackColor = Color.Yellow;			    	
			    }
			}
			
			#region SQL Query datagridview1
			sql = @"Select Top 5 ID, SN, Line, Stringer, Defect1, String1A, String1B, Cell1A, Cell1B, BusTable, EasyOP, HardOP, ModType, DefectTime
					From NPCDefects Where Line = '" + cbLine2.Text.Trim() + "' Order by ID Desc";
			#endregion			
			dt = ToolsClass.GetDataTable(sql);
			if(dt != null && dt.Rows.Count > 0)
			{
				dataGridView2.DataSource = dt;				
			}
			
			if(dataGridView2.Columns.Contains("Modify Entry"))
				dataGridView2.Columns.Remove("Modify Entry");
			var col = new DataGridViewButtonColumn();
			col.UseColumnTextForButtonValue = true;
			col.Text = "Modify";
			col.Name = "Modify Entry";
			dataGridView2.Columns.Add(col);	
			
		}
		void BtRefresh2Click(object sender, EventArgs e)
		{
			refreshTablesNew();
		}
		void CbLine2SelectedIndexChanged(object sender, EventArgs e)
		{
			cbLine.SelectedIndex = cbLine2.SelectedIndex;
		}
		void CbLineSelectedIndexChanged(object sender, EventArgs e)
		{
			cbLine2.SelectedIndex = cbLine.SelectedIndex;
		}
		private void DataGridView2_CellClick(object sender, DataGridViewCellEventArgs e) {
			if (dataGridView2.Columns[e.ColumnIndex].Name == "Modify Entry") 
		    { 
		        DialogResult dialogResult = MessageBox.Show("Are you sure you want to modify this entry?", "Modify", MessageBoxButtons.YesNo);
				if(dialogResult == DialogResult.Yes)
				{
					textBox1.Text = dataGridView2[0, e.RowIndex].Value.ToString();
					snTB.Text = dataGridView2[1, e.RowIndex].Value.ToString();
					dt1CB.Text = dataGridView2[4, e.RowIndex].Value.ToString();
					cbStr1.Text = dataGridView2[5, e.RowIndex].Value.ToString();
					cbStr2.Text = dataGridView2[6, e.RowIndex].Value.ToString();
					tbCell1.Text = dataGridView2[7, e.RowIndex].Value.ToString();
					tbCell2.Text = dataGridView2[8, e.RowIndex].Value.ToString();
					cbMod.Text = dataGridView2[12, e.RowIndex].Value.ToString();
				}
		    } 
		}

        private void dt1CB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

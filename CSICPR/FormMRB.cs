﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 09/11/2015
 * Time: 3:35 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace CSICPR
{
	public partial class FormMRB : Form
	{
		public FormMRB()
		{
			InitializeComponent();
		}		

		
		//--------------This is to ensure only one instance of this form is loaded
		private static FormMRB theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormMRB();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		#region Supporting Functions
		private void Clear_fields()
		{
			textBox1.Clear();           
            textBox2.Clear();
            textBox3.Clear();           
            textBox4.Clear();
            textBox5.Clear();
            stringCB1.SelectedItem = null;
            stringCB2.SelectedItem = null;
            stringCB3.SelectedItem = null;
            celltb1.Clear();
            celltb2.Clear();
            celltb3.Clear();
		
            comboBox1.SelectedItem = null;
            comboBox2.SelectedItem = null;
            comboBox3.SelectedItem = null;
            comboBox4.SelectedItem = null;
            comboBox5.SelectedItem = null;
            comboBox6.SelectedItem = null;
            comboBox7.SelectedItem = null;
            
            dataGridView1.Rows.Clear();
		}
		void ComboBox3SelectedIndexChanged(object sender, EventArgs e)
		{
			Populate_Defects();
		}
		void TextBox1TextChanged(object sender, EventArgs e)		
        {
			textBox3.Clear();
            string SN = "";
            if (textBox1.Text.Trim().Length < 1)
                return;
            if (textBox1.Text.Trim().Substring(0, 1) == "e")
            {
                if (textBox1.Text.Trim().Length < 22)
                    return;
                else
                {
                    SN = ToolsClass.CsiToFlexSN(textBox1.Text.Trim());
                    tBFlexMatch.Text = textBox1.Text;
                    textBox1.Text = SN;
                }
                tBFlexMatch.Visible = true;
            }
            else if (textBox1.Text.Trim().Substring(0, 1) == "G")
            {
                if (textBox1.Text.Trim().Length < 21)
                    return;
                else
                {
                    SN = ToolsClass.CsiToGrape(textBox1.Text.Trim());
                    tBFlexMatch.Text = textBox1.Text;
                    textBox1.Text = SN;
                }
                tBFlexMatch.Visible = true;
            }
            else
            {
                tBFlexMatch.Visible = false;
                SN = this.textBox1.Text.Trim();
            }
			    

			if(SN.Length>=2&&SN!=""&&SN!=null)
			CalculateLine(SN);
		}

		#endregion
		
        string GetOtherSN(string sn)
        {
            string sn_return = "";
            if (sn.Substring(0, 1) == "e")
            {
                if (sn.Trim().Length < 22)
                    return "";
               
                    sn_return = ToolsClass.CsiToFlexSN(sn);            
                
               
            }
            else if (sn.Substring(0, 1) == "G")
            {
                if (sn.Trim().Length < 21)
                    return "";

                    sn_return = ToolsClass.CsiToGrape(sn);                 
         
            }
            if (string.IsNullOrEmpty(sn_return))
                sn_return = "";

            return sn_return;
        }


		#region Verify Entries
		
		private bool VerifyAll()
		{
			if(this.textBox1.Text.Trim() == "")
			{
				MessageBox.Show("please fill Serial number");
				return false;
			}


            else if( !VerifyAllOtherEntries())
				return false;
			else
				return true;
			
		}

        private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            string connection = ToolsClass.NEWPACKINGConnStr();
            sql = string.Format(sql, sSerialNumber);
            SqlDataReader sdr = ToolsClass.GetDataReader(sql, connection);
            if (sdr != null && sdr.Read())
            {
                return false;
            }
            sdr.Close();
            sdr = null;
            return true;
        }

        private bool VerifyAllOtherEntries()
		{
			//MessageBox.Show("textbox1.text.trim is"+this.textBox1.Text.Trim()+"      "+"textbox1.Text.trim is:"+this.textBox1.ToString().Trim());
			if(this.textBox5.Text.Trim() == "")
			{
				MessageBox.Show("please fill Picture");
				return false;
			}
            Int32 i;
            if (!Int32.TryParse(this.textBox5.Text.Trim(), out i))
            {
                MessageBox.Show("please fill Picture With Int");
                return false;
            }
            if ( this.comboBox1.Text.Trim()==""||this.comboBox2.Text.Trim()==""||this.comboBox3.Text.Trim()==""||this.comboBox4.Text.Trim()==""||
                this.comboBox5.Text.Trim()==""||this.comboBox6.Text.Trim()==""||this.comboBox7.Text.Trim()==""||this.stringCB1.Text.Trim()==""||
                this.celltb1.Text.Trim()=="")
			{
				MessageBox.Show("please Select from the DropBox");
				return false;
			}
			else
				return true;
		}
		private bool verifyGroupEntry()
		{
			if(this.textBox4.Text.Trim() == "")
			{
				MessageBox.Show("please fill in the Batch Grade entry");
				return false;
			}
			else if (VerifyAllOtherEntries() == false)
				return false ;
			else
				return true;
		}
		#endregion
		
		#region FunctionMRB
		private void CalculateLine(string Temp)
		{
			int temp=0;
            textBox3.Clear();
		
			if(Temp == "")
			{
				MessageBox.Show(" invaild serial number");
				return;
			}
			if(Temp.Length > 6){
				temp = Temp[5]-'0';
				//MessageBox.Show(" this number is:"+ temp+"          "+"this Temp is :" + Temp+"          "+"this T8 is :"+ T8);
				if ( Temp[0] == '3')
                {
                    if (temp == 1)
                    {
                        this.textBox3.Text += "A";
                    }
                    else if (temp == 2)
                    {
                        this.textBox3.Text += "B";
                    }
                    else if (temp == 4)
                    {
                        this.textBox3.Text += "D";
                    }
                    else
                        this.textBox3.Text += "N";
                }
                else
                {
                    temp = Temp[6] - '0';
                    if (temp == 0 || temp == 1)
                    {
                        this.textBox3.Text += "A";
                    }
                    else if (temp == 2 || temp == 3)
                    {
                        this.textBox3.Text += "B";
                    }
                    else if (temp == 4) 
                    {
                        this.textBox3.Text += "D";// or Line C
                    }
                    else
                        this.textBox3.Text += "N";
                }
				
			}
			return;
			
		}
		private void Loop_Through(string BGrade)
		{
		
			string[] B_Grade= BGrade.Split('\n');
			for(int i=0; i<B_Grade.Length; i++)
			{
				B_Grade[i] = B_Grade[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
				if (B_Grade[i] != null && B_Grade[i] != "")
				Grade(B_Grade[i]);
			}
		}
		private void Loop_Through_Histroy(string BGrade)
		{
		
			string[] B_Grade= BGrade.Split('\n');
			for(int i=0; i<B_Grade.Length; i++)
			{
				B_Grade[i] = B_Grade[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
				if (B_Grade[i] != null && B_Grade[i] != "")
				History_Check(B_Grade[i]);
			}
		}
			private void Grade(string SN_Grade)
		{


            if (!isPacked(SN_Grade.Trim()))
            {
                MessageBox.Show("Module " + SN_Grade + " has been packaged, its class cannot be changed!");
                return ;
            }



            if (SN_Grade.Length > 14)
                SN_Grade = GetOtherSN(SN_Grade);

            if (SN_Grade.Length < 14)
                return;
            #region initializing
            if (string.IsNullOrEmpty(textBox3.Text.Trim()))								
				CalculateLine(SN_Grade);

                

                string Remark= this.textBox2.Text.ToString();
			
				char Line= this.textBox3.Text.Trim()[0];
				string Serial_Numbers= this.textBox4.Text.ToString();
				string Picture= this.textBox5.Text.ToString();
				
				string Class= this.comboBox1.Text.ToString();
				string MRB= this.comboBox2.Text.ToString();
				string Defect_Category=this.comboBox3.Text.ToString();
				string Stage= this.comboBox4.Text.ToString();
				string Specific_Defect=this.comboBox5.Text.ToString();
				string Model=this.comboBox6.Text.ToString();
				string Shifts=this.comboBox7.Text.ToString();
				string String_1=this.stringCB1.Text.ToString();
				string Cell_1 = this.celltb1.Text.ToString();
				string Cell_2 =""; 
				if (!string.IsNullOrEmpty(celltb2.Text.ToString()))
					Cell_2=celltb2.Text.ToString();
				string String_2="";
				if (!string.IsNullOrEmpty(stringCB2.Text.ToString()))
					String_2=stringCB2.Text.ToString();
				string Cell_3 =""; 
				if (!string.IsNullOrEmpty(celltb3.Text.ToString()))
					Cell_3=celltb3.Text.ToString();
				string String_3="";
				if (!string.IsNullOrEmpty(stringCB3.Text.ToString()))
					String_3=stringCB3.Text.ToString();				
				
				#endregion
				string defect_code = "";
				string sql = "insert into MRBData (SN, Line, Class, Model, Shifts, MRB, String_1, String_2, String_3, " +
					"Cell_1, Cell_2, Cell_3, Stage, Defect_Category, Specific_Defect, Picture, Remarks, Timestamp, Operator) values " +
					"('"+SN_Grade+"', '"+Line+"','"+Class+"', '"+Model+"', '"+Shifts+"', '"+MRB+"','"+String_1+"', '"+String_2+"', '" +String_3+"', '"
					+Cell_1+"', '"+Cell_2+"','"+Cell_3+"','"+Stage+"', '"+Defect_Category+"', '"+Specific_Defect+"', '"
					+Picture+"', '"+Remark+"',getdate(),'"+FormCover.CurrUserWorkID+"')";
				ToolsClass.ExecuteNonQuery(sql);
				sql = "insert into QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, DefectCategory , remark) values ('"+SN_Grade+"', '"+Class+"', getdate(), '"+FormCover.CurrUserWorkID+"','"+defect_code+"','" + Defect_Category + "','" +Remark+"')";
				
				ToolsClass.ExecuteNonQuery(sql);
		}

		private void Check_SN(String SN_Check)
		{
			Clear_fields();
				textBox1.Text= SN_Check;
				
			string sql = "select Line, Class, Model,Shifts, MRB, String_1, String_2, String_3, Cell_1, Cell_2, Cell_3, Stage, Defect_Category, Specific_Defect, Picture, Remarks from (select row_number() over ( partition by SN order by Timestamp desc) as RN, * from MRBData where SN = '"+SN_Check+"' )as temp where Rn = '1'";
			//						0	1		2		3	4		5		6			7		8		9		10		11		12				13				 14		    15		
			//string sql = "select Line, Class, Model,Shifts, MRB, String_1, String_2, Cell_1, Cell_2, Stage, Defect_Category, Specific_Defect, Location_1, Location_2, Picture, Remarks from MRBData where SN  = '"+SN_Check+"'";
			SqlDataReader sdr = ToolsClass.GetDataReader(sql);
			if(sdr != null && sdr.Read())
			{
				//TextBox
				textBox2.Text += sdr.GetString(15);
				//textBox3.Text += sdr.GetString(0);
				textBox5.Text += sdr.GetValue(14);
				//ComboBox
				comboBox1.Text += sdr.GetString(1);
				comboBox2.Text += sdr.GetString(4);
				comboBox3.Text += sdr.GetString(12);
				comboBox4.Text += sdr.GetString(11);
				comboBox5.Text += sdr.GetString(13);
				comboBox6.Text += sdr.GetString(2);
				comboBox7.Text += sdr.GetString(3);
				stringCB1.Text += sdr.GetString(5);
				stringCB2.Text += sdr.GetValue(6);
				stringCB3.Text += sdr.GetValue(7);
				celltb1.Text += sdr.GetString(8);
				celltb2.Text += sdr.GetString(9);
				celltb3.Text += sdr.GetString(10);
			}
			sdr.Close();
			sdr=null;
		}
		private string Duplication_Check(string TextBox4_Text)
		{
			string[] B_Grade= TextBox4_Text.Split('\n');
			string Copy_Over="";
				
			
				for( int i=0; i<B_Grade.Length;i++)
				{					
				if (B_Grade[i] != null && B_Grade[i] != "")
					B_Grade[i] = B_Grade[i].Trim();
					
					bool is_duplication = false;
					for ( int j=0; j< i; j++)
					{
						if(string.Equals(B_Grade[i],B_Grade[j]))
							is_duplication=true;
					}
					
					if(!is_duplication)
						Copy_Over=Copy_Over+B_Grade[i]+'\n';
					
				}
				
				return Copy_Over;
			
		}
		private void History_Check(String SN_Check)
		{
		 	
		 	
			string sql = "select ModuleClass, Remark, Timestamp from QualityJudge where SN = '"+SN_Check+"' order by Timestamp desc";
			SqlDataReader sdr = ToolsClass.GetDataReader(sql);
			
			string MClass = "";
			string Tstamp = "";
			string Rmark = "";
			int row= 0;
			
			while(sdr != null && sdr.Read())
			{
			
				MClass = sdr.GetString(0);
				Rmark = Convert.ToString(sdr.GetSqlValue(1));
				Tstamp =Convert.ToString(sdr.GetSqlValue(2));//(sdr.GetDateTime(2));
				bool IsDuplication = false;
				for (int i = 0; i < dataGridView1.Rows.Count-1;i++)
				{
					if(string.Compare(SN_Check,dataGridView1.Rows[i].Cells[0].Value.ToString(),false)== 0 &&string.Compare(MClass,dataGridView1.Rows[i].Cells[1].Value.ToString(),false)== 0 && string.Compare(Tstamp,dataGridView1.Rows[i].Cells[3].Value.ToString(),false)== 0 &&string.Compare(Rmark,dataGridView1.Rows[i].Cells[2].Value.ToString(),false)== 0 )
						IsDuplication = true;
					
				}
				
				if(!IsDuplication){
				dataGridView1.Rows.Add(SN_Check, MClass,Rmark,Tstamp);
				row++;}
				
			}
			sdr.Close();
			sdr=null;
		}
		#endregion
				
		#region Color Change	
		private void Color_Change(object sender, System.EventArgs e)
		{
			((Control)sender).BackColor= Color.Yellow;
		}
			private void Color__Change(object sender, System.EventArgs e)
		{
				((Control)sender).BackColor= Color.Violet;
				
				if((((Control)sender).Text == null)||(((Control)sender).Text == ""))
				((Control)sender).BackColor =Color.White;	
					
				
		}
		private void Color_Back(object sender, System.EventArgs e)
		{
			((Control)sender).BackColor= Color.White;
		}
	
		#endregion
		
		#region Buttons Click
		
		
		
			
		
		void Button2Click(object sender, EventArgs e) // check
		{
			
			string SN= this.textBox1.Text.Trim();
			if (!string.IsNullOrEmpty(SN)) {				
				if (!ToolsClass.GradeIfBFClass(SN))
					return;
				Check_SN(SN);
			}
		}
		
		void Button3Click(object sender, EventArgs e)// Grade
		{		
			bool is_verified=VerifyAll();					
			if(is_verified)
			{
				string SN= this.textBox1.Text.Trim();
				if (!ToolsClass.GradeIfBFClass(SN))
					return;	
				Grade(SN);
				Clear_fields();
			}
			//MessageBox.Show("is_verified is"+is_verified);
		}
		
		void Button4Click(object sender, EventArgs e)//Batch Grade
		{
			
			bool is_verified=verifyGroupEntry();
		
			if(is_verified)
			{
				string SN = Duplication_Check(textBox4.Text);
				CalculateLine(SN);
				Loop_Through(SN);
				Clear_fields();
			}
			
		}
		
		void Button5Click(object sender, EventArgs e)
		{
			
			if(textBox4.Text!="" && textBox4.Text!= null)
			{
				string SN= Duplication_Check(textBox4.Text);
				
				//Clear_fields();
				Loop_Through_Histroy(SN);
				return;
			}
			else if(textBox1.Text!="" && textBox1.Text != null)
			{
				string SN= this.textBox1.Text.Trim();
				//Clear_fields();
				if(SN!=""&&SN!=null)
				History_Check(SN);
			}
			else
			{
				MessageBox.Show("Please enter in one or more Serial number!");
			}
		}
	
		
		void ResetClick(object sender, EventArgs e)
		{
			Clear_fields();
		}
		#endregion
				
		#region Populate_Defects
		public void Populate_Defects(){
			
		
			comboBox5.Items.Clear();//Clear all Data from Sepecific Defect.
			string Cate = comboBox3.Text.ToString();
			if( Cate=="Foreign_Material"){
				comboBox5.Items.AddRange(new object[] {
				                         	"Piece(s) of Ribbon",
											"Piece(s) of Cell",
											"Cello Tape",
											"Bug(s)",
											"Flux",
											"Paper",
											"Hair",
											"Fiber",
											"Dirt or Debris",
											"Organic with Bubble",
											"Teflon Tape",
											"Piece of Wood",
											"Glass",
											"Screw",
											"Solder flow - String repair",
											"Foreign Object Under Backsheet", 
											"Piece(s) of Busbar",
											"Scotch tape",
											"EVA deposit on glass",
											"Extra Barcode",
											"Bussing template",
											"Contamination - Unknown",
				                   });
			}
			
			else if (Cate=="Cell_damage") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Cracked cell",
												"Cracked Corner",
												"Broken cell",
												"Chipped Cell - V",
												"Chipped Cell - Non V",
												"Hole in Cell",
												"Multiple Cells Chipped",
												"Microcrack(s)",
												"Broken Gridlines",				
				                   });
			}
				
			else if (Cate=="Ribbon_Misaligned") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Misaligned Ribbon",
												"Unsoldered Ribbon",													
					               });
			}
			else if (Cate=="Cell_Spacing") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Cell to Cell Gap (too narrow)",
												"Cell to Cell Gap (Cell Touching)",
												"String to String Gap (too narrow)",
												"String to String Gap (Cell Touching)",
												"String Gap Too Large",
												"Cell Gap Too Large",
												"Cell too close to edge-frame",
												"Misaligned Cell",													
						        	});
			}
			else if (Cate=="Backsheet_Defect") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Backsheet Scratch-Cut",
												"Backsheet Gouged",
												"Backsheet Puncture",
												"Backsheet Crease",
												"Backsheet Burn",
												"Backsheet Dent",
												"Writing on Backsheet",
												"Backsheet not trimmed",
												"Discolored backsheet",						                        	 		
							        });
			}
				else if (Cate=="Solder_Splatter") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Solder Splatter - Bussing",
												"Solder Splatter - Tacking",
												"Solder Splatter - EVA Burn-Bussing",
												"Solder Splatter - EVA Burn-Tack",
												"Solder Splash - On Backsheet",
												"Solder Splash - On Cell",
												"Solder Splash - Bussing",
												"Poor Soldering",	
												"Other Solder Defect - see notes",
												"Multiple Solder Defects - see notes",				                        	 		
								       });
			}
				else if (Cate=="Overbaked_or_Underbaked_laminates") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Under Baked Laminate",
												"Delamination",
												"Overbaked Laminate",														                        	 		
									    });
			}
				else if (Cate=="Broken_Laminates_or_Modules") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Broken Laminate",
												"Broken Module",
												"Broken Glass",														                        	 		
										});
			}
				else if (Cate=="Busbar_Showing") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Bus Bar Showing",													                        	 		
										});
			}
				else if (Cate=="Serialization_and_Barcodes") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Barcode Upside Down",
												"Missing Barcode",
												"Extra SN label",
												"Error in Label content",
												"Label illegible",
												"Mismatched Label(s)",
												"Incorrect Label position",
												"Barcode Twisted",
										});
			}
				else if (Cate=="Engineering_or_Lab_Analysis") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Certification",
												"Other Destructive test",
												"Reliability",
												"Lab - Microcracks",													                       	 		
										});
			}
			
				else if (Cate=="Backsheet_or_EVA_-_Blue_Tape") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Backsheet Tape",
				                        	 	"EVA_Tape",	                        	 	
										});
			}
				else if (Cate=="Backsheet_not_covering_glass") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Backsheet Offset",												                        	 		
										});
			}
				else if (Cate=="Bubbles") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Bubbles near barcode",
												"Bubbles in EVA",
												"Bubbles on strip",
												"Bubbles in Backsheet",
												"Other bubbles",										                        	 		
										});
			}
				else if (Cate=="Bussbar_Defects") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Busbar too close to frame",
												"Busbar to Cell Gap Too Narrow",
												"Busbar to busbar gap",
												"Additional Busbar(s)",
												"Unsoldered Busbar",
												"broken busbar",
												"Twisted Busbar",
												"Bent Busbar",				                        	 		
										});
			}
				else if (Cate=="Supplier_Defects") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Damaged Diodes",
												"Cell Defect",
												"Defective Frame Countersink",
												"Glass Bubbles",
												"Incorrect position of diodes",
												"Jbox gasket",
												"Missing Holes",
												"Oversized frame",
												"Undersized frame",
												"Warped Frame",
				                        	 	"Wrong Hole position",	
				                        	 	"Wrong number of holes",	
				                        	 	"Broken connector",
				                        	 	"Jbox Lid tabs",
												"Other (add comment)",          	 		
										});
			}
																		
				else if (Cate=="Uncut_Ribbon_or_Busbar") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Uncut ribbon",
												"Uncut busbar",													                        	 		
										});
			}
				else if (Cate=="Ribbon_Twisted") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Twisted Ribbon",
												"Bent Ribbon",             	 		
										});
			}
				else if (Cate=="Scratch_on_Glass") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Glass Scratch",
												"Glass Chipping",                      	 		
										});
			}
				else if (Cate=="Part_Orientation") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Reversed Backsheet",
												"Glass Reversed",              	 		
										});
			}
				else if (Cate=="Backsheet_Slit_Defects") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Extra Backsheet Slit",
												"Oversized slit",                    	 		
										});
			}
																							
				else if (Cate=="Frame_Damage_or_Defect") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Frame Scratch",
												"Frame Punctured",
												"Warped Frame",      	 		
										});
			}
				else if (Cate=="Cosmetic_Defects") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Color Variation",
												"Dark Lines",
												"Uneven Color",
												"Skewed Jbox",
												"Dirt on backsheet",       	 		
										});
			}
				else if (Cate=="Final_Test_Failures") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Low Power",
												"Barcode Rejects",
												"Hi-Pot Rejects",
												"Other Final Test Rejects",         	 		
										});
			}
				else if (Cate=="Silicone_or_Adhesive_Defects") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Excessive silicone",
												"Insufficient silicone",
												"Silicone voids",
												"Potting not cured",
												"Too little potting",
												"Too much potting",				                        	 		
										});
			}
				else if (Cate=="Product_Configuration") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Unqualified BOM",
												"Wrong Backsheet",
												"Wrong frame",
												"Wrong Jbox",
												"Wrong silicone type",
												"Wrong Cells",									                     	 		
				                        });
			}
				
																												
				else if (Cate=="Frame_Joint") {
				comboBox5.Items.AddRange(new object[] { 
				                        	 	"Screw(s) missing",
												"Screw(s) stripped",
												"Frame joint(s) not flush",
												"Frame Gap",
												"Frame Offset",									                     	 		
				                        });
			}
            else if (Cate == "Other")
            {
                comboBox5.Items.AddRange(new object[] {
                                                 "Oxidized ribbon-cell",
                                                "String Polarity Reversed",
                                                "EVA splice",
                                                "J-Box Lifted",
                                                "Damaged J-box Leads",
                                                "Damaged Busbar leads",
                                                "Broken connector",
                                                 "Jbox Lid tabs",
                                                "Other",
                                        });
            }
            else if (Cate == "EL_Reject") {
				comboBox5.Items.AddRange(new object[] {
                                                 "Micro crack",
                                                "Over  soldering",
                                                "Poor soldering",
                                                "Black spot",
                                                "Dark edge",
                                                "Mixed cell efficiency",
                                                "Short-circuit or dead cell",
                                                "No image ",
                                                "Pressure mark for flex module ",
                                        });
			}
            else if (Cate == "Flex Module cosmetic defect")
            {
                comboBox5.Items.AddRange(new object[] {
                                                 "Wrinkl",
                                                "Others",                                               
                                        });
            }





        }
	
#endregion

		
		
		
	
		
		
}
}
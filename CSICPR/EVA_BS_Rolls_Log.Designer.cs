﻿namespace CSICPR
{
    partial class EVA_BS_Rolls_Log
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.line = new System.Windows.Forms.Label();
            this.lineicon = new System.Windows.Forms.Label();
            this.HoistChecklist = new System.Windows.Forms.Label();
            this.Type = new System.Windows.Forms.GroupBox();
            this.backsheet = new System.Windows.Forms.RadioButton();
            this.secondeva = new System.Windows.Forms.RadioButton();
            this.firsteva = new System.Windows.Forms.RadioButton();
            this.station = new System.Windows.Forms.GroupBox();
            this.stn2 = new System.Windows.Forms.RadioButton();
            this.stn1 = new System.Windows.Forms.RadioButton();
            this.OperatorIniatial = new System.Windows.Forms.Label();
            this.initial = new System.Windows.Forms.ComboBox();
            this.su = new System.Windows.Forms.TextBox();
            this.sunum = new System.Windows.Forms.Label();
            this.batchnum = new System.Windows.Forms.Label();
            this.batch = new System.Windows.Forms.TextBox();
            this.rollnum = new System.Windows.Forms.Label();
            this.roll = new System.Windows.Forms.TextBox();
            this.submit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comments = new System.Windows.Forms.TextBox();
            this.Type.SuspendLayout();
            this.station.SuspendLayout();
            this.SuspendLayout();
            // 
            // line
            // 
            this.line.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.line.AutoSize = true;
            this.line.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.line.Location = new System.Drawing.Point(285, 65);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(41, 39);
            this.line.TabIndex = 6;
            this.line.Text = "X";
            // 
            // lineicon
            // 
            this.lineicon.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lineicon.AutoSize = true;
            this.lineicon.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lineicon.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lineicon.Location = new System.Drawing.Point(167, 65);
            this.lineicon.Name = "lineicon";
            this.lineicon.Size = new System.Drawing.Size(107, 39);
            this.lineicon.TabIndex = 5;
            this.lineicon.Text = "Line :";
            // 
            // HoistChecklist
            // 
            this.HoistChecklist.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.HoistChecklist.AutoSize = true;
            this.HoistChecklist.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HoistChecklist.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.HoistChecklist.Location = new System.Drawing.Point(366, 65);
            this.HoistChecklist.Name = "HoistChecklist";
            this.HoistChecklist.Size = new System.Drawing.Size(481, 39);
            this.HoistChecklist.TabIndex = 8;
            this.HoistChecklist.Text = "EVA/Backsheet Loading Log";
            // 
            // Type
            // 
            this.Type.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Type.Controls.Add(this.backsheet);
            this.Type.Controls.Add(this.secondeva);
            this.Type.Controls.Add(this.firsteva);
            this.Type.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Type.Location = new System.Drawing.Point(178, 141);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(175, 111);
            this.Type.TabIndex = 15;
            this.Type.TabStop = false;
            this.Type.Text = "TYPE";
            // 
            // backsheet
            // 
            this.backsheet.AutoSize = true;
            this.backsheet.Location = new System.Drawing.Point(6, 79);
            this.backsheet.Name = "backsheet";
            this.backsheet.Size = new System.Drawing.Size(140, 29);
            this.backsheet.TabIndex = 1;
            this.backsheet.Text = "Backsheet";
            this.backsheet.UseVisualStyleBackColor = true;
            this.backsheet.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
            // 
            // secondeva
            // 
            this.secondeva.AutoSize = true;
            this.secondeva.Location = new System.Drawing.Point(6, 52);
            this.secondeva.Name = "secondeva";
            this.secondeva.Size = new System.Drawing.Size(121, 29);
            this.secondeva.TabIndex = 1;
            this.secondeva.Text = "2nd EVA";
            this.secondeva.UseVisualStyleBackColor = true;
            this.secondeva.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
            // 
            // firsteva
            // 
            this.firsteva.AutoSize = true;
            this.firsteva.Location = new System.Drawing.Point(6, 25);
            this.firsteva.Name = "firsteva";
            this.firsteva.Size = new System.Drawing.Size(114, 29);
            this.firsteva.TabIndex = 0;
            this.firsteva.Text = "1st EVA";
            this.firsteva.UseVisualStyleBackColor = true;
            this.firsteva.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
            // 
            // station
            // 
            this.station.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.station.Controls.Add(this.stn2);
            this.station.Controls.Add(this.stn1);
            this.station.Cursor = System.Windows.Forms.Cursors.Hand;
            this.station.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.station.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.station.Location = new System.Drawing.Point(429, 141);
            this.station.Name = "station";
            this.station.Size = new System.Drawing.Size(175, 111);
            this.station.TabIndex = 15;
            this.station.TabStop = false;
            this.station.Text = "STATION";
            // 
            // stn2
            // 
            this.stn2.AutoSize = true;
            this.stn2.Location = new System.Drawing.Point(6, 79);
            this.stn2.Name = "stn2";
            this.stn2.Size = new System.Drawing.Size(85, 29);
            this.stn2.TabIndex = 1;
            this.stn2.Text = "Stn 2";
            this.stn2.UseVisualStyleBackColor = true;
            this.stn2.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
            // 
            // stn1
            // 
            this.stn1.AutoSize = true;
            this.stn1.Location = new System.Drawing.Point(6, 25);
            this.stn1.Name = "stn1";
            this.stn1.Size = new System.Drawing.Size(85, 29);
            this.stn1.TabIndex = 0;
            this.stn1.Text = "Stn 1";
            this.stn1.UseVisualStyleBackColor = true;
            this.stn1.CheckedChanged += new System.EventHandler(this.radiobutton_CheckedChanged);
            // 
            // OperatorIniatial
            // 
            this.OperatorIniatial.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.OperatorIniatial.AutoSize = true;
            this.OperatorIniatial.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OperatorIniatial.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.OperatorIniatial.Location = new System.Drawing.Point(168, 468);
            this.OperatorIniatial.Name = "OperatorIniatial";
            this.OperatorIniatial.Size = new System.Drawing.Size(229, 31);
            this.OperatorIniatial.TabIndex = 17;
            this.OperatorIniatial.Text = "Operator\'s Initial";
            // 
            // initial
            // 
            this.initial.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.initial.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initial.FormattingEnabled = true;
            this.initial.Location = new System.Drawing.Point(416, 468);
            this.initial.Name = "initial";
            this.initial.Size = new System.Drawing.Size(202, 33);
            this.initial.TabIndex = 16;
            this.initial.TextUpdate += new System.EventHandler(this.initial_TextUpdate);
            // 
            // su
            // 
            this.su.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.su.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.su.Location = new System.Drawing.Point(416, 344);
            this.su.Name = "su";
            this.su.Size = new System.Drawing.Size(202, 31);
            this.su.TabIndex = 18;
            // 
            // sunum
            // 
            this.sunum.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.sunum.AutoSize = true;
            this.sunum.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sunum.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.sunum.Location = new System.Drawing.Point(168, 344);
            this.sunum.Name = "sunum";
            this.sunum.Size = new System.Drawing.Size(254, 31);
            this.sunum.TabIndex = 17;
            this.sunum.Text = "SU #(if applicable)";
            // 
            // batchnum
            // 
            this.batchnum.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.batchnum.AutoSize = true;
            this.batchnum.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batchnum.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.batchnum.Location = new System.Drawing.Point(168, 282);
            this.batchnum.Name = "batchnum";
            this.batchnum.Size = new System.Drawing.Size(113, 31);
            this.batchnum.TabIndex = 17;
            this.batchnum.Text = "Batch #";
            // 
            // batch
            // 
            this.batch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.batch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batch.Location = new System.Drawing.Point(416, 282);
            this.batch.Name = "batch";
            this.batch.Size = new System.Drawing.Size(202, 31);
            this.batch.TabIndex = 18;
            // 
            // rollnum
            // 
            this.rollnum.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rollnum.AutoSize = true;
            this.rollnum.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rollnum.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rollnum.Location = new System.Drawing.Point(168, 406);
            this.rollnum.Name = "rollnum";
            this.rollnum.Size = new System.Drawing.Size(182, 31);
            this.rollnum.TabIndex = 17;
            this.rollnum.Text = "Roll # (Lot #)";
            // 
            // roll
            // 
            this.roll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roll.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roll.Location = new System.Drawing.Point(416, 406);
            this.roll.Name = "roll";
            this.roll.Size = new System.Drawing.Size(202, 31);
            this.roll.TabIndex = 18;
            // 
            // submit
            // 
            this.submit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submit.Location = new System.Drawing.Point(390, 528);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(134, 35);
            this.submit.TabIndex = 19;
            this.submit.Text = "SUBMIT";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(668, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 31);
            this.label1.TabIndex = 17;
            this.label1.Text = "Comments";
            // 
            // comments
            // 
            this.comments.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comments.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comments.Location = new System.Drawing.Point(645, 282);
            this.comments.Multiline = true;
            this.comments.Name = "comments";
            this.comments.Size = new System.Drawing.Size(202, 219);
            this.comments.TabIndex = 18;
            this.comments.TextChanged += new System.EventHandler(this.comments_TextChanged);
            // 
            // EVA_BS_Rolls_Log
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1016, 635);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.comments);
            this.Controls.Add(this.batch);
            this.Controls.Add(this.roll);
            this.Controls.Add(this.su);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.batchnum);
            this.Controls.Add(this.rollnum);
            this.Controls.Add(this.sunum);
            this.Controls.Add(this.OperatorIniatial);
            this.Controls.Add(this.initial);
            this.Controls.Add(this.station);
            this.Controls.Add(this.Type);
            this.Controls.Add(this.HoistChecklist);
            this.Controls.Add(this.line);
            this.Controls.Add(this.lineicon);
            this.Name = "EVA_BS_Rolls_Log";
            this.Text = "EVA BS Rolls Log";
            this.Load += new System.EventHandler(this.EVA_BS_Rolls_Log_Load);
            this.Type.ResumeLayout(false);
            this.Type.PerformLayout();
            this.station.ResumeLayout(false);
            this.station.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label line;
        private System.Windows.Forms.Label lineicon;
        private System.Windows.Forms.Label HoistChecklist;
        private System.Windows.Forms.GroupBox Type;
        private System.Windows.Forms.RadioButton backsheet;
        private System.Windows.Forms.RadioButton secondeva;
        private System.Windows.Forms.RadioButton firsteva;
        private System.Windows.Forms.GroupBox station;
        private System.Windows.Forms.RadioButton stn2;
        private System.Windows.Forms.RadioButton stn1;
        private System.Windows.Forms.Label OperatorIniatial;
        private System.Windows.Forms.ComboBox initial;
        private System.Windows.Forms.TextBox su;
        private System.Windows.Forms.Label sunum;
        private System.Windows.Forms.Label batchnum;
        private System.Windows.Forms.TextBox batch;
        private System.Windows.Forms.Label rollnum;
        private System.Windows.Forms.TextBox roll;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox comments;
    }
}
﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 22/09/2011
 * Time: 11:22 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace CSICPR
{
	/// <summary>
	/// Description of FormQualityCheck.
	/// </summary>
	public partial class FormQualityInspection : Form
	{
		public FormQualityInspection()
		{
			InitializeComponent();
		}
		
		//--------------This is to ensure only one instance of this form is loaded
		private static FormQualityInspection theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormQualityInspection();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		void Button1Click(object sender, EventArgs e)
		{
			string sn = textBox1.Text.ToString().Trim();
			string moduleClass = "";
			
			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length!");
				return;
			}
			
			if(comboBox1.SelectedItem == null)
			{
				MessageBox.Show("Class field is empty. Please select the class.");
				return;
			}
			else
			{
				moduleClass = comboBox1.SelectedItem.ToString().Trim();
			}
			if(comboBox3.SelectedItem == null)
			{
				MessageBox.Show("Source field is empty. Please select a source.");
				return;
			}			
			if(moduleClass == "A1" && Cb_Defect.SelectedItem == null)
			{
				MessageBox.Show("Please select defect Category for class A1.");
				return;
			}
			
            if(!isPacked(textBox1.Text.ToString().Trim()))
            {
            	MessageBox.Show("Module "+sn+" has been packaged, its class cannot be changed!");
                return;
            }
            if(comboBox1.Text.ToString() == "B" || comboBox1.Text.ToString() == "F")
            if(string.IsNullOrEmpty(Cb_Defect.Text.ToString()))
            {
            	MessageBox.Show("Please select defect category!");
            	return;
            }

            if (!ToolsClass.GradeIfBFClass(sn))
                return;


            string sql = "insert into Qualityjudge (SN, ModuleClass, timestamp, operator, QualitySource, DefectCategory, remark) values ('"+sn+"', '"+moduleClass+"', getdate(), '"+FormCover.CurrUserWorkID+"','"+ comboBox3.Text.ToString() +"', '"+ Cb_Defect.Text.ToString() +"', '"+textBox2.Text+"')";
          
            ToolsClass.ExecuteNonQuery(sql);
            
            MessageBox.Show("Module "+sn+" has been graded as class "+moduleClass);
            
            textBox1.Clear();
            comboBox1.SelectedItem = null;
          	Cb_Defect.SelectedItem = null;
          	comboBox3.SelectedItem = null;
            textBox2.Clear();

		}
		
		void Button2MouseClick(object sender, MouseEventArgs e)
		{
			string sn = textBox1.Text.ToString().Trim();
			string moduleClass = "";
			
			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length!");
				return;
			}
			
			string sql = "select moduleclass from qualityjudge where id = (select max(ID) from qualityjudge where sn = '"+sn+"')";
			SqlDataReader sdr = ToolsClass.GetDataReader(sql);
			if(sdr != null && sdr.Read())
			{
				moduleClass = sdr.GetString(0);
			}
			else
			{
				moduleClass = "A";
			}
			sdr.Close();
            sdr = null;
            
            MessageBox.Show("Module "+sn+" is class "+moduleClass);
		}
		
		private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            string connection = ToolsClass.NEWPACKINGConnStr();
            sql = string.Format(sql, sSerialNumber);
            SqlDataReader sdr = ToolsClass.GetDataReader(sql,connection);
            if(sdr != null && sdr.Read())
            {
            	return false;
            }
            sdr.Close();
            sdr = null;
            return true;
        }
		
		void TextBox1Enter(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.Color.Yellow;
		}
		
		void TextBox1Leave(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.SystemColors.Window;
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			if(comboBox1.SelectedItem == null)
			{
				MessageBox.Show("Class field is empty. Please select the class.");
				return;
			}
			
			
            string strTemp = textBox3.Text;
            string[] sDataSet = strTemp.Split('\n');
            for (int i = 0; i < sDataSet.Length; i++)
            {
                sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                if (sDataSet[i] != null && sDataSet[i] != "")
                    batchGrade(sDataSet[i]);
            }
            textBox3.Clear();
            comboBox3.SelectedItem = null;
            comboBox1.SelectedItem = null;
            Cb_Defect.SelectedItem = null;
            textBox2.Clear();
		}
		
		void batchGrade(string sn)
		{
			string moduleClass = comboBox1.SelectedItem.ToString().Trim();
		
			if(string.IsNullOrEmpty(Cb_Defect.Text.ToString()))
            {
            	MessageBox.Show("Please select defect category!");
            	return;
            }
			if(comboBox3.SelectedItem == null)
			{
				MessageBox.Show("Source field is empty. Please select a source.");
				return;
			}
            if (sn.Length > 14)
                sn = GetOtherSN(sn);

			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length!");
				return;
			}			
			
            if (!isPacked(sn))
            {
            	MessageBox.Show("Module "+sn+" has been packaged, its class cannot be changed!");
                return;
            }

            if (!ToolsClass.GradeIfBFClass(sn))
                return;
                  
            string sql = "insert into Qualityjudge (SN, ModuleClass, timestamp, operator, QualitySource , DefectCategory , remark) values ('"+sn+"', '"+moduleClass+"', getdate(), '"+FormCover.CurrUserWorkID+"', '"+ comboBox3.Text.ToString() + "','"+ Cb_Defect.Text.ToString() +"', '"+textBox2.Text+"')";
            ToolsClass.ExecuteNonQuery(sql);
		}       

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string SN = "";
            if (textBox1.Text.Trim().Length < 1)
                return;
            if (textBox1.Text.Trim().Substring(0, 1) == "e")
            {
                if (textBox1.Text.Trim().Length < 22)
                    return;
                else
                {
                    SN = ToolsClass.CsiToFlexSN(textBox1.Text.Trim());
                    tBFlexMatch.Text = textBox1.Text;
                    textBox1.Text = SN;
                }
                tBFlexMatch.Visible = true;
            }
            else if (textBox1.Text.Trim().Substring(0, 1) == "G")
            {
                 if (textBox1.Text.Trim().Length < 21)
                     return;
                 else
                 {
                     SN = ToolsClass.CsiToGrape(textBox1.Text.Trim());
                     tBFlexMatch.Text = textBox1.Text;
                     textBox1.Text = SN;
                 }
                 tBFlexMatch.Visible = true;
             }
            else
            {
                tBFlexMatch.Visible = false;
                SN = this.textBox1.Text.Trim();
            }
        }

        string GetOtherSN(string sn)
        {
            string sn_return = "";
            if (sn.Substring(0, 1) == "e")
            {
                if (sn.Trim().Length < 22)
                    return "";
                sn_return = ToolsClass.CsiToFlexSN(sn);
            }
            else if (sn.Substring(0, 1) == "G")
            {
                if (sn.Trim().Length < 21)
                    return "";
                sn_return = ToolsClass.CsiToGrape(sn);
            }
            return sn_return;
        }

    }
}

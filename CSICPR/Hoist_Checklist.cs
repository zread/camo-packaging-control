﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;


namespace CSICPR
{
    public partial class Hoist_Checklist : Form
    {
        public Hoist_Checklist(string lne)
        {
            InitializeComponent();
            line.Text = lne;
            tableLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;       

        }
        public string[] isgood= {"","","","","","",""};
        private DataTable Executenonequery(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void CheckListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
       

        private void submit_Click(object sender, EventArgs e)
        {
            submit.Enabled = false;
                 
            
                
                //checks if it already is in the combobox
                if ( !initial.Items.Contains(initial.Text.ToUpper()) && initial.Text != "")
                {
                //adds to combobox
                    StreamWriter writor = new StreamWriter("C:\\initial\\initial.txt", true);
                    writor.WriteLine(initial.Text.ToUpper());
                    writor.Flush();
                    writor.Dispose();
                    initial.Items.Add(initial.Text.ToUpper());
                }
                else
                {
                    //nothing
                  
                }

            
           

            bool isnull=false;
            for (int i= 0; i < 7;i++)
            {
                if (isgood[i] == "")
                    isnull = true;
            }
            if (string.IsNullOrEmpty(Equipment.Text.ToString()) || string.IsNullOrEmpty(initial.Text.ToString())||isnull || string.IsNullOrEmpty(Shift.Text.ToString()))
            {
                MessageBox.Show("Some Field Is Blank.");
            }
            else  
                    {
                string sqlselect = "SELECT * FROM [CAPA01DB01].[dbo].[Hoist_Inspection] " +
                    "where [equipment]='" + Equipment.Text + "' and [line]='"+line.Text+"'and [createdate]>=getdate()-0.5 and shift = '" + Shift.Text + "'";
                 DataTable dt= Executenonequery(sqlselect);
                
                
               if (dt.Rows.Count==0)
                {
                    string sql = "Insert into [CAPA01DB01].[dbo].[Hoist_Inspection] " +
                    	"(equipment, initial, all_components, hoist_equipment, hooks, chains, end_stops, operating_mechanism, controls, createdate, Line, comments, shift)" +
"                    	values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',getdate(),'{9}', '{10}', '{11}')";
                    sql = string.Format(sql, Equipment.Text.ToString(), initial.Text.ToUpper(), 
                                        isgood[0], isgood[1], isgood[2], isgood[3], isgood[4], isgood[5], isgood[6],line.Text.ToString(), 
                                        commentsTB.Text.ToString(), Shift.Text.ToString());
                    Executenonequery(sql);
                    MessageBox.Show("Done");
                    Equipment.Items.Clear();
                    Shift.Items.Clear();
                    initial.Text = "";
                    good1.Checked = false;
                    good2.Checked = false;
                    good3.Checked = false;
                    good4.Checked = false;
                    good5.Checked = false;
                    good6.Checked = false;
                    good7.Checked = false;
                    ng1.Checked = false;
                    ng2.Checked = false;
                    ng3.Checked = false;
                    ng4.Checked = false;
                    ng5.Checked = false;
                    ng6.Checked = false;
                    ng7.Checked = false;
                    commentsTB.Text = "";
                }
                else
                {
                    MessageBox.Show("Line "+line.Text+" "+Equipment.Text + " was already pre-shift inspected today.");
                }


            }
            submit.Enabled = true;
        }

        private void radiobutton_CheckedChanged(object sender, EventArgs e)
        {
            for(int i = 0; i < 7; i++)
            {
                GroupBox[] gb = { groupBox1, groupBox2, groupBox3, groupBox4, groupBox5, groupBox6, groupBox7 };

                foreach (Control control in gb[i].Controls)
                {
                    if (control is RadioButton)
                    {
                        RadioButton radio = control as RadioButton;
                        if (radio.Checked)
                        {
                            isgood[i] = radio.Text;
                        }
                    }
                }

                    
            }
           
        }

        private void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            var rectangle = e.CellBounds;
            rectangle.Inflate(-1, -1);

            ControlPaint.DrawBorder(e.Graphics, rectangle, Color.DarkBlue, ButtonBorderStyle.Dotted); // dotted 
        }

        private void Hoist_Checklist_Load(object sender, EventArgs e)
        {
            // Check if the directory exists, if not it's created!  

            if (Directory.Exists("C:\\initial\\"))
            {

                //nothing  

            }
            else
            {

                Directory.CreateDirectory("C:\\initial\\");
            }

            // Check if the file exists, if not it's created!  

            if (File.Exists("C:\\initial\\initial.txt"))
            {

                //nothing  

            }
            else
            {

                File.Create("C:\\initial\\initial.txt");

            }


            StreamReader sr = new StreamReader("C:\\initial\\initial.txt", true);

            while (sr.Peek() >= 0)

            {

                initial.Items.Add(sr.ReadLine());

            }

            sr.Close();

        }

        private void initial_TextUpdate(object sender, EventArgs e)
        {
            if (initial.Text.Count() > 3)
            {
                initial.Text = initial.Text.Substring(0,initial.Text.Length-1);
                MessageBox.Show("Initial's Characters couldn't be greater than 3.");
            }
        }
		void ShiftSelectedIndexChanged(object sender, EventArgs e)
		{
	
		}
    }
}

﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 22/09/2011
 * Time: 11:22 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CSICPR
{
	partial class FormQualityInspection
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.Cb_Defect = new System.Windows.Forms.ComboBox();
			this.tBFlexMatch = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.comboBox3 = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(154, 15);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(176, 20);
			this.textBox1.TabIndex = 0;
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			this.textBox1.Enter += new System.EventHandler(this.TextBox1Enter);
			this.textBox1.Leave += new System.EventHandler(this.TextBox1Leave);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(184, 326);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(89, 34);
			this.button1.TabIndex = 1;
			this.button1.Text = "Grade";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// comboBox1
			// 
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
			"A",
			"A-",
			"C"});
			this.comboBox1.Location = new System.Drawing.Point(154, 60);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(63, 21);
			this.comboBox1.TabIndex = 2;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(183, 276);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(89, 35);
			this.button2.TabIndex = 3;
			this.button2.Text = "Check";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button2MouseClick);
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(154, 214);
			this.textBox2.MaxLength = 255;
			this.textBox2.Multiline = true;
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(321, 43);
			this.textBox2.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "SN";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(16, 63);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 14);
			this.label2.TabIndex = 7;
			this.label2.Text = "Class";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(16, 214);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(70, 21);
			this.label4.TabIndex = 9;
			this.label4.Text = "Remark";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(552, 27);
			this.textBox3.Multiline = true;
			this.textBox3.Name = "textBox3";
			this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox3.Size = new System.Drawing.Size(206, 266);
			this.textBox3.TabIndex = 10;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(470, 30);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(78, 26);
			this.label5.TabIndex = 11;
			this.label5.Text = "SN Batch";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(611, 330);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(89, 35);
			this.button3.TabIndex = 12;
			this.button3.Text = "Batch Grade";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(16, 161);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(122, 22);
			this.label6.TabIndex = 14;
			this.label6.Text = "Defect Category";
			// 
			// Cb_Defect
			// 
			this.Cb_Defect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.Cb_Defect.FormattingEnabled = true;
			this.Cb_Defect.Items.AddRange(new object[] {
			"Backsheet_Defect",
			"Backsheet_or_EVA_-_Blue_Tape",
			"Backsheet_Slit_Defects",
			"Backsheet_not_covering_glass",
			"Broken_Laminates_or_Modules",
			"Busbar_Showing",
			"Bubbles",
			"Bussbar_Defects",
			"Cell_damage",
			"Cell_Spacing",
			"Cosmetic_Defects",
			"Engineering_or_Lab_Analysis",
			"Final_Test_Failures",
			"Foreign_Material",
			"Frame_Joint",
			"Frame_Damage_or_Defect",
			"Hotplate_Rework",
			"Overbaked_or_Underbaked_laminates",
			"Part_Orientation",
			"Potting_Sillicon_Defect",
			"Product_Configuration",
			"Ribbon_Misaligned",
			"Ribbon_Twisted",
			"Scratch_on_Glass",
			"Serialization_and_Barcodes",
			"Silicone_or_Adhesive_Defects",
			"Solder_Splatter",
			"Supplier_Defects",
			"Uncut_Ribbon_or_Busbar",
			"Other"});
			this.Cb_Defect.Location = new System.Drawing.Point(154, 162);
			this.Cb_Defect.Name = "Cb_Defect";
			this.Cb_Defect.Size = new System.Drawing.Size(321, 21);
			this.Cb_Defect.TabIndex = 13;
			// 
			// tBFlexMatch
			// 
			this.tBFlexMatch.Enabled = false;
			this.tBFlexMatch.Location = new System.Drawing.Point(154, 39);
			this.tBFlexMatch.Name = "tBFlexMatch";
			this.tBFlexMatch.Size = new System.Drawing.Size(176, 20);
			this.tBFlexMatch.TabIndex = 17;
			this.tBFlexMatch.Visible = false;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(16, 108);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(70, 22);
			this.label7.TabIndex = 19;
			this.label7.Text = "Source";
			// 
			// comboBox3
			// 
			this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox3.FormattingEnabled = true;
			this.comboBox3.Items.AddRange(new object[] {
			"Bin 5",
			"OQC",
			"PASAN / HiPot",
			"Hotplate RWK",
			"Other"});
			this.comboBox3.Location = new System.Drawing.Point(154, 109);
			this.comboBox3.Name = "comboBox3";
			this.comboBox3.Size = new System.Drawing.Size(176, 21);
			this.comboBox3.TabIndex = 18;
			// 
			// FormQualityInspection
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.ClientSize = new System.Drawing.Size(963, 538);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.comboBox3);
			this.Controls.Add(this.tBFlexMatch);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.Cb_Defect);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.textBox1);
			this.Name = "FormQualityInspection";
			this.Text = "Quality Inspection";
			this.Load += new System.EventHandler(this.FormQualityInspectionLoad);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.ComboBox Cb_Defect;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		
		void FormQualityInspectionLoad(object sender, System.EventArgs e)
		{
			
		}

        private System.Windows.Forms.TextBox tBFlexMatch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox3;
    }
}

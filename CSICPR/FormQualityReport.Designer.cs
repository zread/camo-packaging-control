﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 3/16/2012
 * Time: 2:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CSICPR
{
	partial class FormQualityReport
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.dateFrom = new System.Windows.Forms.DateTimePicker();
			this.dateTo = new System.Windows.Forms.DateTimePicker();
			this.lblfrom = new System.Windows.Forms.Label();
			this.lblto = new System.Windows.Forms.Label();
			this.txtSN = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Source = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.button1 = new System.Windows.Forms.Button();
			this.rbSN = new System.Windows.Forms.RadioButton();
			this.rbDate = new System.Windows.Forms.RadioButton();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button2 = new System.Windows.Forms.Button();
			this.tBFlexMatch = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dateFrom
			// 
			this.dateFrom.CustomFormat = "yyyy-M-d";
			this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateFrom.Location = new System.Drawing.Point(68, 30);
			this.dateFrom.Name = "dateFrom";
			this.dateFrom.Size = new System.Drawing.Size(100, 20);
			this.dateFrom.TabIndex = 1;
			this.dateFrom.Value = new System.DateTime(2011, 5, 26, 0, 0, 0, 0);
			// 
			// dateTo
			// 
			this.dateTo.CustomFormat = "yyyy-M-d";
			this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTo.Location = new System.Drawing.Point(270, 30);
			this.dateTo.Name = "dateTo";
			this.dateTo.Size = new System.Drawing.Size(99, 20);
			this.dateTo.TabIndex = 2;
			this.dateTo.Value = new System.DateTime(2011, 5, 27, 0, 0, 0, 0);
			// 
			// lblfrom
			// 
			this.lblfrom.AutoSize = true;
			this.lblfrom.Location = new System.Drawing.Point(12, 30);
			this.lblfrom.Name = "lblfrom";
			this.lblfrom.Size = new System.Drawing.Size(33, 13);
			this.lblfrom.TabIndex = 5;
			this.lblfrom.Text = "From:";
			// 
			// lblto
			// 
			this.lblto.AutoSize = true;
			this.lblto.Location = new System.Drawing.Point(232, 30);
			this.lblto.Name = "lblto";
			this.lblto.Size = new System.Drawing.Size(23, 13);
			this.lblto.TabIndex = 6;
			this.lblto.Text = "To:";
			// 
			// txtSN
			// 
			this.txtSN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtSN.Location = new System.Drawing.Point(68, 67);
			this.txtSN.MaxLength = 30;
			this.txtSN.Name = "txtSN";
			this.txtSN.Size = new System.Drawing.Size(100, 20);
			this.txtSN.TabIndex = 22;
			this.txtSN.TextChanged += new System.EventHandler(this.txtSN_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 67);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(22, 13);
			this.label1.TabIndex = 23;
			this.label1.Text = "SN";
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Column1,
			this.Column2,
			this.Column3,
			this.Column4,
			this.Column5,
			this.Source,
			this.Column6});
			this.dataGridView1.Location = new System.Drawing.Point(3, 112);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(947, 510);
			this.dataGridView1.TabIndex = 24;
			this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellContentClick);
			// 
			// Column1
			// 
			this.Column1.HeaderText = "SN";
			this.Column1.Name = "Column1";
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Class";
			this.Column2.Name = "Column2";
			// 
			// Column3
			// 
			this.Column3.HeaderText = "TimeStamp";
			this.Column3.Name = "Column3";
			this.Column3.Width = 150;
			// 
			// Column4
			// 
			this.Column4.HeaderText = "Operator";
			this.Column4.Name = "Column4";
			// 
			// Column5
			// 
			this.Column5.HeaderText = "Defect Code";
			this.Column5.Name = "Column5";
			// 
			// Source
			// 
			this.Source.HeaderText = "Source";
			this.Source.Name = "Source";
			this.Source.ReadOnly = true;
			// 
			// Column6
			// 
			this.Column6.HeaderText = "Remark";
			this.Column6.Name = "Column6";
			this.Column6.Width = 250;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(852, 27);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 25;
			this.button1.Text = "Export";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// rbSN
			// 
			this.rbSN.Location = new System.Drawing.Point(16, 43);
			this.rbSN.Name = "rbSN";
			this.rbSN.Size = new System.Drawing.Size(104, 24);
			this.rbSN.TabIndex = 26;
			this.rbSN.Text = "Search by SN";
			this.rbSN.UseVisualStyleBackColor = true;
			// 
			// rbDate
			// 
			this.rbDate.Checked = true;
			this.rbDate.Location = new System.Drawing.Point(16, 14);
			this.rbDate.Name = "rbDate";
			this.rbDate.Size = new System.Drawing.Size(104, 24);
			this.rbDate.TabIndex = 27;
			this.rbDate.TabStop = true;
			this.rbDate.Text = "Search by Date";
			this.rbDate.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.rbDate);
			this.groupBox1.Controls.Add(this.rbSN);
			this.groupBox1.Location = new System.Drawing.Point(375, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(184, 75);
			this.groupBox1.TabIndex = 28;
			this.groupBox1.TabStop = false;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(597, 26);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 29;
			this.button2.Text = "Query";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// tBFlexMatch
			// 
			this.tBFlexMatch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.tBFlexMatch.Enabled = false;
			this.tBFlexMatch.Location = new System.Drawing.Point(68, 90);
			this.tBFlexMatch.MaxLength = 30;
			this.tBFlexMatch.Name = "tBFlexMatch";
			this.tBFlexMatch.Size = new System.Drawing.Size(206, 20);
			this.tBFlexMatch.TabIndex = 30;
			this.tBFlexMatch.Visible = false;
			// 
			// FormQualityReport
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(962, 634);
			this.Controls.Add(this.tBFlexMatch);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtSN);
			this.Controls.Add(this.lblto);
			this.Controls.Add(this.lblfrom);
			this.Controls.Add(this.dateTo);
			this.Controls.Add(this.dateFrom);
			this.Name = "FormQualityReport";
			this.Text = "FormQualityReport";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton rbDate;
		private System.Windows.Forms.RadioButton rbSN;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSN;
		private System.Windows.Forms.Label lblto;
		private System.Windows.Forms.Label lblfrom;
		private System.Windows.Forms.DateTimePicker dateTo;
		private System.Windows.Forms.DateTimePicker dateFrom;
		private System.Windows.Forms.DataGridViewTextBoxColumn Source;
		private System.Windows.Forms.TextBox tBFlexMatch;
	}
}

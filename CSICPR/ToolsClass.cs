﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using org.in2bits.MyXls;

namespace CSICPR
{
    class ToolsClass
    {
        #region"Declare Parameters"
        //FormAnalyse Parameters
        public const int iDateReport = 1;//report by date
        public const int iCartonReport = 2;//report by carton
        public const int iModuleReport = 3;//report by module
        private const int iSerial_Length = 3;
        private const int iDate_Length = 3;
        public const string ReportByCarton = "Report By Carton";
        public const string ReportByDate = "Report By Date";
        public const string ReportByModule = "Report By Module";
        private const string fileName = "Trace";
        private const string NormalMsg = "NORMAL";
        private const string AbnormalMsg = "ABNORMAL";
        private const string WorkShop = "C";//Canadian solar factory
        #endregion

        public enum DateInterval
        {
            Second, Minute, Hour, Day, Week, Month, Quarter, Year
        }
        public static int DateDiff(DateInterval Interval, System.DateTime StartDate, System.DateTime EndDate)
        {
            int lngDateDiffValue = 0;
            System.TimeSpan TS = new System.TimeSpan(EndDate.Ticks - StartDate.Ticks);
            switch (Interval)
            {
                case DateInterval.Second:
                    lngDateDiffValue = (int)TS.TotalSeconds;
                    break;
                case DateInterval.Minute:
                    lngDateDiffValue = (int)TS.TotalMinutes;
                    break;
                case DateInterval.Hour:
                    lngDateDiffValue = (int)TS.TotalHours;
                    break;
                case DateInterval.Day:
                    lngDateDiffValue = (int)TS.Days;
                    break;
                case DateInterval.Week:
                    lngDateDiffValue = (int)(TS.Days / 7);
                    break;
                case DateInterval.Month:
                    lngDateDiffValue = (int)(TS.Days / 30);
                    break;
                case DateInterval.Quarter:
                    lngDateDiffValue = (int)((TS.Days / 30) / 3);
                    break;
                case DateInterval.Year:
                    lngDateDiffValue = (int)(TS.Days / 365);
                    break;
            }
            return (lngDateDiffValue);
        }
        
        //2011-05-24|add by alex.dong|for Label string compare
        public static bool Like(string strText, string strPattern)
        {
            strPattern = strPattern.Replace("*", @"\w*");
            return System.Text.RegularExpressions.Regex.IsMatch(strText, strPattern);
        }
        
        #region"XML"
        //Initial/Save XML parameters
        public static void setConfig(string sData)//(int CartonQty, int IdealPower, int MixCount, int PrintMode,int LabelFormat,string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule,string MixChecked,string CheckNOChecked)
        {
            string xmlFile = Application.StartupPath + "\\ParametersSetting.xml";
            string[] sDataSet = sData.Split('|');

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode parentsNode = doc.CreateElement("Parameters");
            doc.AppendChild(parentsNode);

            subNode(doc, parentsNode, "CartonQty", sDataSet[0]);
            subNode(doc, parentsNode, "IdealPower", sDataSet[1]);
            subNode(doc, parentsNode, "MixCount", sDataSet[2], "Checked", sDataSet[7]);
            subNode(doc, parentsNode, "CheckRule", sDataSet[6], "Checked", sDataSet[8]);
            subNode(doc, parentsNode, "ModuleLabelPath", sDataSet[3]);
            subNode(doc, parentsNode, "CartonLabelPath", sDataSet[4]);
            subNode(doc, parentsNode, "CartonLabelParameter", sDataSet[5]);

            subNode(doc, parentsNode, "OnePrint", sDataSet[9]);
            subNode(doc, parentsNode, "BatchPrint", sDataSet[10]);
            subNode(doc, parentsNode, "NoPrint", sDataSet[11]);
            subNode(doc, parentsNode, "LargeLabelPrint", sDataSet[12]);		//modified by Shen Yu July 15, 2011
            subNode(doc, parentsNode, "Model", sDataSet[13]);
            subNode(doc, parentsNode, "RealPower", sDataSet[14]);
            subNode(doc, parentsNode, "NominalPower", sDataSet[15]);
            //subNode(doc, parentsNode, "IsUseCurrentCreateBoxID", sDataSet[15]);

            subNode(doc, parentsNode, "StationNo", sDataSet[16]);
            subNode(doc, parentsNode, "SerialCode", "1");		//modified by Shen Yu on July 15, 2011
//          subNode(doc, parentsNode, "ColorCode", "C");		//modified by Shen Yu on July 15, 2011

            subNode(doc, parentsNode, "AutoMode", sDataSet[17]);
            subNode(doc, parentsNode, "ManualMode", sDataSet[18]);
            subNode(doc, parentsNode, "PrintCount", sDataSet[19]);
            subNode(doc, parentsNode, "SystemDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            subNode(doc, parentsNode, "ModuleClass", "A");          //add by alex.dong 2011-07-22
            
//          subNode(doc, parentsNode, "LoadSize", sDataSet[20]);	//added by Shen Yu on Aug 19, 2011
//          subNode(doc, parentsNode, "LoadSequentialNumber", "1");	//added by Shen Yu on Aug 19, 2011
            try
            {
                doc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "ParametersSetting.xml";
            string xmlFile = Application.StartupPath + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {
                MessageBox.Show("Please Inform Manager to Config Package Parameters Before Packing", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }
        public static XmlNode subNode(XmlDocument doc, XmlNode ParentNode, string sNodeName, string sValue, string sAttributeName = "",string sAttribute = "")
        {
            XmlNode subNode = doc.CreateElement(sNodeName);
            if (sAttribute != "")
            {
                XmlAttribute subAttribute = doc.CreateAttribute(sAttributeName);
                subAttribute.Value = sAttribute;
                subNode.Attributes.Append(subAttribute);
            }
            if (sValue != "")
                subNode.AppendChild(doc.CreateTextNode(sValue));
            ParentNode.AppendChild(subNode);
            return subNode;
        }
        public static void saveXMLNode(string snode, string value = "", bool isAttribute = false, string sAttributeName = "")
        {
            string xmlFile = Application.StartupPath + "\\ParametersSetting.xml";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile);
            XmlNodeList nodeList = xmlDoc.SelectSingleNode("Parameters").ChildNodes;
            foreach (XmlNode xn in nodeList)
            {
                XmlElement xe = (XmlElement)xn;
                if (xe.Name == snode)
                {
                    if (isAttribute)
                    {
                        xe.SetAttribute(sAttributeName, value);
                        break;
                    }
                    else
                    {
                        xe.InnerText = value;
                        break;
                    }
                }
            }
            try
            {
                xmlDoc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region"Sql Execute"
		public static string NEWPACKINGConnStr()
        {
            string dbServer = getConfig("NewPackdbserver", false, "", "config.xml");
            string dbName = getConfig("NewPackdbName", false, "", "config.xml");
            string dbUser = getConfig("NewPackdbUser", false, "", "config.xml");
            string dbPwd = getConfig("NewPackdbPwd", false, "", "config.xml");

            string connStringBase = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, dbName, dbUser, dbPwd);
            return connStringBase;
        }	
        
        public static System.Data.DataTable GetDataTable(string SqlStr, System.Data.DataTable ds = null)
        {
            if (ds == null)
                ds = new DataTable();
            else
                ds.Clear();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                //cmd.Connection = conn;
                using (System.Data.SqlClient.SqlDataAdapter ada = new System.Data.SqlClient.SqlDataAdapter())
                {
                    ada.SelectCommand = cmd;
                    try
                    {
                        ada.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return ds;
        }
        
        public static bool GradeIfBFClass(string sn)
        {
            string sql = @"select ModuleClass from ( 
                          select *, ROW_NUMBER() over (partition by sn order by	ID desc) as RN from QualityJudge
                          ) Q where RN = 1  AND sn = '" + sn + "' AND ModuleClass IN ('B','F')";

           DataTable dt =  ToolsClass.PullData(sql);
           if(dt!= null && dt.Rows.Count > 0)
           {
                DialogResult dialogResult = MessageBox.Show("Current Status of (SN#) "+sn+" is class "+dt.Rows[0][0].ToString()+", Do you still want to change it to class A??", "Warning", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    return true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    return false;
                }
            }
           
            return true;
        }
       
        public static SqlDataReader GetDataReader(string SqlStr,string sDBConn="")
        {
            SqlDataReader dr = null;
            string ConnStr = "";
            if (sDBConn == "" || sDBConn == null)
                ConnStr = FormCover.connectionBase;
            else
                ConnStr = sDBConn;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                    dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dr = null;
                }
               
            }
            return dr;
        }
        
        public static SqlDataReader GetDataReaderMES(string SqlStr)
        {
            SqlDataReader dr = null;
            string ConnStr = "";
           
            ConnStr = "Data Source=CA01A0047;Initial Catalog=CAPA01DB01;User ID=reader;Password=12345;";
          
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                    dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dr = null;
                }
               
            }
            return dr;
        }
       
        public static int ExecuteNonQuery(string sqlStr, string sDBConn = "")
        {
            int line = 0;
            string ConnStr = "";
            if (sDBConn == "" || sDBConn == null)
                ConnStr = FormCover.connectionBase;
            else
                ConnStr = sDBConn;

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    line = cmd.ExecuteNonQuery();

                    conn.Close();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                return line;
            }
        }

        public static int ExecuteQuery(string sqlStr)
        {
            int line = 0;
            SqlDataReader dr = null;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    dr = cmd.ExecuteReader();
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            line++;
                        }
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                return line;
            }
        }
       
        public static void saveSQL(System.Windows.Forms.TextBox tb, string xmlFile = "config.xml")
        {
            xmlFile = Application.StartupPath + "\\" + xmlFile;
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNode node = doc.DocumentElement.FirstChild.SelectSingleNode("//" + tb.Name);
            if (node == null)
            {
                XmlElement elem = doc.CreateElement(tb.Name);
                XmlText text = doc.CreateTextNode(tb.Text);
                node = doc.DocumentElement.AppendChild(elem);
                node.AppendChild(text);
            }

            try
            {
                doc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public static System.Data.DataTable PullData(string query,string connString = "")
    	{		 	
		 	try{
		    		DataTable dt = new DataTable();
                    SqlConnection conn;
                    if ( string.IsNullOrEmpty( connString) )
                        conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase);
                    else
                        conn = new System.Data.SqlClient.SqlConnection(connString);

                SqlCommand cmd = new SqlCommand(query, conn);
       				conn.Open();

       				 // create data adapter
        			SqlDataAdapter da = new SqlDataAdapter(cmd);
        			// this will query your database and return the result to your datatable
       			 	da.Fill(dt);
        			conn.Close();
        			da.Dispose();
        			return dt;
		    	}
		    	catch (Exception e)
		    	{
		    		throw e;		    		
		    	}
    	}        
      
        public static string getSQL(string xmlNode, string xmlFile = "config.xml")
        {
            xmlFile = Application.StartupPath + "\\" + xmlFile;
            if (System.IO.File.Exists(xmlFile))
            {
                try
                {
                    using (XmlReader reader = XmlReader.Create(xmlFile))//XmlReader reader = new XmlNodeReader(doc) //XmlReader reader = XmlReader.Create(textBox1.Text)
                    {
                        if (reader.ReadToFollowing(xmlNode))
                        {
                            xmlNode = reader.ReadElementContentAsString();// ReadInnerXml();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Read Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Config File is not exist", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return xmlNode;
        }
        #endregion       

        #region"Import Data"
        public static string CsiToFlexSN(string FlexSN)
        {
            string sql = @"select CsiSN from CsiToFlexSN where FlexSN = '" + FlexSN + "'";
            DataTable dt = ToolsClass.PullData(sql, ToolsClass.NEWPACKINGConnStr());
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

        public static string CsiToGrape(string GrapeSN)
        {
            string sql = @"select CsiSN from CsiToGrapeSN where GrapeSn = '" + GrapeSN + "'";
            DataTable dt = ToolsClass.PullData(sql, ToolsClass.NEWPACKINGConnStr());
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

        public static void DataToExcel(DataGridView m_DataView,bool flag=true)
        {
            int rowscount = m_DataView.Rows.Count;
            int colscount = m_DataView.Columns.Count;
            //var xlApp = new Microsoft.Office.Interop.Excel.Application();
             
            //行数列数必须大于0    
            if (rowscount <= 0 || colscount <= 0)
            {
                MessageBox.Show("No data can be saved", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //行数不可以大于65536    
            if (rowscount > 65536)
            {
                if (MessageBox.Show("Records too much(can not exceed 65536 rows)，Excel maybe can not open! Continue?", "Prompting Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                    return;
            }

            //列数不可以大于255    
            if (colscount > 255)
            {
                MessageBox.Show("Records too much(can not exceed 255 columns),can not save file！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Save Excel File";
            sfd.Filter = "Excel File(*.xls)|*.xls";
            //sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.DefaultExt = "xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string FileName = sfd.FileName;
                XlsDocument xls = new XlsDocument();//创建空xls文档
                xls.FileName = FileName;
                Worksheet sheet = xls.Workbook.Worksheets.AddNamed("Report"); //创建一个工作页为Report                
               
                //设定列宽度--Carton Number、Class、Module Number
                ColumnInfo colInfo0 = new ColumnInfo(xls, sheet);
                colInfo0.ColumnIndexStart = 0;
                colInfo0.ColumnIndexEnd = 0;
                colInfo0.Width = 15 * 256;
                sheet.AddColumnInfo(colInfo0);

                ColumnInfo colInfo1 = new ColumnInfo(xls, sheet);
                colInfo1.ColumnIndexStart = 1;
                colInfo1.ColumnIndexEnd = 1;
                colInfo1.Width = 25 * 256;
                sheet.AddColumnInfo(colInfo1);

                ColumnInfo colInfo2 = new ColumnInfo(xls, sheet);
                colInfo2.ColumnIndexStart = 2;
                colInfo2.ColumnIndexEnd = 2;
                colInfo2.Width = 18 * 256;
                sheet.AddColumnInfo(colInfo2);

                ColumnInfo colInfo3 = new ColumnInfo(xls, sheet);
                colInfo3.ColumnIndexStart = 3;
                colInfo3.ColumnIndexEnd = 3;
                colInfo3.Width = 10 * 256;
                sheet.AddColumnInfo(colInfo3);

                ColumnInfo colInfo8 = new ColumnInfo(xls, sheet);
                colInfo8.ColumnIndexStart = 8;
                colInfo8.ColumnIndexEnd = 8;
                colInfo8.Width = 12 * 256;
                sheet.AddColumnInfo(colInfo8);
                //创建列
                Cells cells = sheet.Cells; //获得指定工作页列集合

                int intRows = m_DataView.Rows.Count;
                int intColumns = m_DataView.Columns.Count;
                for (int i = 0; i < intColumns; i++)
                {
                    if (m_DataView.Columns[i].HeaderText.Replace(" ", "").Length > 0)
                    {
                        //创建列表头
                        Cell title = cells.Add(1, i+1, m_DataView.Columns[i].HeaderText);
                        title.HorizontalAlignment = HorizontalAlignments.Centered;
                        title.VerticalAlignment = VerticalAlignments.Centered;
                    }
                }

                for (int i = 0; i < intRows; i++)
                {
                    for (int j = 0; j < intColumns; j++)
                    {
                        if (m_DataView.Rows[i].Cells[j].Value == null || m_DataView.Rows[i].Cells[j].Value == DBNull.Value)
                        {
                            //range = (Microsoft.Office.Interop.Excel.Range)worksheet.Cells[i + 2, j + 1];
                            //range.NumberFormatLocal = "@";
                            //worksheet.Cells[i + 2, j + 1] = "";// m_DataView.Rows[i].Cells[j].Value.ToString();
                            //range.EntireColumn.AutoFit();
                            //.Borders.LineStyle = 1;
                            //range.Borders.get_Item(Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {
                            Cell value = cells.Add(i + 2, j + 1, m_DataView.Rows[i].Cells[j].Value.ToString());
                            if (m_DataView.Rows[i].Cells[j].Value.ToString().Length > 7)
                                value.Format = StandardFormats.Text;
                            else
                                cells.Add(i + 2, j + 1, m_DataView.Rows[i].Cells[j].Value);//value.Format = StandardFormats.Decimal_2;
                            value.HorizontalAlignment = HorizontalAlignments.Left;
                            value.VerticalAlignment = VerticalAlignments.Centered;
                            
                        }
                    }
                }
                try
                {
                    xls.Save(true);
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message, "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }
        #endregion
    }
    #region"utility class"
    class Security
    {
        public Security()
        {

        }

        //默认密钥向量
        private static byte[] Keys = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
        /// <summary>
        /// DES加密字符串
        /// </summary>
        /// <param name="encryptString">待加密的字符串</param>
        /// <param name="encryptKey">加密密钥,要求为8位</param>
        /// <returns>加密成功返回加密后的字符串，失败返回源串</returns>
        public static string EncryptDES(string encryptString, string encryptKey)
        {
            try
            {
                byte[] rgbKey = System.Text.Encoding.UTF8.GetBytes(encryptKey.Substring(0, 8));
                byte[] rgbIV = Keys;
                byte[] inputByteArray = System.Text.Encoding.UTF8.GetBytes(encryptString);
                DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey,rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Convert.ToBase64String(mStream.ToArray());
            }
            catch
            {
                return encryptString;
            }
        }

        /// <summary>
        /// DES解密字符串
        /// </summary>
        /// <param name="decryptString">待解密的字符串</param>
        /// <param name="decryptKey">解密密钥,要求为8位,和加密密钥相同</param>
        /// <returns>解密成功返回解密后的字符串，失败返源串</returns>
        public static string DecryptDES(string decryptString, string decryptKey)
        {
            try
            {
                byte[] rgbKey = System.Text.Encoding.UTF8.GetBytes(decryptKey);
                byte[] rgbIV = Keys;
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider DCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey,rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return System.Text.Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return decryptString;
            }
        }

    }


    //class for chinese and english exchange
    class languagechange
    {
        private static string frmname;
        private static Form frm;
        private static ArrayList list;
        private static string filepath = "D:\\Language.ini";//can be config
        private static int dtemp = 0;
        //private static Boolean flag = false;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileInt(string section, string key, int def, string filePath);

        public languagechange(string fname, Form form)
        {
            frmname = fname;
            frm = form;
            readConfigFile();
        }

        public void readConfigFile()
        {
            list = new ArrayList();
            int i = 0, len;
            string str = "";

            len = GetPrivateProfileInt(frmname, "TagCount", -1, filepath);
            for (i = 0; i <= len - 1; i++)
            {
                StringBuilder temp = new StringBuilder(500);
                int j = GetPrivateProfileString(frmname, i.ToString(), "", temp, 500, filepath);
                str = temp.ToString();
                list.Add(str);
            }
        }

        public static void writeConfigFile(Boolean flag)
        {
            WritePrivateProfileString("Setting", "Language", flag.ToString().ToLower(), filepath);
        }

        public static Boolean readSettingConfigFile()
        {
            Boolean flag = false;
            StringBuilder temp = new StringBuilder(500);
            GetPrivateProfileString("Setting", "Language", "", temp, 500, filepath);
            if (temp.ToString().ToUpper() == "ENG")
                flag = true;
            else
                flag = false;
            return flag;
        }

        //public static void checkType(Boolean languageflag)
        //{

        //}

        public static void changefrmName(Boolean languageflag)
        {
            for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
            {
                if (list[dtemp].ToString().Contains("FRM"))
                {
                    string[] sSet = list[dtemp].ToString().Split(',');
                    if (languageflag)
                        frm.Text = sSet[2];
                    else
                        frm.Text = sSet[1];
                    return;
                }
            }
        }

        public static void changeBTN(Boolean languageflag)
        {
            foreach (Control col in frm.Controls)
            {
                 if (col.GetType() != typeof(Button)) continue;
                 for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
                 {
                     if (list[dtemp].ToString().Contains(((Button)col).Tag.ToString()))
                    {
                         string[] sSet=list[dtemp].ToString().Split(',');
                         if (languageflag)
                             col.Text = sSet[2];
                         else
                             col.Text = sSet[1];
                    }
                }
            }
        }

        public static void changeLBL(Boolean languageflag)
        {
            foreach (Control col in frm.Controls)
            {
                if (col.GetType() != typeof(Label)) continue;
                for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
                {
                    if (list[dtemp].ToString().Contains(((Label)col).Tag.ToString()))
                    {
                        string[] sSet = list[dtemp].ToString().Split(',');
                        if (languageflag)
                            col.Text = sSet[2];
                        else
                            col.Text = sSet[1];
                    }
                }
            }
        }

        public static void changeDataGrid(Boolean languageflag)
        {
            int i = 0;
            foreach (Control col in frm.Controls)
            {
                if (col.GetType() != typeof(DataGridView)) continue;
                for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
                {
                    for (i = 0; i < ((DataGridView)col).Columns.Count; i++)
                    {
                        if (list[dtemp].ToString().Contains(((DataGridView)col).Columns[i].Name.ToString()))
                        {
                            string[] sSet = list[dtemp].ToString().Split(',');
                            if (languageflag)
                                ((DataGridView)col).Columns[i].HeaderText = sSet[2];
                            else
                                ((DataGridView)col).Columns[i].HeaderText = sSet[1];
                        }
                    }
                }
            }
        }

        public static void changeToolStrip(Boolean languageflag)
        {
        }

        public static void changeStatusStrip(Boolean languageflag)
        {
        }

        public static void changeRadioBTN(Boolean languageflag)
        {
        }

        public static void changeCheckBox(Boolean languageflag)
        {
        }

        public static void changeGroupBox(Boolean languageflag)
        {
        }
    }

    #endregion
}

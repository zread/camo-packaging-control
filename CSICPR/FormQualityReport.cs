﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 3/16/2012
 * Time: 2:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
	public partial class FormQualityReport : Form
	{		
		public FormQualityReport()
		{
			InitializeComponent();
			this.dateFrom.Value = DateTime.Today;
            this.dateTo.Value = DateTime.Today.AddDays(1);
		}
		
		//--------------This is to ensure only one instance of this form is loaded
		private static FormQualityReport theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormQualityReport();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
			
		void Button2Click(object sender, EventArgs e)
		{
			string sql = "";
			if(rbDate.Checked == true)
			{
				sql = "select * from QualityJudge where timestamp >= '"+dateFrom.Value.ToString("yyyy-MM-dd HH:mm:ss")+"'and timestamp <= '"+dateTo.Value.ToString("yyyy-MM-dd HH:mm:ss")+"' and operator <> 'admin'";
			}
			else
			{
				sql = "select * from QualityJudge where sn = '"+txtSN.Text+"' and operator <> 'admin'";
			}
			
			dataGridView1.Rows.Clear();
			
			
			SqlDataReader reader = ToolsClass.GetDataReader(sql);
			while(reader.Read())
			{
				dataGridView1.Rows.Insert(dataGridView1.RowCount - 1, new object[] { reader.GetValue(1), reader.GetValue(2), reader.GetValue(3), reader.GetValue(4), reader.GetValue(5), reader.GetValue(10), reader.GetValue(6)});
			}
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			ToolsClass.DataToExcel(dataGridView1, true);
		}
		void DataGridView1CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
	
		}
		
		private void txtSN_TextChanged(object sender, EventArgs e)
        {
            string SN = "";
            if (txtSN.Text.Trim().Length < 1)
                return;
            if (txtSN.Text.Trim().Substring(0, 1) == "e" || txtSN.Text.Trim().Substring(0, 1) == "E")
            {
            	if (txtSN.Text.Trim().Length < 22)
                    return;
                else
                {
                    SN = ToolsClass.CsiToFlexSN(txtSN.Text.Trim());
                    tBFlexMatch.Text = txtSN.Text;
                    txtSN.Text = SN;
                }
                tBFlexMatch.Visible = true;
            }
            else if (txtSN.Text.Trim().Substring(0, 1) == "G")
            {
                 if (txtSN.Text.Trim().Length < 21)
                     return;
                 else
                 {
                     SN = ToolsClass.CsiToGrape(txtSN.Text.Trim());
                     tBFlexMatch.Text = txtSN.Text;
                     txtSN.Text = SN;
                 }
                 tBFlexMatch.Visible = true;
             }
            else
            {
                tBFlexMatch.Visible = false;
                SN = this.txtSN.Text.Trim();
            }
        }
	}
}
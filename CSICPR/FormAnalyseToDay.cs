﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormAnalyseToDay : Form
    {
        DataTable prodDateSource;

        public FormAnalyseToDay()
        {
            InitializeComponent();
           
            prodDateSource = new DataTable();
            upSourcd();
            //tbSQL.Text = "SELECT [Product].[BoxID],[Product].[SN] FROM (SELECT distinct [SN] FROM [Process] WHERE [Process]='T5' AND  [ProcessDate]>'{0}' AND [ProcessDate]<'{1}') AS pd1 inner join [Product] on pd1.[SN]=[Product].[SN] where len([Product].[BoxID])>0 order by [Product].[BoxID]";
            tbCPTPackingQuerySQL.Text = ToolsClass.getSQL(tbCPTPackingQuerySQL.Name);
            //dataGridView1.Rows.Add(new string[] { "061038219B","0","0610421051469","224.0"});
            
        }

       
        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }
        private void upSourcd()
        {
            //清除当前显示内容
            dataGridView1.Rows.Clear();
            //调整表头结构
            int cols = (int)numericUpDown1.Value - ((dataGridView1.ColumnCount - 3) / 2);
            if (cols > 0)
            {
                DataGridViewColumn col, col2;
                for (int i = 0; i < cols; i++)
                {
                    col = new DataGridViewColumn(dataGridView1.Columns[2].CellTemplate);//dataGridView1[2, 0]
                    col2 = new DataGridViewColumn(dataGridView1.Columns[3].CellTemplate);
                    col.HeaderText = dataGridView1.Columns[2].HeaderText;
                    col.Width = dataGridView1.Columns[2].Width;
                    col.DefaultCellStyle = dataGridView1.Columns[2].DefaultCellStyle;
                    col2.HeaderText = dataGridView1.Columns[3].HeaderText;
                    col2.Width = dataGridView1.Columns[3].Width;
                    col2.DefaultCellStyle = dataGridView1.Columns[3].DefaultCellStyle;
                    dataGridView1.Columns.Insert(dataGridView1.ColumnCount - 1, col);
                    dataGridView1.Columns.Insert(dataGridView1.ColumnCount - 1, col2);
                }
            }
            else if (cols < 0)
            {
                for (int i = 0; i > cols; i--)
                {
                    dataGridView1.Columns.RemoveAt(dataGridView1.ColumnCount - 2);
                    dataGridView1.Columns.RemoveAt(dataGridView1.ColumnCount - 2);
                }
            }
            //显示数据
            //if(prodDateSource 
            string boxID = "", boxIDnew = "", pdcSN = "", pdcSNnew = "";
            int rowNum = 0,reNum=0;
            foreach (DataRow row in prodDateSource.Rows)
            {
                pdcSNnew = row[2].ToString();
                if (pdcSN == pdcSNnew)
                {
                    reNum++;
                    continue;
                }
                else
                    pdcSN = pdcSNnew;
                boxIDnew = row[0].ToString();
                if (boxID == boxIDnew)
                {
                    if (cols == numericUpDown1.Value)
                    {
                        rowNum = dataGridView1.Rows.Add();
                        dataGridView1.Rows[rowNum].Cells[2].Value = row[2];//+ ",";
                        dataGridView1.Rows[rowNum].Cells[3].Value = row[3];
                        cols = 1;
                    }
                    else
                    {
                        dataGridView1.Rows[rowNum].Cells[2 + cols * 2].Value = row[2];// +",";
                        dataGridView1.Rows[rowNum].Cells[3 + cols * 2].Value = row[3];
                        cols++; 
                    }
                }
                else
                {
                    rowNum = dataGridView1.Rows.Add();
                    boxID = boxIDnew;
                    dataGridView1.Rows[rowNum].Cells[0].Value = row[0];
                    dataGridView1.Rows[rowNum].Cells[1].Value = row[1];
                    dataGridView1.Rows[rowNum].Cells[2].Value = row[2];// +",";
                    dataGridView1.Rows[rowNum].Cells[3].Value = row[3];
                    cols = 1;
                }
            }
            this.Cursor = System.Windows.Forms.Cursors.Default;
            label2.Text = "个产品，" + "共查询到" + (prodDateSource.Rows.Count - reNum) + "个产品";
        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //tbSQL.Text = numericUpDown1.Value.ToString();
            if (this.Cursor == System.Windows.Forms.Cursors.WaitCursor)
            {
                MessageBox.Show("正在处理数据，请稍候！", "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                upSourcd();
            }
        }

        private void btOutpu_Click(object sender, EventArgs e)
        {
            ToolsClass.DataToExcel(dataGridView1);
        }

        private void label14_DoubleClick(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;
        }

        private void btQuery_Click(object sender, EventArgs e)
        {
            
            if (backgroundWorker1.IsBusy)
            {
                MessageBox.Show("正在处理数据，请稍候！", "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //Cursor = new Cursor(CSICPR.Properties.Resources.LoadSoftList.GetHicon());
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                backgroundWorker1.RunWorkerAsync();
            }
            
        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //更新显示
            upSourcd();
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string ToStr = dateTo.Value.ToString();
            ToStr = ToStr.Remove(ToStr.LastIndexOf(':'));
            string fromStr = dateFrom.Value.ToString();
            fromStr = fromStr.Remove(fromStr.LastIndexOf(':'));
            string sqlStr = string.Format(tbCPTPackingQuerySQL.Text, fromStr, ToStr);
            //获取数据
            ToolsClass.GetDataTable(sqlStr, prodDateSource);

        }

        private void btSaveSQL_Click(object sender, EventArgs e)
        {
            //保存组件装箱报表sql语句 tbCPTBoxSQL
            ToolsClass.saveSQL(tbCPTPackingQuerySQL);
            splitContainer1.Panel2Collapsed = true;
        }

        private void btCancelSQL_Click(object sender, EventArgs e)
        {
            tbCPTPackingQuerySQL.Undo();
        }

        private void FormAnalyseToDay_Load(object sender, EventArgs e)
        {
            //if (null == theSingleton || theSingleton.IsDisposed)
            //{
            //    theSingleton = new FormAnalyseToDay();
            //    if (queryToDay)
            //    { theSingleton.ShowDialog(); }
            //    else
            //    {
            //        theSingleton.MdiParent = fm;
            //        theSingleton.WindowState = FormWindowState.Maximized;
            //        theSingleton.Show();
            //    }
            //}
            //else
            //    theSingleton.Activate();
            //if (queryToDay)
            {
                this.dateFrom.Value = DateTime.Today;
                this.dateTo.Value = DateTime.Today.AddDays(1);
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                if (!this.backgroundWorker1.IsBusy) this.backgroundWorker1.RunWorkerAsync();
            }
        }

    }
}

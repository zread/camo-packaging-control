﻿namespace CSICPR.Report
{
    partial class frmOverallReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnQuery = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cmbWattage = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbClassA2 = new System.Windows.Forms.CheckBox();
            this.cbClassA1 = new System.Windows.Forms.CheckBox();
            this.cbClassA = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbMono = new System.Windows.Forms.CheckBox();
            this.cbPoly = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.cSILabelPrintDBDataSet = new CSICPR.CSILabelPrintDBDataSet();
            this.cSILabelPrintDBDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.proc_Overall_BoxTableAdapter = new CSICPR.CSILabelPrintDBDataSetTableAdapters.Proc_Overall_BoxTableAdapter();
            this.procOverallBoxBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cSILabelPrintDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSILabelPrintDBDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.procOverallBoxBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rptViewer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(885, 472);
            this.panel2.TabIndex = 3;
            // 
            // rptViewer
            // 
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DS_Overall";
            reportDataSource1.Value = this.procOverallBoxBindingSource;
            this.rptViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.rptViewer.LocalReport.ReportEmbeddedResource = "CSICPR.Report.Overall_Report.rdlc";
            this.rptViewer.Location = new System.Drawing.Point(0, 0);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.Size = new System.Drawing.Size(885, 472);
            this.rptViewer.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(885, 48);
            this.panel1.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnQuery);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(742, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(143, 48);
            this.panel8.TabIndex = 5;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(24, 15);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(71, 23);
            this.btnQuery.TabIndex = 11;
            this.btnQuery.Text = "Query";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.cmbWattage);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(603, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(139, 48);
            this.panel7.TabIndex = 4;
            // 
            // cmbWattage
            // 
            this.cmbWattage.FormattingEnabled = true;
            this.cmbWattage.Location = new System.Drawing.Point(77, 17);
            this.cmbWattage.Name = "cmbWattage";
            this.cmbWattage.Size = new System.Drawing.Size(53, 20);
            this.cmbWattage.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "Wattages:";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.groupBox2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(488, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(115, 48);
            this.panel6.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbClassA2);
            this.groupBox2.Controls.Add(this.cbClassA1);
            this.groupBox2.Controls.Add(this.cbClassA);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 48);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Class";
            // 
            // cbClassA2
            // 
            this.cbClassA2.AutoSize = true;
            this.cbClassA2.Location = new System.Drawing.Point(76, 20);
            this.cbClassA2.Name = "cbClassA2";
            this.cbClassA2.Size = new System.Drawing.Size(36, 16);
            this.cbClassA2.TabIndex = 10;
            this.cbClassA2.Text = "A2";
            this.cbClassA2.UseVisualStyleBackColor = true;
            // 
            // cbClassA1
            // 
            this.cbClassA1.AutoSize = true;
            this.cbClassA1.Location = new System.Drawing.Point(39, 20);
            this.cbClassA1.Name = "cbClassA1";
            this.cbClassA1.Size = new System.Drawing.Size(36, 16);
            this.cbClassA1.TabIndex = 8;
            this.cbClassA1.Text = "A1";
            this.cbClassA1.UseVisualStyleBackColor = true;
            // 
            // cbClassA
            // 
            this.cbClassA.AutoSize = true;
            this.cbClassA.Location = new System.Drawing.Point(8, 20);
            this.cbClassA.Name = "cbClassA";
            this.cbClassA.Size = new System.Drawing.Size(30, 16);
            this.cbClassA.TabIndex = 9;
            this.cbClassA.Text = "A";
            this.cbClassA.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(369, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(119, 48);
            this.panel5.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbMono);
            this.groupBox1.Controls.Add(this.cbPoly);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(119, 48);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ChipType";
            // 
            // cbMono
            // 
            this.cbMono.AutoSize = true;
            this.cbMono.Location = new System.Drawing.Point(63, 20);
            this.cbMono.Name = "cbMono";
            this.cbMono.Size = new System.Drawing.Size(48, 16);
            this.cbMono.TabIndex = 7;
            this.cbMono.Text = "Mono";
            this.cbMono.UseVisualStyleBackColor = true;
            // 
            // cbPoly
            // 
            this.cbPoly.AutoSize = true;
            this.cbPoly.Location = new System.Drawing.Point(11, 20);
            this.cbPoly.Name = "cbPoly";
            this.cbPoly.Size = new System.Drawing.Size(48, 16);
            this.cbPoly.TabIndex = 6;
            this.cbPoly.Text = "Poly";
            this.cbPoly.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dateTo);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(188, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(181, 48);
            this.panel4.TabIndex = 1;
            // 
            // dateTo
            // 
            this.dateTo.CustomFormat = "yyyy-M-d";
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTo.Location = new System.Drawing.Point(64, 13);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(109, 21);
            this.dateTo.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "End Date:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.dateFrom);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(188, 48);
            this.panel3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "Start Date:";
            // 
            // dateFrom
            // 
            this.dateFrom.CustomFormat = "yyyy-M-d";
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateFrom.Location = new System.Drawing.Point(74, 13);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(109, 21);
            this.dateFrom.TabIndex = 3;
            // 
            // cSILabelPrintDBDataSet
            // 
            this.cSILabelPrintDBDataSet.DataSetName = "CSILabelPrintDBDataSet";
            this.cSILabelPrintDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cSILabelPrintDBDataSetBindingSource
            // 
            this.cSILabelPrintDBDataSetBindingSource.DataSource = this.cSILabelPrintDBDataSet;
            this.cSILabelPrintDBDataSetBindingSource.Position = 0;
            // 
            // proc_Overall_BoxTableAdapter
            // 
            this.proc_Overall_BoxTableAdapter.ClearBeforeFill = true;
            // 
            // procOverallBoxBindingSource
            // 
            this.procOverallBoxBindingSource.DataMember = "Proc_Overall_Box";
            this.procOverallBoxBindingSource.DataSource = this.cSILabelPrintDBDataSetBindingSource;
            // 
            // frmOverallReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 520);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmOverallReport";
            this.Text = "Overall Report";
            this.Load += new System.EventHandler(this.frmOverallReport_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cSILabelPrintDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSILabelPrintDBDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.procOverallBoxBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox cmbWattage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbClassA2;
        private System.Windows.Forms.CheckBox cbClassA1;
        private System.Windows.Forms.CheckBox cbClassA;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbMono;
        private System.Windows.Forms.CheckBox cbPoly;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;
        private System.Windows.Forms.BindingSource cSILabelPrintDBDataSetBindingSource;
        private CSILabelPrintDBDataSet cSILabelPrintDBDataSet;
        private CSILabelPrintDBDataSetTableAdapters.Proc_Overall_BoxTableAdapter proc_Overall_BoxTableAdapter;
        private System.Windows.Forms.BindingSource procOverallBoxBindingSource;
        //private Overall_DataSetTableAdapters.Proc_Overall_BoxTableAdapter proc_Overall_BoxTableAdapter;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR.Report
{
    public partial class frmOverallReport : Form
    {
        public frmOverallReport(Form fm)
        {
            InitializeComponent();
            this.MdiParent = fm;
            this.WindowState = FormWindowState.Maximized;
            this.Show();
        }

        private void frmOverallReport_Load(object sender, EventArgs e)
        {
            this.dateFrom.Value = DateTime.Today;
            this.dateTo.Value = DateTime.Today.AddDays(1);

            this.cmbWattage.Items.Add("All");
            string sWattageSql = "select ItemValue from Item where ItemClassInterID=2 order by cast(ItemValue  as int) asc";
            using (System.Data.SqlClient.SqlDataReader reader = ToolsClass.GetDataReader(sWattageSql))
            {
                while (reader != null && reader.Read())
                {
                    this.cmbWattage.Items.Add(reader.GetString(0));
                }
            }
            this.cmbWattage.Items.Add("Mixed");
            //this.rptViewer.RefreshReport();
            //this.rptViewer.RefreshReport();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            string sDateFrom = null, sDateTo = null;
            string sChipTypeP = null, sChipTypeM = null;
            string sGradeA = null, sGradeA1 = null, sGradeA2 = null;
            string sWattage = null;

            this.proc_Overall_BoxTableAdapter.Connection.ConnectionString = FormCover.connectionBase;
            sDateFrom = this.dateFrom.Value.ToString("yyyy-MM-dd");
            sDateTo = this.dateTo.Value.ToString("yyyy-MM-dd");
            if (this.cbPoly.Checked)
                sChipTypeP = "P";
            if (this.cbMono.Checked)
                sChipTypeM = "M";
            if (this.cbClassA.Checked)
                sGradeA = "A";
            if (this.cbClassA1.Checked)
                sGradeA1 = "A1";
            if (this.cbClassA2.Checked)
                sGradeA2 = "A2";
            //try
            //{
            sWattage = this.cmbWattage.Text.Trim();
            if (sWattage.ToUpper().Equals("ALL") || sWattage.Trim().Equals(""))
                sWattage=null;
            //}
            //catch
            //{
            //    iWattage = 0;
            //}
            try
            {
                this.proc_Overall_BoxTableAdapter.Fill(this.cSILabelPrintDBDataSet.Proc_Overall_Box, sDateFrom, sDateTo, sChipTypeP, sChipTypeM, sGradeA, sGradeA1, sGradeA2, sWattage);
                    //(this.cSILabelPrintDBDataSet.Proc_Overall_Box, sDateFrom, sDateTo, sChipTypeP, sChipTypeM, sGradeA, sGradeA1, sGradeA2, sWattage);
                this.rptViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void rptViewer_Load(object sender, EventArgs e)
        {

        }
    }
}

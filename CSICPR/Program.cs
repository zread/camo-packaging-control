﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.Xml;
using System.Net;
using System.Data;
using System.Threading;

namespace CSICPR
{
    internal sealed class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            checkIfOpend();
            checkUpdates();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }
        public static Mutex objMutex;
        private static void checkIfOpend()
        {
            objMutex = new Mutex(false, "CSICRP_Mutex");

            if (objMutex.WaitOne(0, false) == false)
            {
                objMutex.Close();
                objMutex = null;
                MessageBox.Show("Cannot start the program twice!! Process End!!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                System.Environment.Exit(0);
            }
        }

        private static void checkUpdates()
        {
            Version newVersion = null;
            string xmlurl = "";

            xmlurl = @"\\10.127.34.15\AutoUpdate\update.xml";

            XmlTextReader reader = null;
            try
            {
                reader = new XmlTextReader(xmlurl);

                reader.MoveToContent();
                string elementName = "";
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "CSICPR"))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            elementName = reader.Name;
                        }
                        else
                        {
                            if ((reader.NodeType == XmlNodeType.Text) && (reader.HasValue))
                            {
                                switch (elementName)
                                {
                                    case "version":
                                        newVersion = new Version(reader.Value);
                                        break;
                                    case "url":
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Exception caught: " + e.Message.ToString());
                Environment.Exit(1);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            Version applicationVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

            if (applicationVersion.CompareTo(newVersion) < 0)
            {

                DialogResult dialogResult = MessageBox.Show("The update is avalible, click yes to update", "System update detected", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start("CPRAutoUpdate.exe");
                    Application.Exit();

                }
                else if (dialogResult == DialogResult.No)
                {
                   
                    System.Environment.Exit(1);

                }
            }
            else
            {
                return;
            }

        }

    }
}

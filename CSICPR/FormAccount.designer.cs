﻿namespace CSICPR
{
    partial class FormAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bSave = new System.Windows.Forms.Button();
            this.btQuit = new System.Windows.Forms.Button();
            this.tbDBServer = new System.Windows.Forms.TextBox();
            this.tbDBpwd = new System.Windows.Forms.TextBox();
            this.tbDBUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btTestConn = new System.Windows.Forms.Button();
            this.tbDBName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bSave
            // 
            this.bSave.Enabled = false;
            this.bSave.Location = new System.Drawing.Point(239, 165);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(68, 28);
            this.bSave.TabIndex = 5;
            this.bSave.Tag = "BTN0003";
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // btQuit
            // 
            this.btQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btQuit.Location = new System.Drawing.Point(239, 77);
            this.btQuit.Name = "btQuit";
            this.btQuit.Size = new System.Drawing.Size(68, 28);
            this.btQuit.TabIndex = 6;
            this.btQuit.Tag = "BTN0001";
            this.btQuit.Text = "Return";
            this.btQuit.UseVisualStyleBackColor = true;
            // 
            // tbDBServer
            // 
            this.tbDBServer.Location = new System.Drawing.Point(79, 80);
            this.tbDBServer.MaxLength = 50;
            this.tbDBServer.Name = "tbDBServer";
            this.tbDBServer.Size = new System.Drawing.Size(121, 21);
            this.tbDBServer.TabIndex = 0;
            this.tbDBServer.WordWrap = false;
            this.tbDBServer.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbDBServer.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // tbDBpwd
            // 
            this.tbDBpwd.Location = new System.Drawing.Point(79, 140);
            this.tbDBpwd.MaxLength = 25;
            this.tbDBpwd.Name = "tbDBpwd";
            this.tbDBpwd.Size = new System.Drawing.Size(121, 21);
            this.tbDBpwd.TabIndex = 2;
            this.tbDBpwd.UseSystemPasswordChar = true;
            // 
            // tbDBUser
            // 
            this.tbDBUser.Location = new System.Drawing.Point(79, 111);
            this.tbDBUser.MaxLength = 25;
            this.tbDBUser.Name = "tbDBUser";
            this.tbDBUser.Size = new System.Drawing.Size(121, 21);
            this.tbDBUser.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(21, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 9;
            this.label3.Tag = "LBL0003";
            this.label3.Text = "Password:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(21, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 10;
            this.label2.Tag = "LBL0004";
            this.label2.Text = "Database:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(27, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 8;
            this.label1.Tag = "LBL0002";
            this.label1.Text = "Account:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(33, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 7;
            this.label4.Tag = "LBL0001";
            this.label4.Text = "Server:";
            // 
            // btTestConn
            // 
            this.btTestConn.Location = new System.Drawing.Point(239, 129);
            this.btTestConn.Name = "btTestConn";
            this.btTestConn.Size = new System.Drawing.Size(68, 28);
            this.btTestConn.TabIndex = 4;
            this.btTestConn.Tag = "BTN0002";
            this.btTestConn.Text = "Test";
            this.btTestConn.UseVisualStyleBackColor = true;
            this.btTestConn.Click += new System.EventHandler(this.btTestConn_Click);
            // 
            // tbDBName
            // 
            this.tbDBName.Location = new System.Drawing.Point(79, 171);
            this.tbDBName.MaxLength = 25;
            this.tbDBName.Name = "tbDBName";
            this.tbDBName.Size = new System.Drawing.Size(121, 21);
            this.tbDBName.TabIndex = 3;
            // 
            // FormAccount
            // 
            this.AcceptButton = this.bSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.btQuit;
            this.ClientSize = new System.Drawing.Size(324, 208);
            this.Controls.Add(this.tbDBName);
            this.Controls.Add(this.btTestConn);
            this.Controls.Add(this.tbDBUser);
            this.Controls.Add(this.tbDBpwd);
            this.Controls.Add(this.tbDBServer);
            this.Controls.Add(this.btQuit);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAccount";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "FRM0001";
            this.Text = "Database Setting";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button btQuit;
        private System.Windows.Forms.TextBox tbDBServer;
        private System.Windows.Forms.TextBox tbDBpwd;
        private System.Windows.Forms.TextBox tbDBUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btTestConn;
        private System.Windows.Forms.TextBox tbDBName;
    }
}
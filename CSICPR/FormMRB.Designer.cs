﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 09/11/2015
 * Time: 3:35 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CSICPR
{
	partial class FormMRB
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.comboBox3 = new System.Windows.Forms.ComboBox();
			this.comboBox4 = new System.Windows.Forms.ComboBox();
			this.comboBox5 = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.comboBox6 = new System.Windows.Forms.ComboBox();
			this.comboBox7 = new System.Windows.Forms.ComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.label18 = new System.Windows.Forms.Label();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.Reset = new System.Windows.Forms.Button();
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.button5 = new System.Windows.Forms.Button();
			this.label19 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.stringCB1 = new System.Windows.Forms.ComboBox();
			this.stringCB2 = new System.Windows.Forms.ComboBox();
			this.stringCB3 = new System.Windows.Forms.ComboBox();
			this.celltb3 = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.celltb1 = new System.Windows.Forms.TextBox();
			this.celltb2 = new System.Windows.Forms.TextBox();
			this.tBFlexMatch = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(14, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 29);
			this.label1.TabIndex = 0;
			this.label1.Text = "Serial No";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(95, 11);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(121, 20);
			this.textBox1.TabIndex = 1;
			this.textBox1.TextChanged += new System.EventHandler(this.TextBox1TextChanged);
			this.textBox1.Enter += new System.EventHandler(this.Color_Change);
			this.textBox1.Leave += new System.EventHandler(this.Color_Back);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(14, 60);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 18);
			this.label2.TabIndex = 2;
			this.label2.Text = "Class";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(14, 101);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(61, 29);
			this.label3.TabIndex = 3;
			this.label3.Text = "MRB";
			// 
			// comboBox1
			// 
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
			"A",
			"A-",
			"B",
			"F"});
			this.comboBox1.Location = new System.Drawing.Point(95, 59);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(121, 21);
			this.comboBox1.TabIndex = 4;
			// 
			// comboBox2
			// 
			this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox2.FormattingEnabled = true;
			this.comboBox2.Items.AddRange(new object[] {
			"LAB",
			"MRB",
			"RWK",
			"QA",
			"HP RWK"});
			this.comboBox2.Location = new System.Drawing.Point(95, 98);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(121, 21);
			this.comboBox2.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(14, 139);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(83, 16);
			this.label4.TabIndex = 6;
			this.label4.Text = "Stage";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(14, 209);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 18);
			this.label5.TabIndex = 7;
			this.label5.Text = "Defect Category";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(14, 253);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 18);
			this.label6.TabIndex = 8;
			this.label6.Text = "Specific Defect";
			// 
			// comboBox3
			// 
			this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox3.FormattingEnabled = true;
			this.comboBox3.Items.AddRange(new object[] {
			"Backsheet_Defect",
			"Backsheet_or_EVA_-_Blue_Tape",
			"Backsheet_Slit_Defects",
			"Backsheet_not_covering_glass",
			"Broken_Laminates_or_Modules",
			"Busbar_Showing",
			"Bubbles",
			"Bussbar_Defects",
			"Cell_damage",
			"Cell_Spacing",
			"Cosmetic_Defects",
			"Engineering_or_Lab_Analysis",
			"Final_Test_Failures",
			"Foreign_Material",
			"Frame_Joint",
			"Frame_Damage_or_Defect",
			"Overbaked_or_Underbaked_laminates",
			"Part_Orientation",
			"Product_Configuration",
			"Ribbon_Misaligned",
			"Ribbon_Twisted",
			"Scratch_on_Glass",
			"Serialization_and_Barcodes",
			"Silicone_or_Adhesive_Defects",
			"Solder_Splatter",
			"Supplier_Defects",
			"Uncut_Ribbon_or_Busbar",
			"EL_Reject",
			"Flex Module cosmetic defect",
			"Other"});
			this.comboBox3.Location = new System.Drawing.Point(95, 206);
			this.comboBox3.Name = "comboBox3";
			this.comboBox3.Size = new System.Drawing.Size(191, 21);
			this.comboBox3.TabIndex = 10;
			this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.ComboBox3SelectedIndexChanged);
			// 
			// comboBox4
			// 
			this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox4.FormattingEnabled = true;
			this.comboBox4.Items.AddRange(new object[] {
			"Laminate",
			"Module"});
			this.comboBox4.Location = new System.Drawing.Point(95, 137);
			this.comboBox4.Name = "comboBox4";
			this.comboBox4.Size = new System.Drawing.Size(121, 21);
			this.comboBox4.TabIndex = 9;
			// 
			// comboBox5
			// 
			this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox5.FormattingEnabled = true;
			this.comboBox5.Location = new System.Drawing.Point(95, 250);
			this.comboBox5.Name = "comboBox5";
			this.comboBox5.Size = new System.Drawing.Size(191, 21);
			this.comboBox5.TabIndex = 11;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(14, 299);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 23);
			this.label7.TabIndex = 12;
			this.label7.Text = "Remarks";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(95, 296);
			this.textBox2.Multiline = true;
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(413, 90);
			this.textBox2.TabIndex = 13;
			this.textBox2.Enter += new System.EventHandler(this.Color_Change);
			this.textBox2.Leave += new System.EventHandler(this.Color_Back);
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(273, 14);
			this.textBox3.Name = "textBox3";
			this.textBox3.ReadOnly = true;
			this.textBox3.Size = new System.Drawing.Size(73, 20);
			this.textBox3.TabIndex = 15;
			this.textBox3.TextChanged += new System.EventHandler(this.Color__Change);
			this.textBox3.Enter += new System.EventHandler(this.Color_Change);
			this.textBox3.Leave += new System.EventHandler(this.Color_Back);
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(225, 62);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(61, 14);
			this.label8.TabIndex = 16;
			this.label8.Text = "Model";
			// 
			// comboBox6
			// 
			this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox6.FormattingEnabled = true;
			this.comboBox6.Items.AddRange(new object[] {
			"6X",
			"6P",
			"6U",
			"6K",
			"6C",
			"6F"});
			this.comboBox6.Location = new System.Drawing.Point(273, 59);
			this.comboBox6.Name = "comboBox6";
			this.comboBox6.Size = new System.Drawing.Size(73, 21);
			this.comboBox6.TabIndex = 17;
			// 
			// comboBox7
			// 
			this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox7.FormattingEnabled = true;
			this.comboBox7.Items.AddRange(new object[] {
			"SD_1",
			"SN_1",
			"SD_2",
			"SN_2",
			"UNK"});
			this.comboBox7.Location = new System.Drawing.Point(406, 59);
			this.comboBox7.Name = "comboBox7";
			this.comboBox7.Size = new System.Drawing.Size(79, 21);
			this.comboBox7.TabIndex = 19;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(361, 62);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(42, 17);
			this.label9.TabIndex = 18;
			this.label9.Text = "Shifts";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(225, 101);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(61, 18);
			this.label10.TabIndex = 20;
			this.label10.Text = "String_1";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(361, 102);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(42, 17);
			this.label11.TabIndex = 22;
			this.label11.Text = "Cell_1";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(361, 140);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(42, 15);
			this.label12.TabIndex = 26;
			this.label12.Text = "Cell_2";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(225, 140);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(61, 15);
			this.label13.TabIndex = 24;
			this.label13.Text = "String_2";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(225, 17);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(39, 21);
			this.label14.TabIndex = 33;
			this.label14.Text = "Line";
			// 
			// label17
			// 
			this.label17.BackColor = System.Drawing.SystemColors.HotTrack;
			this.label17.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label17.Location = new System.Drawing.Point(1038, 56);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(271, 35);
			this.label17.TabIndex = 34;
			this.label17.Text = "Batch Grade";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(1040, 94);
			this.textBox4.Multiline = true;
			this.textBox4.Name = "textBox4";
			this.textBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox4.Size = new System.Drawing.Size(269, 406);
			this.textBox4.TabIndex = 35;
			this.textBox4.Enter += new System.EventHandler(this.Color_Change);
			this.textBox4.Leave += new System.EventHandler(this.Color_Back);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(162, 518);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 36;
			this.button2.Text = "Check";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(348, 518);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 37;
			this.button3.Text = "Grade";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(1136, 518);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 38;
			this.button4.Text = "Batch Grade";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Button4Click);
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(361, 20);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(57, 23);
			this.label18.TabIndex = 39;
			this.label18.Text = "Picture";
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(406, 17);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(69, 20);
			this.textBox5.TabIndex = 40;
			this.textBox5.Enter += new System.EventHandler(this.Color_Change);
			this.textBox5.Leave += new System.EventHandler(this.Color_Back);
			// 
			// Reset
			// 
			this.Reset.Location = new System.Drawing.Point(855, 518);
			this.Reset.Name = "Reset";
			this.Reset.Size = new System.Drawing.Size(75, 23);
			this.Reset.TabIndex = 41;
			this.Reset.Text = "Reset";
			this.Reset.UseVisualStyleBackColor = true;
			this.Reset.Click += new System.EventHandler(this.ResetClick);
			// 
			// dataGrid1
			// 
			this.dataGrid1.AlternatingBackColor = System.Drawing.SystemColors.ActiveBorder;
			this.dataGrid1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.dataGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
			this.dataGrid1.DataMember = "";
			this.dataGrid1.ForeColor = System.Drawing.Color.YellowGreen;
			this.dataGrid1.GridLineColor = System.Drawing.SystemColors.ControlDark;
			this.dataGrid1.HeaderBackColor = System.Drawing.SystemColors.ControlDark;
			this.dataGrid1.HeaderForeColor = System.Drawing.Color.Cornsilk;
			this.dataGrid1.LinkColor = System.Drawing.SystemColors.InactiveCaption;
			this.dataGrid1.Location = new System.Drawing.Point(0, 0);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.ParentRowsBackColor = System.Drawing.SystemColors.ControlDark;
			this.dataGrid1.ParentRowsForeColor = System.Drawing.Color.Yellow;
			this.dataGrid1.SelectionBackColor = System.Drawing.Color.AliceBlue;
			this.dataGrid1.Size = new System.Drawing.Size(1679, 555);
			this.dataGrid1.TabIndex = 1;
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.Color.BlanchedAlmond;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Column1,
			this.Column2,
			this.Column3,
			this.Column4});
			this.dataGridView1.GridColor = System.Drawing.Color.Crimson;
			this.dataGridView1.Location = new System.Drawing.Point(541, 94);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.Size = new System.Drawing.Size(493, 406);
			this.dataGridView1.TabIndex = 42;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "SN";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Class";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column2.Width = 50;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Remark";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.Width = 150;
			// 
			// Column4
			// 
			this.Column4.HeaderText = "DateTime";
			this.Column4.Name = "Column4";
			this.Column4.ReadOnly = true;
			this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column4.Width = 150;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(616, 518);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(75, 23);
			this.button5.TabIndex = 43;
			this.button5.Text = "History";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Button5Click);
			// 
			// label19
			// 
			this.label19.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label19.ForeColor = System.Drawing.Color.Transparent;
			this.label19.Location = new System.Drawing.Point(541, 56);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(493, 35);
			this.label19.TabIndex = 45;
			this.label19.Text = "QualityJudge History Tracking";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label19.UseCompatibleTextRendering = true;
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label20.ForeColor = System.Drawing.Color.Transparent;
			this.label20.Location = new System.Drawing.Point(9, 56);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(529, 35);
			this.label20.TabIndex = 46;
			this.label20.Text = "Single Grade";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label20.UseCompatibleTextRendering = true;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.stringCB1);
			this.panel1.Controls.Add(this.stringCB2);
			this.panel1.Controls.Add(this.stringCB3);
			this.panel1.Controls.Add(this.celltb3);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.celltb1);
			this.panel1.Controls.Add(this.celltb2);
			this.panel1.Controls.Add(this.tBFlexMatch);
			this.panel1.Controls.Add(this.textBox5);
			this.panel1.Controls.Add(this.label18);
			this.panel1.Controls.Add(this.label14);
			this.panel1.Controls.Add(this.label12);
			this.panel1.Controls.Add(this.label13);
			this.panel1.Controls.Add(this.label11);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.comboBox7);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.comboBox6);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.textBox3);
			this.panel1.Controls.Add(this.textBox2);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.comboBox5);
			this.panel1.Controls.Add(this.comboBox3);
			this.panel1.Controls.Add(this.comboBox4);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.comboBox2);
			this.panel1.Controls.Add(this.comboBox1);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.textBox1);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(9, 96);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(529, 404);
			this.panel1.TabIndex = 48;
			// 
			// stringCB1
			// 
			this.stringCB1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.stringCB1.FormattingEnabled = true;
			this.stringCB1.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.stringCB1.Location = new System.Drawing.Point(273, 99);
			this.stringCB1.Name = "stringCB1";
			this.stringCB1.Size = new System.Drawing.Size(73, 21);
			this.stringCB1.TabIndex = 54;
			// 
			// stringCB2
			// 
			this.stringCB2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.stringCB2.FormattingEnabled = true;
			this.stringCB2.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.stringCB2.Location = new System.Drawing.Point(273, 136);
			this.stringCB2.Name = "stringCB2";
			this.stringCB2.Size = new System.Drawing.Size(73, 21);
			this.stringCB2.TabIndex = 53;
			// 
			// stringCB3
			// 
			this.stringCB3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.stringCB3.FormattingEnabled = true;
			this.stringCB3.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.stringCB3.Location = new System.Drawing.Point(273, 173);
			this.stringCB3.Name = "stringCB3";
			this.stringCB3.Size = new System.Drawing.Size(73, 21);
			this.stringCB3.TabIndex = 52;
			// 
			// celltb3
			// 
			this.celltb3.Location = new System.Drawing.Point(406, 173);
			this.celltb3.Name = "celltb3";
			this.celltb3.Size = new System.Drawing.Size(74, 20);
			this.celltb3.TabIndex = 51;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(361, 176);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(42, 15);
			this.label15.TabIndex = 49;
			this.label15.Text = "Cell_3";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(225, 176);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(61, 15);
			this.label16.TabIndex = 48;
			this.label16.Text = "String_3";
			// 
			// celltb1
			// 
			this.celltb1.Location = new System.Drawing.Point(406, 100);
			this.celltb1.Name = "celltb1";
			this.celltb1.Size = new System.Drawing.Size(74, 20);
			this.celltb1.TabIndex = 47;
			// 
			// celltb2
			// 
			this.celltb2.Location = new System.Drawing.Point(406, 137);
			this.celltb2.Name = "celltb2";
			this.celltb2.Size = new System.Drawing.Size(74, 20);
			this.celltb2.TabIndex = 46;
			// 
			// tBFlexMatch
			// 
			this.tBFlexMatch.Enabled = false;
			this.tBFlexMatch.Location = new System.Drawing.Point(95, 33);
			this.tBFlexMatch.Name = "tBFlexMatch";
			this.tBFlexMatch.Size = new System.Drawing.Size(169, 20);
			this.tBFlexMatch.TabIndex = 43;
			this.tBFlexMatch.Visible = false;
			// 
			// FormMRB
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDark;
			this.ClientSize = new System.Drawing.Size(1362, 742);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label20);
			this.Controls.Add(this.label19);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.Reset);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.dataGrid1);
			this.Name = "FormMRB";
			this.Text = "MRB Data";
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.TextBox celltb3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Button Reset;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox comboBox7;
		private System.Windows.Forms.ComboBox comboBox6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox comboBox5;
		private System.Windows.Forms.ComboBox comboBox4;
		private System.Windows.Forms.ComboBox comboBox3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tBFlexMatch;
        private System.Windows.Forms.TextBox celltb1;
        private System.Windows.Forms.TextBox celltb2;
        private System.Windows.Forms.ComboBox stringCB1;
        private System.Windows.Forms.ComboBox stringCB2;
        private System.Windows.Forms.ComboBox stringCB3;
    }
}

﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            toolTip1.IsBalloon = true;
            showCover(true);
            this.menuStrip1.MdiWindowListItem = this.menuHelp;
            toolStripStatusLabel4.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        }
        //public Mutex objMutex;
        private static ToolTip toolTip1 = new ToolTip();
        public static SolidBrush sbrush = new SolidBrush(SystemColors.MenuText);//ControlDarkDark
        public static Boolean languageflag = true;//add by alex.dong for language change
        
        public static EVA_BS_Rolls_Log EVA = null;
        public static Hoist_Checklist checklist = null;
        
        public static void showTipMSG(string msg, Control target, string title = "", ToolTipIcon icon = ToolTipIcon.None)
        {
            toolTip1.ToolTipTitle = title;
            toolTip1.ToolTipIcon = icon;
            toolTip1.SetToolTip(target, msg);
            toolTip1.Show(msg, target, 6000);
        }
        private void showCover(bool isLoading, bool isLock = false)
        {
            FormCover f2 = new FormCover(isLoading, isLock);
            f2.Owner = this;
            
            if (!isLoading) f2.Size = this.Size;
            f2.ShowDialog();
            this.tsslCurUserName.Text = FormCover.CurrUserName;
            this.tsslCurWorkshop.Text = ToolsClass.getConfig("StationNo").Replace('\n', ' ').Trim();
            //this.tslAccount.Text = BaseClass.currAccount;
            //this.tslRole.Text = BaseClass.currUserRole;
        }
        private void MenuItemClock_Click(object sender, EventArgs e)
        {
            //showCover(false, true);
        }

        private void MenuItemReStart_Click(object sender, EventArgs e)
        {
            //Application.Restart();
        }

        private void MenuItemQuit_Click(object sender, EventArgs e)
        {
            //this.Close();
        }

        private void tssbChart_ButtonClick(object sender, EventArgs e)
        {
            tssbChart.ShowDropDown();
        }

        private void MenuItemPackage_Click(object sender, EventArgs e)
        {            
        }

        private void tssbSystem_ButtonClick(object sender, EventArgs e)
        {
            tssbSystem.ShowDropDown();
        }

        private void menuStrip1_ItemAdded(object sender, ToolStripItemEventArgs e)
        {
            if (e.Item.Text.Length == 0 || e.Item.Text == " 还原(&R) " || e.Item.Text == " 最小化(&N) " || e.Item.Text == " Restore Down(&R) " || e.Item.Text == " MinimizeBox(&N) ")
            {
                e.Item.Visible = false;
            }
        }

        private void tsbWindowTile_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void tsbWindowHzt_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void tsbWindowVtcl_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void MenuItemDBServer_Click(object sender, EventArgs e)
        {
            //new FormAccount().ShowDialog();
        }

  

        private void accountManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormAccountManager().ShowDialog();
        }

        private void MenuItemBase_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower(sender.ToString()))
                new FormAccount().ShowDialog();
        }

        private void packageConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {           
        }

        private void tssbExit_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void packageReportByCartonToolStripMenuItem_Click(object sender, EventArgs e)
        {            
        }

        //Check Form is open in mdi paraent
        public bool isOpen(string strFormName,bool isActive=false)
        {
            for (int i = 0; i < this.MdiChildren.Length; i++)
            {
                if (this.MdiChildren[i].Text.Equals(strFormName))
                {
                    if (isActive)
                    {
                        this.MdiChildren[i].Activate();
                        if (this.MdiChildren[i].WindowState == FormWindowState.Minimized)
                            this.MdiChildren[i].WindowState = FormWindowState.Maximized;
                    }
                    return true;
                }
            }
            return false;
        }

        private void packageReportByModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            //objMutex = new Mutex(false, this.Text + "_Mutex");

            //if (objMutex.WaitOne(0, false) == false)
            //{
            //    objMutex.Close();
            //    objMutex = null;
            //    MessageBox.Show("Cannot start same form twice!! Process End!!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    this.Close();
            //}
        }

        
        void LoadReportByDateToolStripMenuItemClick(object sender, EventArgs e)
        {
        }
        
        void LoadReportByLoadToolStripMenuItemClick(object sender, EventArgs e)
        {
        }
        
        void LoadReportByCartonToolStripMenuItemClick(object sender, EventArgs e)
        {
        }
        
        void LoadReportByModuleToolStripMenuItemClick(object sender, EventArgs e)
        {
        }
        
        void ToolStripButton2Click(object sender, EventArgs e)
        {
        	if (FormCover.HasPower(toolStripButton2.Text))
        	{
        		FormQualityInspection.Instance(this);
        	}      	
        }
        
        void ToolStripButton1Click(object sender, EventArgs e)
        {
        	
        		FormAMinusMarking.Instance(this);
        	
        }
        
        
        void QualityReportToolStripMenuItemClick(object sender, EventArgs e)
        {
        	
        		FormQualityReport.Instance(this);
        	
        }         
        void ToolStripButton5Click(object sender, EventArgs e)
        {
        	if (FormCover.HasPower(toolStripButton5.Text))        
        	{
        		FormMRB.Instance(this);
        	}
        }
        
        void ToolStripButton6Click(object sender, EventArgs e)
        {
        	if (FormCover.HasPower(toolStripButton6.Text))        
        	{
        		ELDefects.Instance(this);
        	}
        }
		void ToolStripButton7Click(object sender, EventArgs e)
		{
			if (FormCover.HasPower(toolStripButton7.Text))        
        	{
        		FormStringerDefects.Instance(this);
        	}
		}

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower(toolStripButton3.Text))
            {
                FormRework.Instance(this);
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower(toolStripButton4.Text))
            {
                FormReworkQA.Instance(this);
            }
        }
		void ToolStripStatusLabel4Click(object sender, EventArgs e)
		{
	
		}
		void LineARollsClick(object sender, EventArgs e)
        {
            if (FormCover.HasPower(LineARolls.Text))
            {             
				if (null == EVA || EVA.IsDisposed)
	            {
	                EVA = new EVA_BS_Rolls_Log("A");
	                EVA.MdiParent = this;
	                EVA.WindowState = FormWindowState.Maximized;
	                EVA.Show();
	
	            }
	            else
	            {
	                EVA.Close();
	                EVA = new EVA_BS_Rolls_Log("A");
	                EVA.MdiParent = this;
	                EVA.WindowState = FormWindowState.Maximized;
	                EVA.Show();
	            }
            }
        }

        void LineBRollsClick(object sender, EventArgs e)
        {
        	if (FormCover.HasPower(LineBRolls.Text))
            {
	            if (null == EVA || EVA.IsDisposed)
	            {
	                EVA = new EVA_BS_Rolls_Log("B");
	                EVA.MdiParent = this;
	                EVA.WindowState = FormWindowState.Maximized;
	                EVA.Show();
	
	            }
	            else
	            {
	                EVA.Close();
	                EVA = new EVA_BS_Rolls_Log("B");
	                EVA.MdiParent = this;
	                EVA.WindowState = FormWindowState.Maximized;
	                EVA.Show();
	            }
        	}
        }

        void LineAHoistClick(object sender, EventArgs e)
        {
            if (FormCover.HasPower(lineAHoist.Text))
	            {
	        	if (null == checklist || checklist.IsDisposed)
	            {
	                checklist = new Hoist_Checklist("A");
	                checklist.MdiParent = this;
	                checklist.WindowState = FormWindowState.Maximized;
	                checklist.Show();
	
	            }
	            else
	            {                
	                checklist.Close();
	                checklist = new Hoist_Checklist("A");
	                checklist.MdiParent = this;
	                checklist.WindowState = FormWindowState.Maximized;
	                checklist.Show();
	            }
            }
        }

        void LineBHoistClick(object sender, EventArgs e)
        {
            if (FormCover.HasPower(lineBHoist.Text))
            {
            	if (null == checklist || checklist.IsDisposed)
	            {
	                checklist = new Hoist_Checklist("B");
	                checklist.MdiParent = this;
	                checklist.WindowState = FormWindowState.Maximized;
	                checklist.Show();
	
	            }
	            else
	            {                
	                checklist.Close();
	                checklist = new Hoist_Checklist("B");
	                checklist.MdiParent = this;
	                checklist.WindowState = FormWindowState.Maximized;
	                checklist.Show();
	            }
            }
        }
    }
}

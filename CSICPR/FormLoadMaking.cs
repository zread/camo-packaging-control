﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Data.SqlTypes;
using System.Data;


namespace CSICPR
{

	public partial class FormLoadMaking : Form
	{
		private int loadSize = Int32.Parse(ToolsClass.getConfig("LoadSize"));
		private bool loadMix = bool.Parse(ToolsClass.getConfig("LoadMix"));
		private string boxType = "";
		
		public FormLoadMaking()
		{
			InitializeComponent();
			GenerateLoadID();
		}
		
		private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }
		
		//--------------This is to ensure only one instance of this form is loaded
		private static FormLoadMaking theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormLoadMaking();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		//---------------
		
		void TbBarCode_KeyPress(object sender, KeyPressEventArgs e)
		{
	        if (e.KeyChar == '\r')
	        {
	            Thread.Sleep(100);
	            string strTemp = tbBarCode.Text;
	            string[] sDataSet = strTemp.Split('\n');
	            for (int i = 0; i < sDataSet.Length; i++)
	            {
	                sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
	                if (sDataSet[i] != null && sDataSet[i] != "")
	                {
	                    enterCarton(sDataSet[i],sender,e);
	                }
	            }
	            tbBarCode.Clear();
	        }
		}
		
		void enterCarton(string sCartonNumber, object sender, KeyPressEventArgs e)
		{		
			//check if load is full
			if (dataGridView1.RowCount == loadSize )
            {
				showError("Carton "+sCartonNumber+" did not enter because the load is full!", listView1);
                tbBarCode.Clear();
                return;
            }
			
			//check if this carton number has been entered before			
			foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[0].Value != null && row.Cells[0].Value.Equals(sCartonNumber))
                {
                	showError("Carton "+sCartonNumber+" is already in the list!", listView1);
                    tbBarCode.Clear();
                    return;
                }
            }
			
			//check if this carton number has already been made into a load			
			string sqlstr = "select [LoadID] from LoadDetail where BoxID = '"+sCartonNumber+"'";
			using (SqlDataReader reader = ToolsClass.GetDataReader(sqlstr))
			{
				if(reader.Read())
				{
					showError("Carton "+sCartonNumber+" has already been made into load "+reader.GetValue(0).ToString().Trim()+"", listView1);
					return;
				}
			}
			
			//get carton information and display it in dataGridView
			sqlstr = "select [BoxType], [Color], [BoxClass], [Number], CONVERT(varchar(10), [PackingDate], 101) from Box where boxID = '"+sCartonNumber+"'";
			using (SqlDataReader reader = ToolsClass.GetDataReader(sqlstr))
			{
				if (reader != null && reader.Read())
				{
					//check mixing
					if(loadMix == false)
					{
						if(dataGridView1.RowCount == 0)
						{
							boxType = reader.GetValue(0).ToString();
						}
						else
						{
							if(reader.GetValue(0).ToString() != boxType)
							{
								showError("Carton "+sCartonNumber+" is "+reader.GetValue(0).ToString()+", cannot be mixed with "+boxType+"!", listView1);
								return;
							}
						}
					}
					
					dataGridView1.Rows.Insert(0, new object[] { sCartonNumber, reader.GetValue(0), reader.GetValue(1), reader.GetValue(2), reader.GetValue(3), reader.GetValue(4)});
				}
				else
				{
					showError("Cannot find carton "+sCartonNumber+" in database!", listView1);
				}
			}
			
			//if load is full, enable "make load" button
			if(dataGridView1.RowCount == loadSize)
			{
				button1.Enabled = true;
			}
					
		}
		
		//"Make Load" button click
		void Button1Click(object sender, EventArgs e)
		{	
			if(dataGridView1.RowCount != loadSize)
			{
				showError("Load is not full!", listView1);
				button1.Enabled = false;
				return;
			}
			
			string conStr = FormCover.connectionBase;
		    using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(conStr))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                SqlTransaction transaction = null;
                DateTime dt = DateTime.Now;
                try
                {
                    if (conn.State == ConnectionState.Closed)
                    {
                    	conn.Open();
                    }
                    transaction = conn.BeginTransaction();
                    cmd.Transaction = transaction;
                    cmd.CommandText = "update Load set LoadStatus = 'Y' , TimeStamp = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' where LoadID = '" + this.tbBoxCode.Text.ToString() + "'";
                    cmd.ExecuteNonQuery();
                    for (int i = 1; i <= dataGridView1.RowCount; i++)
                    {
                    	cmd.CommandText = "insert into LoadDetail (LoadID, BoxID) values( '" + this.tbBoxCode.Text.ToString().Trim() + "','" + dataGridView1[0, i-1].Value.ToString().Trim() + "')";
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    if (transaction != null)
                    {
                    	transaction.Rollback();
                    }
                    showError("Making load fail! "+ex.Message.ToString(), listView1);
                    return;
                }
            }
			
			button1.Enabled = false;
			GenerateLoadID();
			dataGridView1.Rows.Clear();
		}

		public void GenerateLoadID()
		{
			string strsql="";
			string loadID = "";
			string temp = "100-";
			int sequentialNumber = int.Parse(ToolsClass.getConfig("LoadSequentialNumber"));
			
            try
            {
                DateTime dt1 = Convert.ToDateTime(ToolsClass.getConfig("SystemDate"));
                DateTime dt2 = DateTime.Now;
                if (dt1.Month != dt2.Month)
                {
                    ToolsClass.saveXMLNode("SystemDate", dt2.Date.ToString("yyyy-MM-dd HH:mm:ss"));
                    sequentialNumber = 1;
                }
                temp = temp + dt2.Year.ToString().Substring(2, 2) + dt2.Month.ToString().PadLeft(2, '0')+"-";
            }
            catch(Exception e)
            {
                MessageBox.Show("Get System Date Fail:"+e.Message, "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            while(true)
            {
            	if (sequentialNumber > 99)
                {
                    MessageBox.Show("Load Serial Number Has Run Out!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            	loadID = temp + sequentialNumber.ToString().PadLeft(2, '0') +"-"+ loadSize.ToString().PadLeft(2, '0');
            	loadID = loadID.Trim();
            	sequentialNumber++;
            	strsql = "select * from Load where LoadID='"+loadID+"'";
            	if (ToolsClass.ExecuteQuery(strsql) == 0)
            	{
            		break;
            	}
            }
            
           	this.tbBoxCode.Text = loadID;
           	ToolsClass.saveXMLNode("LoadSequentialNumber", sequentialNumber.ToString());
           	
           	//if this LoadID does not exist in Load Table, insert it into the Load Table.
           	strsql = "select * from Load where LoadID = '" + loadID + "'";
           	if(ToolsClass.ExecuteQuery(strsql) == 0)
           	{
           		strsql = "Insert into Load (LoadStatus, LoadID) values ('W', '"+loadID+"')";
            	ToolsClass.ExecuteNonQuery(strsql);
           	}
           	else
           	{
           		strsql = "Update Load set LoadStatus = 'W' where LoadID = '"+loadID+"'";
            	ToolsClass.ExecuteNonQuery(strsql);
           	}
		}
			
		void FormLoadMakingFormClosing(object sender, FormClosingEventArgs e)
		{
			int j = int.Parse(ToolsClass.getConfig("LoadSequentialNumber"));
			ToolsClass.saveXMLNode("LoadSequentialNumber", (--j).ToString());

			string strsql = "delete from Load where LoadStatus = 'W' and LoadID = '"+this.tbBoxCode.Text.ToString()+"'";
            ToolsClass.ExecuteNonQuery(strsql);
		}
		
		void TbBarCodeEnter(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.Color.Yellow;
		}
			
		void TbBarCodeLeave(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.SystemColors.Window;
		}
		
		void showError(String text, ListView listview)
		{
			ListViewItem Item = new ListViewItem();
            Item.SubItems.Clear();
            Item.SubItems[0].Text = text;
            Item.SubItems[0].ForeColor = System.Drawing.Color.Red;
            listview.Items.Add(Item);
            listview.Items[listview.Items.Count - 1].EnsureVisible();
		}

		
		void Button2Click(object sender, EventArgs e)
		{
			if (FormCover.HasPower(button2.Text))
			{
				new FormUnpackLoad();
			}
		}
		
	}
}
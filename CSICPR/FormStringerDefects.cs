﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 12-08-16
 * Time: 10:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace CSICPR
{
	/// <summary>
	/// Description of FormStringerDefects.
	/// </summary>
	public partial class FormStringerDefects : Form
	{
		public FormStringerDefects()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			cbMod.SelectedIndex = 0;
			AddDropDown(dt1CB);
			AddDropDown(dt2CB);
			AddDropDown(dt3CB);
			AddDropDown(dt4CB);
			AddDropDown(dt5CB);
			AddDropDown(dt6CB);
			AddDropDown(dt7CB);
			AddDropDown(dt8CB);
			AddDropDown(dt9CB);
			AddDropDown(dt10CB);
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		//--------------This is to ensure only one instance of this form is loaded
		private static FormStringerDefects theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormStringerDefects();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		void BtRepairClick(object sender, EventArgs e)
		{
			string sql = "";
			int numToRepair = 0;
			
			if(cbLine.Text != ""){
				sql = "Select Line" + cbLine.Text + " From DefectsToRepair";
				SqlDataReader dr = ToolsClass.GetDataReader(sql);	
				dr.Read();
				numToRepair = Convert.ToInt32(numRepair.Value);
				lbRepaired.Text = (dr.GetInt32(0) - numToRepair).ToString();
				sql = @"Update DefectsToRepair Set Line" + cbLine.Text + "  = " + numToRepair;
				ToolsClass.ExecuteNonQuery(sql);
				numRepair.Value = 0;
			}
			else
				MessageBox.Show("Ensure that the line has been selected.");
		}
		void BtSubmitClick(object sender, EventArgs e)
		{			
			if(cbStr1.Text == "Stringer" && cbLine.Text != "" && tbMO.Text != "" && cbStringer1.Text != "" && dt1CB.Text != "" && tbQTY1.Text != ""){				
				submityQuery(dt1CB.Text.Trim(), tbQTY1.Text.Trim(), cbC1.Text.Trim(), cbStringer1.Text.Trim());
				submityQuery(dt2CB.Text.Trim(), tbQTY2.Text.Trim(), cbC2.Text.Trim(), cbStringer2.Text.Trim());
				submityQuery(dt3CB.Text.Trim(), tbQTY3.Text.Trim(), cbC3.Text.Trim(), cbStringer3.Text.Trim());
				submityQuery(dt4CB.Text.Trim(), tbQTY4.Text.Trim(), cbC4.Text.Trim(), cbStringer4.Text.Trim());
				submityQuery(dt5CB.Text.Trim(), tbQTY5.Text.Trim(), cbC5.Text.Trim(), cbStringer5.Text.Trim());
				submityQuery(dt6CB.Text.Trim(), tbQTY6.Text.Trim(), cbC6.Text.Trim(), cbStringer6.Text.Trim());
				submityQuery(dt7CB.Text.Trim(), tbQTY7.Text.Trim(), cbC7.Text.Trim(), cbStringer7.Text.Trim());
				submityQuery(dt8CB.Text.Trim(), tbQTY8.Text.Trim(), cbC8.Text.Trim(), cbStringer8.Text.Trim());
				submityQuery(dt9CB.Text.Trim(), tbQTY9.Text.Trim(), cbC9.Text.Trim(), cbStringer9.Text.Trim());
				submityQuery(dt10CB.Text.Trim(), tbQTY10.Text.Trim(), cbC10.Text.Trim(), cbStringer10.Text.Trim());
				clearText();
			}
			else if(cbStr1.Text == "Bussing" && tbMO.Text != "" && cbLine.Text != "" && dt1CB.Text != "" && tbQTY1.Text != ""){				
				submityQuery(dt1CB.Text.Trim(), tbQTY1.Text.Trim(), cbC1.Text.Trim(), cbStringer1.Text.Trim());
				submityQuery(dt2CB.Text.Trim(), tbQTY2.Text.Trim(), cbC2.Text.Trim(), cbStringer2.Text.Trim());
				submityQuery(dt3CB.Text.Trim(), tbQTY3.Text.Trim(), cbC3.Text.Trim(), cbStringer3.Text.Trim());
				submityQuery(dt4CB.Text.Trim(), tbQTY4.Text.Trim(), cbC4.Text.Trim(), cbStringer4.Text.Trim());
				submityQuery(dt5CB.Text.Trim(), tbQTY5.Text.Trim(), cbC5.Text.Trim(), cbStringer5.Text.Trim());
				submityQuery(dt6CB.Text.Trim(), tbQTY6.Text.Trim(), cbC6.Text.Trim(), cbStringer6.Text.Trim());
				submityQuery(dt7CB.Text.Trim(), tbQTY7.Text.Trim(), cbC7.Text.Trim(), cbStringer7.Text.Trim());
				submityQuery(dt8CB.Text.Trim(), tbQTY8.Text.Trim(), cbC8.Text.Trim(), cbStringer8.Text.Trim());
				submityQuery(dt9CB.Text.Trim(), tbQTY9.Text.Trim(), cbC9.Text.Trim(), cbStringer9.Text.Trim());
				submityQuery(dt10CB.Text.Trim(), tbQTY10.Text.Trim(), cbC10.Text.Trim(), cbStringer10.Text.Trim());
				clearText();
				refreshTables();
			}
			else{
				MessageBox.Show("Missing information, please try again.");
			}			
		}
		void BtClearClick(object sender, EventArgs e)
		{
			clearText();
		}
		void CbStr1SelectedIndexChanged(object sender, EventArgs e)
		{
			if(cbStr1.Text == "Stringer"){
				snTB.Visible = false;
				label4.Visible = false;
			}
			else{
				snTB.Visible = true;
				label4.Visible = true;
			}
		}
		void submityQuery(string defect, string qty, string cause, string stringer){
			string sql = "";
			int numToRepair = 0;
			
			if(defect != "" && qty != "" && (stringer != "" || cbStr1.Text.Trim() == "Bussing") && textBox1.Text.Trim() == ""){
				sql = @"Insert into StringerDefects (Category, Line, MO, SN, Stringer, Defect, Qty, ModType, Remarks, DefectTime)
					Values ('"+cbStr1.Text.Trim()+"', '"+cbLine.Text.Trim()+"','"+tbMO.Text.Trim()+"', '"+snTB.Text.Trim()+"', '"+ stringer +
					"', '"+ defect +"','"+ qty +"', '"+ cbMod.Text.Trim() +"', '"+textBox2.Text.Trim()+"',getdate())";
				ToolsClass.ExecuteNonQuery(sql);
				sql = "Select Line" + cbLine.Text + " From DefectsToRepair";
				SqlDataReader dr = ToolsClass.GetDataReader(sql);	
				dr.Read();
				numToRepair = dr.GetInt32(0) + 1;
				sql = @"Update DefectsToRepair Set Line" + cbLine.Text + "  = " + numToRepair;
				ToolsClass.ExecuteNonQuery(sql);	
			}
			else if(defect != "" && qty != "" && (stringer != "" || cbStr1.Text.Trim() == "Bussing")){
				sql = @"Update StringerDefects Set Category = '"+cbStr1.Text.Trim()+"', Line = '"+cbLine.Text.Trim()+"', MO = '"+tbMO.Text.Trim()+"'," +
					"SN = '"+snTB.Text.Trim()+"', Stringer = '"+ stringer + "', Defect = '"+ defect +"', Qty = '"+ qty +"', ModType = '" + cbMod.Text.Trim() + "', " +
					"Remarks = '"+textBox2.Text.Trim()+"' Where id = '" + textBox1.Text.Trim() + "'";
				ToolsClass.ExecuteNonQuery(sql);
			}
			else{
				//MessageBox.Show("Missing information, please try again.");
			}			
		}
		
		void clearText(){
			cbLine.SelectedItem = null;
			tbMO.Text = "";
			tbQTY1.Text = "";
			tbQTY2.Text = "";
			tbQTY3.Text = "";
			tbQTY4.Text = "";
			tbQTY5.Text = "";
			tbQTY6.Text = "";
			tbQTY7.Text = "";
			tbQTY8.Text = "";
			tbQTY9.Text = "";
			tbQTY10.Text = "";
			cbStringer1.SelectedItem = null;
			snTB.Text ="";
			dt1CB.SelectedItem = null;
			dt2CB.SelectedItem = null;
			dt3CB.SelectedItem = null;
			dt4CB.SelectedItem = null;
			dt5CB.SelectedItem = null;
			dt6CB.SelectedItem = null;
			dt7CB.SelectedItem = null;
			dt8CB.SelectedItem = null;
			dt9CB.SelectedItem = null;
			dt10CB.SelectedItem = null;
			cbC1.SelectedItem = null;
			cbC2.SelectedItem = null;
			cbC3.SelectedItem = null;
			cbC4.SelectedItem = null;
			cbC5.SelectedItem = null;
			cbC6.SelectedItem = null;
			cbC7.SelectedItem = null;
			cbC8.SelectedItem = null;
			cbC9.SelectedItem = null;
			cbC10.SelectedItem = null;
			textBox2.Text = "";
		}		
		void AddDropDown(ComboBox list){
			list.Items.AddRange(new object[] {
			"Cracked/Chipped Cell Handling",
			"Cracked/Chipped Cell Machine",
			"Dead Cell",
			"Ribbon Misalignment",
			"String to String Gap",
			"Cell to Cell Gap",
			"Polarity",
			"False Reject",
			"Dark Spot",
			"Flux",
			"Blue Side Unsoldered",
			"Grey Side Unsoldered",
			"Missing Ribbon"});
		}
		
		void AddDropDownStringer(ComboBox list){
			list.Items.Clear();
			if(cbLine.Text == "A"){
				list.Items.AddRange(new object[] {
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"Unknown"});
			}
			else if(cbLine.Text == "B"){
				list.Items.AddRange(new object[] {
				"1",
				"2",
				"3",
				"4",
				"Unknown"});
			}
			else if(cbLine.Text == "D"){
				list.Items.AddRange(new object[] {
				"1",
				"2",
				"3",
				"4",
				"5",
				"Unknown"});
			}
		}
		void CbLineSelectedIndexChanged(object sender, EventArgs e)
		{
			AddDropDownStringer(cbStringer1);
			AddDropDownStringer(cbStringer2);
			AddDropDownStringer(cbStringer3);
			AddDropDownStringer(cbStringer4);
			AddDropDownStringer(cbStringer5);
			AddDropDownStringer(cbStringer6);
			AddDropDownStringer(cbStringer7);
			AddDropDownStringer(cbStringer8);
			AddDropDownStringer(cbStringer9);
			AddDropDownStringer(cbStringer10);
		}
		
		private void DataGridView2_CellClick(object sender, DataGridViewCellEventArgs e) {
			if (dataGridView2.Columns[e.ColumnIndex].Name == "Modify Entry") 
		    { 
		        DialogResult dialogResult = MessageBox.Show("Are you sure you want to modify this entry?", "Modify", MessageBoxButtons.YesNo);
				if(dialogResult == DialogResult.Yes)
				{
					textBox1.Text = dataGridView2[0, e.RowIndex].Value.ToString();
					cbStr1.Text = dataGridView2[1, e.RowIndex].Value.ToString();
					cbLine.Text = dataGridView2[2, e.RowIndex].Value.ToString();
					tbMO.Text = dataGridView2[3, e.RowIndex].Value.ToString();
					snTB.Text = dataGridView2[4, e.RowIndex].Value.ToString();	
					cbStringer1.Text = dataGridView2[5, e.RowIndex].Value.ToString();	
					dt1CB.Text = dataGridView2[6, e.RowIndex].Value.ToString();	
					tbQTY1.Text = dataGridView2[7, e.RowIndex].Value.ToString();	
					cbMod.Text = dataGridView2[8, e.RowIndex].Value.ToString();					
				}
		    } 
		}
		void BtRefreshClick(object sender, EventArgs e)
		{
			refreshTables();
		}
		
		void refreshTables(){
			#region SQL Query datagridview1
			String sql = @"Select Top 10 ID, Category, Line, MO, SN, Stringer, Defect, Qty, ModType, Remarks, DefectTime From StringerDefects Where Line = '" + 
				cbLine2.Text.Trim() + "' Order by ID Desc";
			#endregion			
			DataTable dt = ToolsClass.GetDataTable(sql);
			if(dt != null && dt.Rows.Count > 0)
			{
				dataGridView2.DataSource = dt;				
			}
			
			if(dataGridView2.Columns.Contains("Modify Entry"))
				dataGridView2.Columns.Remove("Modify Entry");
			var col = new DataGridViewButtonColumn();
			col.UseColumnTextForButtonValue = true;
			col.Text = "Modify";
			col.Name = "Modify Entry";
			dataGridView2.Columns.Add(col);
		}
	}
}

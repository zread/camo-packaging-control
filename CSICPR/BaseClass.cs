﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
//using System.Security.Cryptography;
//using Ionic.Zip;

namespace CSICPR
{
   /// <summary>
   /// 基础类，提供数据库访问和文件压缩等辅助方法。
   /// </summary>
    class BaseClass
    {
        private static System.Data.SqlClient.SqlClientFactory dbFactory = System.Data.SqlClient.SqlClientFactory.Instance;
        private static string connStringPrv ;
        private static string connStringBase ;
        private static string dbServer;
        private static string dbName;
        private static string dbPwd;
        private static string soapURL;
        private static string orgName;
        private static string orgCode;
        private static string orgPWD;
        private static string shipperCode;
        private static bool showAccept;
        private static int dPlaces, roundRule, printTicket, printerWidth;
        private static string address, tel, telSupport, rmk, store;
        /// <summary>
        /// 店铺地址
        /// </summary>
        public static string Address
        {
            get { return address; }
            set { address = value; }
        }
        /// <summary>
        /// 三级网点
        /// </summary>
        public static string Store
        {
            get { return store; }
            set { store = value; }
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        public static string Tel
        {
            get { return tel; }
            set { tel = value; }
        }
        /// <summary>
        /// 售后电话
        /// </summary>
        public static string TelSupport
        {
            get { return telSupport; }
            set { telSupport = value; }
        }
        /// <summary>
        /// 小票备注
        /// </summary>
         public static string RMK
        {
            get { return rmk; }
            set { rmk = value; }
        }
        /// <summary>
        /// 小数位数
        /// </summary>
        public static int parPlaces
        {
            get { return dPlaces; }
            set { dPlaces = value; }
        }
        /// <summary>
        /// 舍入规则:0为四舍五入,1为向上舍入,2为向下舍入
        /// </summary>
        public static int parRoundRule
        {
            get { return roundRule; }
            set { roundRule = value; }
        }
        /// <summary>
        /// 是否打印小票:0为不打印,1为打印
        /// </summary>
        public static int parPrintTicket
        {
            get { return printTicket; }
            set { printTicket = value; }
        }
        /// <summary>
        /// 打印纸张宽度:58为58mm,76为76mm,80为80mm
        /// </summary>
        public static int parPrinterWidth
        {
            get { return printerWidth; }
            set { printerWidth = value; }
        }
        /// <summary>
        /// 是否显示交接窗口
        /// </summary>
        public static bool ShowAccept
        {
            set { showAccept = value; }
            get { return showAccept; }
        }
        /// <summary>
        /// 销售公司名称
        /// </summary>
        public static string OrgName
        {
            set { orgName = value; }
            get { return orgName; }
        }
        /// <summary>
        /// 销售公司帐号
        /// </summary>
        public static string OrgCode
        {
            set { orgCode = value; }
            get { return orgCode; }
        }
        /// <summary>
        /// 经销商帐号
        /// </summary>
        public static string ShipperCode
        {
            set { shipperCode = value; }
            get { return shipperCode; }
        }
        /// <summary>
        /// 销售公司密码
        /// </summary>
        public static string OrgPWD
        {
            set { orgPWD = value; }
            get { return orgPWD; }
        }
        /// <summary>
        /// 在线数据交换地址
        /// </summary>
        public static string SOAPURL
        {
            set { soapURL = value; }
            get { return soapURL; }
        }
        /// <summary>
        /// 数据库服务器
        /// </summary>
        public static string DBServer
        {
            set { dbServer = value; }
            get { return dbServer; }
        }
        /// <summary>
        /// 数据库账号
        /// </summary>
        public static string DBName
        {
            set { dbName = value; }
            get { return dbName; }
        }
        /// <summary>
        /// 数据库密码
        /// </summary>
        public static string DBPWD
        {
            set { dbPwd = value; }
            get { return dbPwd; }
        }
        /// <summary>
        /// 基础资料库连接字符串
        /// </summary>
        public static string connectionBase
        {
            get { return connStringBase; }
            set { connStringBase = value; }
        }
        /// <summary>
        /// 账套业务连接字符串
        /// </summary>
        public static string connectionPrivate
        {
            get { return connStringPrv; }
            //set { connStringPrv = value; }
        }
        //private static int timeOut;
        private static string _name;//姓名
        private static string workid;//工号
        //private static string _time;//登录时间
        private static bool isManager;
        private static string _role;//角色
        private static string account;//帐套名
        private static string act;//帐套
        private static List<string> allRight;
        /// <summary>
        /// 是否管理员
        /// </summary>
        public static bool currUserIsManager
        {
            get
            {
                return isManager;// "admin".Equals(workid, StringComparison.CurrentCultureIgnoreCase);
            }
        }
        /// <summary>
        /// 当前用户名
        /// </summary>
        public static string currUserName
        {
            get
            {
                return _name;
            }
        }
        /// <summary>
        /// 当前用户工号
        /// </summary>
        public static string currUserWorkID
        {
            get
            {
                return workid;
            }
        }
        /// <summary>
        /// 数据库访问超时数
        /// </summary>
        //public static int dbTimeOut
        //{
        //    set { timeOut = value; }
        //    get { return timeOut; }
        //}
        /// <summary>
        /// 当前帐套名称
        /// </summary>
        public static string currAccount
        {
            get
            {
                return account;
            }
        }
        /// <summary>
        /// 当前帐套
        /// </summary>
        public static string currAct
        {
            get
            {
                return act;
            }
        }
        /// <summary>
        /// 当前用户登录时间
        /// </summary>
        //public static string currUserLoginTime
        //{
        //    get
        //    {
        //        return _time;
        //    }
        //}
        /// <summary>
        /// 当前用户角色
        /// </summary>
        public static string currUserRole
        {
            get
            {
                return _role;
            }
        }
        /// <summary>
        /// 检测权限
        /// </summary>
        /// <param name="str">检查类别</param>
        /// <returns>bool值，有（TRUE）与无（FALSE）。</returns>
        public static bool checkRight(string str)
        {
            if (isManager || allRight.Contains(str))
                return true;
            else if (str!="进货价")
            {
                System.Windows.Forms.MessageBox.Show("对不起，您没有执行 " + str + " 的权限！", "权限检查", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            return false;
        }
        private static bool checkUser(string sqlStr)
        {
            bool rt = false;
            using (IDataReader reader = GetDataReader(sqlStr, connStringPrv))
            {
                if (reader == null)
                    return false;
                if (reader.Read())
                {
                    //查出用户信息
                    workid = reader.GetString(0);
                    _name = reader.GetString(1);
                    isManager = reader.GetBoolean(2);
                    if (isManager)
                        _role = "管理员";
                    else
                    {
                        sqlStr = reader.GetString(3);
                        _role = reader.GetString(4);
                        using (IDataReader readerRight = GetDataReader("SELECT func_name FROM sys_Role_Func WHERE roleID='" + sqlStr + "'", connStringPrv))
                        {
                            if (allRight == null) 
                                allRight = new List<string>();
                            else
                                allRight.Clear();
                            while (readerRight.Read())
                            {
                                allRight.Add(readerRight.GetString(0));
                            }
                        }
                    }
                    rt= true;
                }
            }
            return rt;
        }
        /// <summary>
        /// 执行登录验证
        /// </summary>
        /// <param name="name">登录帐号</param>
        /// <param name="pwd">密码</param>
        /// <param name="actName">所选帐套名</param>
        /// <param name="act">所选帐套</param>
        /// <returns>bool值，成功（TRUE）与失败（FALSE）。</returns>
        public static bool doLogin(string name, string pwd, string actName,string acct)
        {
            //return true;
            account = actName;
            act = acct;
            if (dbServer.Equals("(local)\\SQLEXPRESS"))
                connStringPrv = string.Format("Data Source=(local)\\SQLEXPRESS;Initial Catalog={0};Integrated Security=SSPI;", act);
            else
                connStringPrv = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, act, dbName, dbPwd);
            acct = "SELECT user_code,CName, isManager,sys_UserS.roleID,roleName  FROM sys_UserS left join sys_Role on sys_Role.roleID=sys_UserS.roleID where user_code='" + name + "' and PWD='" + BaseClass.GetMd5Str(pwd) + "'";
            if (checkUser(acct))
            {
                //查出销售公司信息
                using (IDataReader reader = GetDataReader("SELECT [OrgName] ,[OrgCode] ,[OrgPWD] ,[ShipperCode] ,[ShowAccept],[Addr],[Tel] ,[after_Tel] ,[RMK],[store] FROM [sys_Organization]", connStringPrv))
                {
                    if (reader.Read())
                    {
                        orgName = reader.GetString(0);
                        orgCode = reader.GetString(1);
                        orgPWD = reader.GetString(2);
                        shipperCode = reader.GetString(3);
                        showAccept = reader.GetBoolean(4);
                        address = reader.GetString(5);
                        tel = reader.GetString(6);
                        telSupport = reader.GetString(7);
                        rmk = reader.GetString(8);
                        store = reader.GetString(9);
                    }
                }
                return true;
            }
            else
                return false;
        }
        public static bool doLogin(string name, string pwd)
        {
            return checkUser("SELECT user_code,CName, isManager,sys_UserS.roleID,roleName  FROM sys_UserS left join sys_Role on sys_Role.roleID=sys_UserS.roleID where user_code='" + name + "' and PWD='" + BaseClass.GetMd5Str(pwd) + "'");
        }
        
        /// <summary>    
        /// 功能：压缩文件（只压缩文件夹下一级目录中的文件，文件夹及其子级被忽略）    
        /// </summary>    
        /// <param name="dirPath">待压缩的文件夹或文件路径</param>    
        /// <param name="zipFilePath">生成压缩文件的路径，为空则默认与被压缩文件夹同一级目录，名称为：文件夹名+.zip</param>    
        /// <param name="err">出错信息</param>    
        /// <returns>是否压缩成功</returns>    
        //public static bool ZipFiles(string dirPath, string zipFilePath, out string err)
        //{
        //    err = "";
        //    if (dirPath == string.Empty)
        //    {
        //        err = "要压缩的文件路径不能为空！";
        //        return false;
        //    }
        //    //if (!Directory.Exists(dirPath))
        //    //{
        //    //    err = "要压缩的文件夹不存在！";
        //    //    return false;
        //    //}
        //    string[] filenames;
        //    if (File.Exists(dirPath))
        //    {

        //        if (zipFilePath == string.Empty)
        //        {
        //            zipFilePath = dirPath.Remove(dirPath.LastIndexOf('.'));
        //            zipFilePath +=   ".zip";
        //        }
        //        filenames = new string[] { dirPath };
        //    }
        //    else if (Directory.Exists(dirPath))
        //    {
        //        if (zipFilePath == string.Empty)
        //        {
        //            if (dirPath.EndsWith("\\"))
        //            {
        //                zipFilePath = dirPath + "tmp.zip";
        //            }
        //            else
        //                zipFilePath = dirPath + "\\tmp.zip";
        //        }
        //        filenames = Directory.GetFiles(dirPath, "*.xml", SearchOption.TopDirectoryOnly);
        //    }
        //    else
        //    {
        //        err = "要压缩的文件夹不存在！";
        //        return false;
        //    }
        //        //压缩文件名为空时使用文件夹名＋.zip    
        //        if (zipFilePath == string.Empty)
        //        {
        //            if (dirPath.EndsWith("\\"))
        //            {
        //                //dirPath = dirPath.TrimEnd('\\');//dirPath.Substring(0, dirPath.Length - 1);
        //                zipFilePath = dirPath + "tmp.zip";
        //            }
        //            else
        //                zipFilePath = dirPath + "\\tmp.zip";
        //        }

        //    try
        //    {
                
        //        using (ZipFile zip = new ZipFile())
        //        {
        //            foreach (string file in filenames)
        //            {
        //                zip.AddFile(file,"");
        //            }
        //            zip.Save(zipFilePath);
        //        }
        //        err = zipFilePath;
        //    }
        //    catch (Exception ex)
        //    {
        //        err = ex.Message;
        //        return false;
        //    }
        //    return true;
        //}
        /// <summary>
        /// 检查ZIP文件中是否包含检测内容
        /// </summary>
        /// <param name="zipFilePath">ZIP文件完整路径</param>
        /// <param name="files">待检测的内容文件</param>
        /// <returns></returns>
        //public static bool ZipContainsFiles(string zipFilePath, params string[] files)
        //{
        //    if (File.Exists(zipFilePath))
        //    {
        //        try
        //        {
        //            using (ZipFile zip = ZipFile.Read(zipFilePath, System.Text.Encoding.Default))
        //            {
        //                //zip.EntryFileNames
        //                foreach (string fls in files)
        //                {
        //                    if (zip.EntryFileNames.Contains(fls))
        //                        continue;
        //                    else
        //                        return false;
        //                }
        //            }
        //        }
        //        catch //(Exception ex)
        //        {
        //            return false;
        //        }
        //        return true;
        //    }
        //    return false;
        //}
        /// <summary>    
        /// 功能：解压zip格式的文件。    
        /// </summary>    
        /// <param name="zipFilePath">压缩文件路径</param>    
        /// <param name="unZipDir">解压文件存放路径,为空时默认与压缩文件同一级目录下，跟压缩文件同名的文件夹</param>    
        /// <param name="err">出错信息</param>    
        /// <returns>解压是否成功</returns>    
        //public static bool UnZipFile(string zipFilePath, string unZipDir, out string err)
        //{
        //    err = "";
        //    if (zipFilePath == string.Empty)
        //    {
        //        err = "压缩文件不能为空！";
        //        return false;
        //    }
        //    if (!File.Exists(zipFilePath))
        //    {
        //        err = "压缩文件不存在！";
        //        return false;
        //    }
        //    //解压文件夹为空时默认与压缩文件同一级目录下，跟压缩文件同名的文件夹    
        //    if (unZipDir == string.Empty)
        //        unZipDir = Path.GetDirectoryName(zipFilePath);
        //    else
        //    {
        //        //if (!unZipDir.EndsWith("\\"))
        //        //    unZipDir += "\\";
        //        if (!Directory.Exists(unZipDir))
        //            Directory.CreateDirectory(unZipDir);
        //    }
        //    try
        //    {
        //        using (ZipFile zip = ZipFile.Read(zipFilePath,System.Text.Encoding.Default))
        //        {
        //            //zip.ExtractAll(unZipDir, ExtractExistingFileAction.OverwriteSilently);
        //            foreach (ZipEntry e in zip)
        //            {
        //                //double tt = e.CompressedSize * 100 / (100 - e.CompressionRatio);
        //                //if (tt < 10485760)
        //                //err += e.FileName + '，';
        //                err = e.FileName;
        //                e.Extract(unZipDir, ExtractExistingFileAction.OverwriteSilently);
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        err = ex.Message;
        //        return false;
        //    }
        //    return true;
        //}

        public static DbConnection getDbConnection
        {
            get { return dbFactory.CreateConnection(); }
        }
        /// <summary>
        /// 取得数据表
        /// </summary>
        /// <param name="conn">数据库连接</param>
        /// <param name="SqlStr">SQL语句</param>
        /// <returns>数据表（DataTable）</returns>
        public static DataTable GetDataTable(DbConnection conn, string SqlStr, DataTable ds = null)
        {
            if (ds == null)
                ds = new DataTable();
            else
                ds.Clear();
            using ( DbCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                //cmd.Connection = conn;
                using (DbDataAdapter ada = dbFactory.CreateDataAdapter())
                {
                    ada.SelectCommand = cmd;
                    try
                    {
                        //if (conn.State == ConnectionState.Closed) conn.Open();
                        ada.Fill(ds);
                        //conn.Close();
                    }
                    catch (Exception ex)
                    {
                        //Log.LogError(SqlStr + ": \r\n" + ex.Message); //记录日志信息
                        //throw ex;
                        if (ex.Message.Contains("posbase"))
                        {
                            System.Windows.Forms.MessageBox.Show("请检测服务器，是否正确安装Pos Server，或者是否拥有访问权限！", "错误信息", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        }
                        else
                            System.Windows.Forms.MessageBox.Show(ex.Message, "错误信息", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                }

            }

            return ds;

        }
        /// <summary>
        /// 取得数据适配器
        /// </summary>
        /// <param name="upDateSql">更新SQL语句</param>
        /// <param name="selectSql">查询SQL语句</param>
        ///  <param name="connStr">连接字符串</param>
        /// <returns>数据适配器（DbDataAdapter）</returns>
        public static DbDataAdapter GetDataAdapter(string connStr, string selectSql, string updateSql="", string deleteSql="")
        {
            DbDataAdapter ada = dbFactory.CreateDataAdapter();
            ada.SelectCommand = dbFactory.CreateCommand();
            ada.SelectCommand.CommandText = selectSql;
            ada.SelectCommand.Connection = dbFactory.CreateConnection();
            ada.SelectCommand.Connection.ConnectionString = connStr;
            if (updateSql != null && updateSql.Length > 0)
            {
                DbCommand cmd = dbFactory.CreateCommand();
                cmd.CommandText = updateSql;
                cmd.Connection = ada.SelectCommand.Connection;
                ada.UpdateCommand = cmd;
            }
            if (deleteSql != null && deleteSql.Length > 0)
            {
                DbCommand cmd = dbFactory.CreateCommand();
                cmd.CommandText = deleteSql;
                cmd.Connection = ada.SelectCommand.Connection;
                ada.DeleteCommand = cmd;
            }
            return ada;
        }
        /// <summary>
        /// 取得数据集
        /// </summary>
        /// <param name="Sql_str">SQL语句</param>
        /// <returns>数据表（DataTable）</returns>
        public static DataTable GetDataTable(string SqlStr,string dbConn,DataTable db=null)
        {
            using (DbConnection conn = dbFactory.CreateConnection())
            {
                conn.ConnectionString = dbConn;
                return GetDataTable(conn, SqlStr,db);
            }
        }
        
        /// <summary>
        /// 取得数据读取器
        /// </summary>
        /// <param name="SqlStr">SQL语句</param>
        /// <param name="dbConn">连接字符串</param>
        /// <returns>DataReader</returns>
        public static IDataReader GetDataReader(string SqlStr, string dbConn)
        {
            IDataReader dr = null;
            DbConnection conn = dbFactory.CreateConnection();
            conn.ConnectionString = dbConn;
            using (DbCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                //cmd.Connection = conn;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);//System.Data.CommandBehavior.SingleResult
                }
                catch (Exception ex)
                {
                    //(SqlStr + ": \r\n" + ex.Message); //记录日志信息
                    //throw ex;
                    System.Windows.Forms.MessageBox.Show(ex.Message, "错误信息", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    
                }
            }
            return dr;
        }
        /// <summary>
        /// 取得数据读取器
        /// </summary>
        /// <param name="conn">数据库连接</param>
        /// <param name="SqlStr">SQL语句</param>
        /// <returns>DataReader</returns>
        public static IDataReader GetDataReader(DbConnection conn, string SqlStr)
        {
            IDataReader dr;
            using (DbCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    dr = cmd.ExecuteReader(System.Data.CommandBehavior.SingleResult);//System.Data.CommandBehavior.CloseConnection
                }
                catch (Exception ex)
                {
                    //(SqlStr + ": \r\n" + ex.Message); //记录日志信息
                    throw ex;
                }
            }
            return dr;
        }
        /// <summary>
        /// 用来判断是否存在数据
        /// </summary>
        /// <param name="Sql_str">SQL语句</param>
        /// <param name="conn">数据库连接</param>
        /// <returns>bool值，存在（TRUE）与不存在（FALSE）。</returns>
        public static bool IsHasValue(DbConnection conn, string SqlStr)
        {
            bool temp_flag = false;

            using (DbCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    if (cmd.ExecuteScalar() != null)
                        temp_flag = true;

                    //conn.Close();
                }
                catch (Exception ex)
                {
                    //(SqlStr + ": \r\n" + ex.Message); //记录日志信息
                    throw ex;
                }
            }
            return temp_flag;

        }
        /// <summary>
        /// 用来判断是否存在数据
        /// </summary>
        /// <param name="Sql_str">SQL语句</param>
        /// <returns>bool值，存在（TRUE）与不存在（FALSE）。</returns>
        public static bool IsHasValue(string SqlStr, string dbConn)
        {

            using (DbConnection conn = dbFactory.CreateConnection())
            {
                conn.ConnectionString = dbConn;
                return IsHasValue(conn, SqlStr);
            }
        }
        /// <summary>
        /// 执行SQL语句,返回受影响的行数
        /// </summary>
        /// <param name="connString">连接字符串</param>
        /// <param name="cmdText">SQL语句</param>
        /// <param name="parameters">SQL的参数</param>
        /// <returns>受影响的行数</returns>
        public static int ExecuteNonQuery(string cmdText, string dbConn, params DbParameter[] parameters)
        {
            using (DbConnection conn = dbFactory.CreateConnection())
            {
                conn.ConnectionString = dbConn;
                //conn.ConnectionTimeout
                return ExecuteNonQuery(conn, cmdText, parameters);
            }
        }

        /// <summary>
        /// 执行SQL语句,返回受影响的行数
        /// </summary>
        /// <param name="connection">数据库链接</param>
        /// <param name="cmdText">SQL语句</param>
        /// <param name="parameters">参数</param>
        /// <returns>受影响的行数</returns>
        public static int ExecuteNonQuery(DbConnection connection, string cmdText, params DbParameter[] parameters)
        {
            int val = 0;

            using (DbCommand cmd = connection.CreateCommand())
            {
                //PrepareCommand(cmd, connection, null, cmdText);
                cmd.CommandText = cmdText;
                if (null != parameters && parameters.Length > 0)
                {
                    cmd.Parameters.AddRange(parameters);
                }
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                val = cmd.ExecuteNonQuery();
            }

            return val;
        }
        /// <summary>
        /// 执行带有事务的SQL语句
        /// </summary>
        /// <param name="trans">事务</param>
        /// <param name="cmdText">SQL语句</param>
        /// <returns>受影响的行数</returns> 
        public static int ExecuteNonQuery(DbTransaction trans, string cmdText, params DbParameter[] parameters)
        {
            int val = 0;

            using (DbCommand cmd = trans.Connection.CreateCommand())
            {
                cmd.CommandText = cmdText;
                if (null != parameters && parameters.Length > 0)
                {
                    cmd.Parameters.AddRange(parameters);
                }
                cmd.Transaction = trans;
                if (trans.Connection.State != ConnectionState.Open)
                    trans.Connection.Open();

                val = cmd.ExecuteNonQuery();
            }

            return val;
        }
        /// <summary>
        /// 执行查询,并返回结果集的第一行的第一列.其他所有的行和列被忽略.
        /// </summary>
        /// <param name="connString">连接字符串</param>
        /// <param name="cmdText">SQL 语句</param>
        /// <returns>第一行的第一列的值</returns>
        public static object ExecuteScalar(string cmdText, string dbConn)
        {
            object val;
            using (DbConnection conn = dbFactory.CreateConnection())
            {
                conn.ConnectionString = dbConn;
                val = ExecuteScalar(conn, cmdText);
            }
            return val;
        }

        /// <summary>
        /// 执行查询,并返回结果集的第一行的第一列.其他所有的行和列被忽略.
        /// </summary>
        /// <param name="connection">数据库链接</param>
        /// <param name="cmdText">SQL 语句</param>
        /// <returns>第一行的第一列的值</returns>
        public static object ExecuteScalar(DbConnection connection, string cmdText)
        {
            object val;

            using (DbCommand cmd = connection.CreateCommand())
            {
                //PrepareCommand(cmd, connection, null, cmdText);
                cmd.CommandText = cmdText;
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                val = cmd.ExecuteScalar();
            }
            
            return val;
        }
        /// <summary>
        /// 执行查询,并返回结果集的第一行的第一列.其他所有的行和列被忽略.
        /// </summary>
        /// <param name="trans">事务处理</param>
        /// <param name="cmdText">SQL 语句</param>
        /// <returns>第一行的第一列的值</returns>
        public static object ExecuteScalar(DbTransaction trans, string cmdText)
        {
            object val;

            using (DbCommand cmd = trans.Connection.CreateCommand())
            {
                cmd.Transaction = trans;
                cmd.CommandText = cmdText;
                if (trans.Connection.State != ConnectionState.Open)
                    trans.Connection.Open();
                val = cmd.ExecuteScalar();
            }

            return val;
        }
        //System.Data.Common.DbParameter pp = new System.Data.SqlClient.SqlParameter("@unit_CN", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, 0, 0, "中文", System.Data.DataRowVersion.Current, false, null, "", "", "");
        public static DbParameter MakeParameter(string parName, DbType dbtype, string sourceName, bool allowNull)
        {
            DbParameter par = dbFactory.CreateParameter();
            par.ParameterName = parName;
            par.DbType = dbtype;
            par.Size = 0;
            par.Direction = System.Data.ParameterDirection.Input;
            par.SourceColumn = sourceName;
            par.SourceVersion = System.Data.DataRowVersion.Current;
            par.SourceColumnNullMapping = allowNull;
            par.Value = null;
            return par;
        }
        public static DbParameter MakeParameter(string parName, DbType dbtype, object value,int maxSize)
        {
            DbParameter par = dbFactory.CreateParameter();
            par.ParameterName = parName;
            par.DbType = dbtype;
            par.Value = value;
            par.Size = maxSize;
            return par;
        }
        public static DbParameter MakeParameter(string parName, DbType dbtype, object value)
        {
            DbParameter par = dbFactory.CreateParameter();
            par.ParameterName = parName;
            par.DbType = dbtype;
            par.Value = value;
            return par;
        }

        /// <summary>
        /// MD5 32位加密
        /// </summary>
        //public static string GetMd5Str(string str)
        //{
        //    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        //    //string t2 = BitConverter.ToString(md5.ComputeHash(System.Text.Encoding.Default.GetBytes(str)), 4, 8);
        //    //t2 = t2.Replace("-", "");
        //    //return t2;
        //    byte[] data = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(str));
        //    System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();
        //    for (int i = 0; i < data.Length; i++)
        //    {
        //        sBuilder.Append(data[i].ToString("x2"));
        //    }

        //    return sBuilder.ToString();
        //}
        
    }
    class querySelect
    {
        public string names { get; set; }
        public string values { get; set; }
        public override string ToString()
        {
            return names;
        }
    }
}

﻿namespace CSICPR
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
        	this.menuStrip1 = new System.Windows.Forms.MenuStrip();
        	this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
        	this.tstHelpAbput = new System.Windows.Forms.ToolStripMenuItem();
        	this.tsbHelpUsing = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
        	this.tsbWindowTile = new System.Windows.Forms.ToolStripMenuItem();
        	this.tsbWindowHzt = new System.Windows.Forms.ToolStripMenuItem();
        	this.tsbWindowVtcl = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.toolStrip1 = new System.Windows.Forms.ToolStrip();
        	this.tssbSystem = new System.Windows.Forms.ToolStripSplitButton();
        	this.MenuItemBase = new System.Windows.Forms.ToolStripMenuItem();
        	this.accountManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.packageConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
        	this.LineARolls = new System.Windows.Forms.ToolStripMenuItem();
        	this.LineBRolls = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
        	this.lineAHoist = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBHoist = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
        	this.tssbChart = new System.Windows.Forms.ToolStripSplitButton();
        	this.MenuItemPackage = new System.Windows.Forms.ToolStripMenuItem();
        	this.packageReportByCartonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.packageReportByModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.loadReportByDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.loadReportByLoadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.loadReportByCartonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.loadReportByModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.qualityReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        	this.tssbExit = new System.Windows.Forms.ToolStripButton();
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
        	this.tsslCurUserName = new System.Windows.Forms.ToolStripStatusLabel();
        	this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
        	this.tsslCurWorkshop = new System.Windows.Forms.ToolStripStatusLabel();
        	this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        	this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
        	this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
        	this.menuStrip1.SuspendLayout();
        	this.panel1.SuspendLayout();
        	this.toolStrip1.SuspendLayout();
        	this.panel2.SuspendLayout();
        	this.statusStrip1.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// menuStrip1
        	// 
        	this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.menuHelp});
        	this.menuStrip1.Location = new System.Drawing.Point(0, 0);
        	this.menuStrip1.Name = "menuStrip1";
        	this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
        	this.menuStrip1.Size = new System.Drawing.Size(86, 55);
        	this.menuStrip1.TabIndex = 8;
        	this.menuStrip1.ItemAdded += new System.Windows.Forms.ToolStripItemEventHandler(this.menuStrip1_ItemAdded);
        	// 
        	// menuHelp
        	// 
        	this.menuHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
        	this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.tstHelpAbput,
			this.tsbHelpUsing,
			this.toolStripSeparator10,
			this.tsbWindowTile,
			this.tsbWindowHzt,
			this.tsbWindowVtcl,
			this.toolStripSeparator11});
        	this.menuHelp.Image = global::CSICPR.Properties.Resources.book;
        	this.menuHelp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.menuHelp.Name = "menuHelp";
        	this.menuHelp.Size = new System.Drawing.Size(44, 51);
        	this.menuHelp.Text = "Help";
        	this.menuHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	// 
        	// tstHelpAbput
        	// 
        	this.tstHelpAbput.Image = global::CSICPR.Properties.Resources.information;
        	this.tstHelpAbput.Name = "tstHelpAbput";
        	this.tstHelpAbput.Size = new System.Drawing.Size(179, 22);
        	this.tstHelpAbput.Text = "About";
        	// 
        	// tsbHelpUsing
        	// 
        	this.tsbHelpUsing.Image = global::CSICPR.Properties.Resources.lightbulb;
        	this.tsbHelpUsing.Name = "tsbHelpUsing";
        	this.tsbHelpUsing.Size = new System.Drawing.Size(179, 22);
        	this.tsbHelpUsing.Text = "Help";
        	// 
        	// toolStripSeparator10
        	// 
        	this.toolStripSeparator10.Name = "toolStripSeparator10";
        	this.toolStripSeparator10.Size = new System.Drawing.Size(176, 6);
        	// 
        	// tsbWindowTile
        	// 
        	this.tsbWindowTile.Name = "tsbWindowTile";
        	this.tsbWindowTile.Size = new System.Drawing.Size(179, 22);
        	this.tsbWindowTile.Text = "Tiling display";
        	this.tsbWindowTile.Click += new System.EventHandler(this.tsbWindowTile_Click);
        	// 
        	// tsbWindowHzt
        	// 
        	this.tsbWindowHzt.Name = "tsbWindowHzt";
        	this.tsbWindowHzt.Size = new System.Drawing.Size(179, 22);
        	this.tsbWindowHzt.Text = "Horizontally Display";
        	this.tsbWindowHzt.Click += new System.EventHandler(this.tsbWindowHzt_Click);
        	// 
        	// tsbWindowVtcl
        	// 
        	this.tsbWindowVtcl.Name = "tsbWindowVtcl";
        	this.tsbWindowVtcl.Size = new System.Drawing.Size(179, 22);
        	this.tsbWindowVtcl.Text = "Vertically Display";
        	this.tsbWindowVtcl.Click += new System.EventHandler(this.tsbWindowVtcl_Click);
        	// 
        	// toolStripSeparator11
        	// 
        	this.toolStripSeparator11.Name = "toolStripSeparator11";
        	this.toolStripSeparator11.Size = new System.Drawing.Size(176, 6);
        	// 
        	// panel1
        	// 
        	this.panel1.Controls.Add(this.toolStrip1);
        	this.panel1.Controls.Add(this.panel2);
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel1.Location = new System.Drawing.Point(0, 0);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(1237, 61);
        	this.panel1.TabIndex = 9;
        	// 
        	// toolStrip1
        	// 
        	this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
        	this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.tssbSystem,
			this.toolStripSeparator5,
			this.toolStripButton1,
			this.toolStripSeparator3,
			this.toolStripButton2,
			this.toolStripSeparator6,
			this.toolStripButton3,
			this.toolStripSeparator7,
			this.toolStripButton4,
			this.toolStripSeparator8,
			this.toolStripButton5,
			this.toolStripSeparator4,
			this.toolStripButton6,
			this.toolStripSeparator2,
			this.toolStripButton7,
			this.toolStripSeparator9,
			this.toolStripSplitButton1,
			this.toolStripSeparator12,
			this.toolStripSplitButton2,
			this.toolStripSeparator13,
			this.tssbChart,
			this.toolStripSeparator1,
			this.tssbExit});
        	this.toolStrip1.Location = new System.Drawing.Point(0, 0);
        	this.toolStrip1.Name = "toolStrip1";
        	this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
        	this.toolStrip1.Size = new System.Drawing.Size(1151, 61);
        	this.toolStrip1.TabIndex = 4;
        	// 
        	// tssbSystem
        	// 
        	this.tssbSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.MenuItemBase,
			this.accountManagerToolStripMenuItem,
			this.packageConfigToolStripMenuItem});
        	this.tssbSystem.Image = global::CSICPR.Properties.Resources.box;
        	this.tssbSystem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.tssbSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.tssbSystem.Name = "tssbSystem";
        	this.tssbSystem.Size = new System.Drawing.Size(106, 58);
        	this.tssbSystem.Text = "System Settings";
        	this.tssbSystem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.tssbSystem.ButtonClick += new System.EventHandler(this.tssbSystem_ButtonClick);
        	// 
        	// MenuItemBase
        	// 
        	this.MenuItemBase.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.MenuItemBase.Name = "MenuItemBase";
        	this.MenuItemBase.Size = new System.Drawing.Size(169, 22);
        	this.MenuItemBase.Text = "Database Config";
        	this.MenuItemBase.Click += new System.EventHandler(this.MenuItemBase_Click);
        	// 
        	// accountManagerToolStripMenuItem
        	// 
        	this.accountManagerToolStripMenuItem.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.accountManagerToolStripMenuItem.Name = "accountManagerToolStripMenuItem";
        	this.accountManagerToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
        	this.accountManagerToolStripMenuItem.Text = "Account Manager";
        	this.accountManagerToolStripMenuItem.Click += new System.EventHandler(this.accountManagerToolStripMenuItem_Click);
        	// 
        	// packageConfigToolStripMenuItem
        	// 
        	this.packageConfigToolStripMenuItem.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.packageConfigToolStripMenuItem.Name = "packageConfigToolStripMenuItem";
        	this.packageConfigToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
        	this.packageConfigToolStripMenuItem.Text = "Package Config";
        	this.packageConfigToolStripMenuItem.Click += new System.EventHandler(this.packageConfigToolStripMenuItem_Click);
        	// 
        	// toolStripSeparator5
        	// 
        	this.toolStripSeparator5.Name = "toolStripSeparator5";
        	this.toolStripSeparator5.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripButton1
        	// 
        	this.toolStripButton1.Image = global::CSICPR.Properties.Resources.down;
        	this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripButton1.Name = "toolStripButton1";
        	this.toolStripButton1.Size = new System.Drawing.Size(71, 58);
        	this.toolStripButton1.Text = "A- Marking";
        	this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripButton1.Click += new System.EventHandler(this.ToolStripButton1Click);
        	// 
        	// toolStripSeparator3
        	// 
        	this.toolStripSeparator3.Name = "toolStripSeparator3";
        	this.toolStripSeparator3.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripButton2
        	// 
        	this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
        	this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.White;
        	this.toolStripButton2.Name = "toolStripButton2";
        	this.toolStripButton2.Size = new System.Drawing.Size(107, 58);
        	this.toolStripButton2.Text = "Quality Inspection";
        	this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripButton2.Click += new System.EventHandler(this.ToolStripButton2Click);
        	// 
        	// toolStripSeparator6
        	// 
        	this.toolStripSeparator6.Name = "toolStripSeparator6";
        	this.toolStripSeparator6.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripButton3
        	// 
        	this.toolStripButton3.Image = global::CSICPR.Properties.Resources.down;
        	this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripButton3.Name = "toolStripButton3";
        	this.toolStripButton3.Size = new System.Drawing.Size(50, 58);
        	this.toolStripButton3.Text = "Rework";
        	this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripButton3.ToolTipText = "Rework";
        	this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
        	// 
        	// toolStripSeparator7
        	// 
        	this.toolStripSeparator7.Name = "toolStripSeparator7";
        	this.toolStripSeparator7.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripButton4
        	// 
        	this.toolStripButton4.Image = global::CSICPR.Properties.Resources.down;
        	this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripButton4.Name = "toolStripButton4";
        	this.toolStripButton4.Size = new System.Drawing.Size(70, 58);
        	this.toolStripButton4.Text = "Rework QA";
        	this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
        	// 
        	// toolStripSeparator8
        	// 
        	this.toolStripSeparator8.Name = "toolStripSeparator8";
        	this.toolStripSeparator8.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripButton5
        	// 
        	this.toolStripButton5.Image = global::CSICPR.Properties.Resources.down;
        	this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripButton5.Name = "toolStripButton5";
        	this.toolStripButton5.Size = new System.Drawing.Size(36, 58);
        	this.toolStripButton5.Text = "MRB";
        	this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripButton5.Click += new System.EventHandler(this.ToolStripButton5Click);
        	// 
        	// toolStripSeparator4
        	// 
        	this.toolStripSeparator4.Name = "toolStripSeparator4";
        	this.toolStripSeparator4.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripButton6
        	// 
        	this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
        	this.toolStripButton6.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripButton6.Name = "toolStripButton6";
        	this.toolStripButton6.Size = new System.Drawing.Size(60, 58);
        	this.toolStripButton6.Text = "EL Defect";
        	this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripButton6.Click += new System.EventHandler(this.ToolStripButton6Click);
        	// 
        	// toolStripSeparator2
        	// 
        	this.toolStripSeparator2.Name = "toolStripSeparator2";
        	this.toolStripSeparator2.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripButton7
        	// 
        	this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
        	this.toolStripButton7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripButton7.Name = "toolStripButton7";
        	this.toolStripButton7.Size = new System.Drawing.Size(135, 58);
        	this.toolStripButton7.Text = "Stringer/Bussing Defect";
        	this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripButton7.Click += new System.EventHandler(this.ToolStripButton7Click);
        	// 
        	// toolStripSeparator9
        	// 
        	this.toolStripSeparator9.Name = "toolStripSeparator9";
        	this.toolStripSeparator9.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripSplitButton1
        	// 
        	this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.LineARolls,
			this.LineBRolls});
        	this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
        	this.toolStripSplitButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripSplitButton1.Name = "toolStripSplitButton1";
        	this.toolStripSplitButton1.Size = new System.Drawing.Size(71, 58);
        	this.toolStripSplitButton1.Text = "Rolls Log";
        	this.toolStripSplitButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	// 
        	// LineARolls
        	// 
        	this.LineARolls.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.LineARolls.Name = "LineARolls";
        	this.LineARolls.Size = new System.Drawing.Size(107, 22);
        	this.LineARolls.Text = "Line A";
        	this.LineARolls.Click += new System.EventHandler(this.LineARollsClick);
        	// 
        	// LineBRolls
        	// 
        	this.LineBRolls.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.LineBRolls.Name = "LineBRolls";
        	this.LineBRolls.Size = new System.Drawing.Size(107, 22);
        	this.LineBRolls.Text = "Line B";
        	this.LineBRolls.Click += new System.EventHandler(this.LineBRollsClick);
        	// 
        	// toolStripSeparator12
        	// 
        	this.toolStripSeparator12.Name = "toolStripSeparator12";
        	this.toolStripSeparator12.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripSplitButton2
        	// 
        	this.toolStripSplitButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAHoist,
			this.lineBHoist});
        	this.toolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton2.Image")));
        	this.toolStripSplitButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripSplitButton2.Name = "toolStripSplitButton2";
        	this.toolStripSplitButton2.Size = new System.Drawing.Size(102, 58);
        	this.toolStripSplitButton2.Text = "Hoist Checklist";
        	this.toolStripSplitButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	// 
        	// lineAHoist
        	// 
        	this.lineAHoist.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.lineAHoist.Name = "lineAHoist";
        	this.lineAHoist.Size = new System.Drawing.Size(152, 22);
        	this.lineAHoist.Text = "Line A";
        	this.lineAHoist.Click += new System.EventHandler(this.LineAHoistClick);
        	// 
        	// lineBHoist
        	// 
        	this.lineBHoist.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.lineBHoist.Name = "lineBHoist";
        	this.lineBHoist.Size = new System.Drawing.Size(152, 22);
        	this.lineBHoist.Text = "Line B";
        	this.lineBHoist.Click += new System.EventHandler(this.LineBHoistClick);
        	// 
        	// toolStripSeparator13
        	// 
        	this.toolStripSeparator13.Name = "toolStripSeparator13";
        	this.toolStripSeparator13.Size = new System.Drawing.Size(6, 61);
        	// 
        	// tssbChart
        	// 
        	this.tssbChart.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.MenuItemPackage,
			this.packageReportByCartonToolStripMenuItem,
			this.packageReportByModuleToolStripMenuItem,
			this.loadReportByDateToolStripMenuItem,
			this.loadReportByLoadToolStripMenuItem,
			this.loadReportByCartonToolStripMenuItem,
			this.loadReportByModuleToolStripMenuItem,
			this.qualityReportToolStripMenuItem});
        	this.tssbChart.Image = global::CSICPR.Properties.Resources.chart;
        	this.tssbChart.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.tssbChart.Name = "tssbChart";
        	this.tssbChart.Size = new System.Drawing.Size(58, 58);
        	this.tssbChart.Text = "Report";
        	this.tssbChart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.tssbChart.ButtonClick += new System.EventHandler(this.tssbChart_ButtonClick);
        	// 
        	// MenuItemPackage
        	// 
        	this.MenuItemPackage.Image = global::CSICPR.Properties.Resources.application_form;
        	this.MenuItemPackage.Name = "MenuItemPackage";
        	this.MenuItemPackage.Size = new System.Drawing.Size(216, 22);
        	this.MenuItemPackage.Text = "Package Report By Date";
        	this.MenuItemPackage.Click += new System.EventHandler(this.MenuItemPackage_Click);
        	// 
        	// packageReportByCartonToolStripMenuItem
        	// 
        	this.packageReportByCartonToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.packageReportByCartonToolStripMenuItem.Name = "packageReportByCartonToolStripMenuItem";
        	this.packageReportByCartonToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.packageReportByCartonToolStripMenuItem.Text = "Package Report By Carton";
        	this.packageReportByCartonToolStripMenuItem.Click += new System.EventHandler(this.packageReportByCartonToolStripMenuItem_Click);
        	// 
        	// packageReportByModuleToolStripMenuItem
        	// 
        	this.packageReportByModuleToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.packageReportByModuleToolStripMenuItem.Name = "packageReportByModuleToolStripMenuItem";
        	this.packageReportByModuleToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.packageReportByModuleToolStripMenuItem.Text = "Package Report By Module";
        	this.packageReportByModuleToolStripMenuItem.Click += new System.EventHandler(this.packageReportByModuleToolStripMenuItem_Click);
        	// 
        	// loadReportByDateToolStripMenuItem
        	// 
        	this.loadReportByDateToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.loadReportByDateToolStripMenuItem.Name = "loadReportByDateToolStripMenuItem";
        	this.loadReportByDateToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.loadReportByDateToolStripMenuItem.Text = "Load Report By Date";
        	this.loadReportByDateToolStripMenuItem.Click += new System.EventHandler(this.LoadReportByDateToolStripMenuItemClick);
        	// 
        	// loadReportByLoadToolStripMenuItem
        	// 
        	this.loadReportByLoadToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.loadReportByLoadToolStripMenuItem.Name = "loadReportByLoadToolStripMenuItem";
        	this.loadReportByLoadToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.loadReportByLoadToolStripMenuItem.Text = "Load Report By Load";
        	this.loadReportByLoadToolStripMenuItem.Click += new System.EventHandler(this.LoadReportByLoadToolStripMenuItemClick);
        	// 
        	// loadReportByCartonToolStripMenuItem
        	// 
        	this.loadReportByCartonToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.loadReportByCartonToolStripMenuItem.Name = "loadReportByCartonToolStripMenuItem";
        	this.loadReportByCartonToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.loadReportByCartonToolStripMenuItem.Text = "Load Report By Carton";
        	this.loadReportByCartonToolStripMenuItem.Click += new System.EventHandler(this.LoadReportByCartonToolStripMenuItemClick);
        	// 
        	// loadReportByModuleToolStripMenuItem
        	// 
        	this.loadReportByModuleToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.loadReportByModuleToolStripMenuItem.Name = "loadReportByModuleToolStripMenuItem";
        	this.loadReportByModuleToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.loadReportByModuleToolStripMenuItem.Text = "Load Report By Module";
        	this.loadReportByModuleToolStripMenuItem.Click += new System.EventHandler(this.LoadReportByModuleToolStripMenuItemClick);
        	// 
        	// qualityReportToolStripMenuItem
        	// 
        	this.qualityReportToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.qualityReportToolStripMenuItem.Name = "qualityReportToolStripMenuItem";
        	this.qualityReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.qualityReportToolStripMenuItem.Text = "Quality Report";
        	this.qualityReportToolStripMenuItem.Click += new System.EventHandler(this.QualityReportToolStripMenuItemClick);
        	// 
        	// toolStripSeparator1
        	// 
        	this.toolStripSeparator1.Name = "toolStripSeparator1";
        	this.toolStripSeparator1.Size = new System.Drawing.Size(6, 61);
        	// 
        	// tssbExit
        	// 
        	this.tssbExit.Image = global::CSICPR.Properties.Resources._return;
        	this.tssbExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.tssbExit.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.tssbExit.Name = "tssbExit";
        	this.tssbExit.Size = new System.Drawing.Size(46, 58);
        	this.tssbExit.Text = "Logoff";
        	this.tssbExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.tssbExit.Click += new System.EventHandler(this.tssbExit_Click);
        	// 
        	// panel2
        	// 
        	this.panel2.Controls.Add(this.menuStrip1);
        	this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
        	this.panel2.Location = new System.Drawing.Point(1151, 0);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(86, 61);
        	this.panel2.TabIndex = 5;
        	// 
        	// toolStripStatusLabel1
        	// 
        	this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
        	this.toolStripStatusLabel1.Size = new System.Drawing.Size(57, 17);
        	this.toolStripStatusLabel1.Text = "Operator:";
        	// 
        	// tsslCurUserName
        	// 
        	this.tsslCurUserName.AutoSize = false;
        	this.tsslCurUserName.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.tsslCurUserName.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.tsslCurUserName.Name = "tsslCurUserName";
        	this.tsslCurUserName.Size = new System.Drawing.Size(100, 17);
        	this.tsslCurUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// toolStripStatusLabel2
        	// 
        	this.toolStripStatusLabel2.Margin = new System.Windows.Forms.Padding(10, 3, 0, 2);
        	this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
        	this.toolStripStatusLabel2.Size = new System.Drawing.Size(65, 17);
        	this.toolStripStatusLabel2.Text = "WorkShop:";
        	// 
        	// tsslCurWorkshop
        	// 
        	this.tsslCurWorkshop.AutoSize = false;
        	this.tsslCurWorkshop.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.tsslCurWorkshop.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.tsslCurWorkshop.Name = "tsslCurWorkshop";
        	this.tsslCurWorkshop.Size = new System.Drawing.Size(100, 17);
        	this.tsslCurWorkshop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// statusStrip1
        	// 
        	this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripStatusLabel1,
			this.tsslCurUserName,
			this.toolStripStatusLabel2,
			this.tsslCurWorkshop,
			this.toolStripStatusLabel3,
			this.toolStripStatusLabel4});
        	this.statusStrip1.Location = new System.Drawing.Point(0, 587);
        	this.statusStrip1.Name = "statusStrip1";
        	this.statusStrip1.Size = new System.Drawing.Size(1237, 22);
        	this.statusStrip1.TabIndex = 6;
        	this.statusStrip1.Text = "statusStrip1";
        	// 
        	// toolStripStatusLabel3
        	// 
        	this.toolStripStatusLabel3.Margin = new System.Windows.Forms.Padding(10, 3, 0, 2);
        	this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
        	this.toolStripStatusLabel3.Size = new System.Drawing.Size(49, 17);
        	this.toolStripStatusLabel3.Text = "Version:";
        	// 
        	// toolStripStatusLabel4
        	// 
        	this.toolStripStatusLabel4.AutoSize = false;
        	this.toolStripStatusLabel4.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.toolStripStatusLabel4.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
        	this.toolStripStatusLabel4.Size = new System.Drawing.Size(100, 17);
        	this.toolStripStatusLabel4.Text = "14.0.0.0";
        	this.toolStripStatusLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	this.toolStripStatusLabel4.Click += new System.EventHandler(this.ToolStripStatusLabel4Click);
        	// 
        	// FormMain
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(1237, 609);
        	this.Controls.Add(this.panel1);
        	this.Controls.Add(this.statusStrip1);
        	this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        	this.IsMdiContainer = true;
        	this.MainMenuStrip = this.menuStrip1;
        	this.Name = "FormMain";
        	this.Text = "CSI Packing System";
        	this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        	this.Load += new System.EventHandler(this.FormMain_Load);
        	this.menuStrip1.ResumeLayout(false);
        	this.menuStrip1.PerformLayout();
        	this.panel1.ResumeLayout(false);
        	this.panel1.PerformLayout();
        	this.toolStrip1.ResumeLayout(false);
        	this.toolStrip1.PerformLayout();
        	this.panel2.ResumeLayout(false);
        	this.panel2.PerformLayout();
        	this.statusStrip1.ResumeLayout(false);
        	this.statusStrip1.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripMenuItem qualityReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripMenuItem loadReportByModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadReportByCartonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadReportByLoadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadReportByDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem tstHelpAbput;
        private System.Windows.Forms.ToolStripMenuItem tsbHelpUsing;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem tsbWindowTile;
        private System.Windows.Forms.ToolStripMenuItem tsbWindowHzt;
        private System.Windows.Forms.ToolStripMenuItem tsbWindowVtcl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSplitButton tssbSystem;
        private System.Windows.Forms.ToolStripMenuItem MenuItemBase;
        private System.Windows.Forms.ToolStripMenuItem accountManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSplitButton tssbChart;
        private System.Windows.Forms.ToolStripMenuItem MenuItemPackage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tsslCurUserName;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tsslCurWorkshop;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem packageConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tssbExit;
        private System.Windows.Forms.ToolStripMenuItem packageReportByCartonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem packageReportByModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem LineARolls;
        private System.Windows.Forms.ToolStripMenuItem LineBRolls;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton2;
        private System.Windows.Forms.ToolStripMenuItem lineAHoist;
        private System.Windows.Forms.ToolStripMenuItem lineBHoist;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
    }
}


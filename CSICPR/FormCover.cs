﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Threading;

namespace CSICPR
{
    public partial class FormCover : Form
    {
        private FormLogin fLogin;
        //System.Data.DataTable accountTable;
        private static Image bgImage;
        public static Image loginBackgroundImage
        {
            get { return bgImage; }
        }
        public FormCover(bool isLoading, bool isLock = false)
        {
            InitializeComponent();
            fLogin = new FormLogin(isLock);
            fLogin.Owner = this;
            
            //this.label1.Visible = isLoading;
            this.label2.Visible = isLoading;
            this.label3.Visible = isLoading;
            this.btLogin.Visible = isLoading;
            this.btQuit.Visible = isLoading;
            this.tbName.Visible = isLoading;
            this.tbPWD.Visible = isLoading;
            //this.btSetUp.Visible = isLoading;
            //this.comboBox1.Visible = isLoading;
            if (isLoading)
            {
                if (bgImage == null && System.IO.File.Exists("loginBG.png"))
                {
                    bgImage = Image.FromFile("loginBG.png");
                }
                if (bgImage != null) this.BackgroundImage = bgImage; ;
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                this.btQuit.Click += new System.EventHandler(this.btQuit_Click);
                this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
                //this.btSetUp.Click += new System.EventHandler(this.btSetUp_Click);
                this.tbPWD.Enter += new System.EventHandler(this.textBox_Enter);
                this.tbPWD.Leave += new System.EventHandler(this.textBox_Leave);
                this.tbName.Enter += new System.EventHandler(this.textBox_Enter);
                this.tbName.Leave += new System.EventHandler(this.textBox_Leave);
                this.TopMost = true;
                getRegistryKeys();
                //accountTable = new System.Data.DataTable();
                //accountTable.Columns.Add("account_code");
                //accountTable.Columns.Add("account_name");
                //accountTable.Columns.Add("server_url");
                ////accountTable.Columns.Add("parameters");
                //comboBox1.DataSource = accountTable;
                //comboBox1.DisplayMember = "account_name";
                //comboBox1.ValueMember = "account_code";
                //getAccount();
            }
            else
            {
                this.Opacity = 0.7;
                this.BackColor = System.Drawing.SystemColors.GrayText;
                this.Shown += new System.EventHandler(this.FormCover_Shown);
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            }
        }

        //private void getAccount()
        //{
        //    if (BaseClass.DBServer != null && BaseClass.DBServer != "")
        //        BaseClass.GetDataTable("Select [account_code],[account_name],[server_url] from base_account WHERE [id] > 1 ", BaseClass.connectionBase, accountTable);
        //}
        private void FormCover_Shown(object sender, EventArgs e)
        {
            fLogin.ShowDialog();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            string user, pwd;
            user = this.tbName.Text.Trim();
            if (user.Length == 0)
            {
                //显示Prompting Message
                //FormMain.showTips(this, null, "帐号不能为空！", new Size(100, 60), this.tbName);
                FormMain.showTipMSG("Account can not be empty!", this.tbName, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            
            pwd = this.tbPWD.Text.Trim();
            if (pwd.Length == 0)
            {
                //显示Prompting Message
                //FormMain.showTips(this, null, "密码不能为空！", new Size(100, 60), this.tbPWD);
                FormMain.showTipMSG("Password can not be empty!", this.tbPWD, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            bool isOK = verifyUser(user,pwd);

            if (isOK) this.DialogResult = DialogResult.OK;
            else
            {
                FormMain.showTipMSG("Account or Password incorrect!", this.tbPWD, "Prompting Message", ToolTipIcon.Warning);
                tbPWD.Text = "";
                tbPWD.Focus();
            }
        }

        private static bool verifyUser(string user,string pwd)
        {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                try
                {
                    conn.Open();
                    currUserName = "";
                    canUse = false;
                    isManager = false;
                    cmd.CommandText = string.Format("SELECT [UserNM],[Status],[UserGroup] FROM [UserLogin] WHERE [UserNM]='{0}' AND [UserPW]='{1}'", user, pwd);//SELECT [Name],[Manager] FROM [Operator] WHERE [OperatorID]='{0}' AND [Password]='{1}'
                    System.Data.SqlClient.SqlDataReader read = cmd.ExecuteReader();                    
                    if (read.Read())
                    {
                        allRight = new List<string>();
                        if (read.GetBoolean(1))
                        {
                            allRight.Add("Unpack");
                            allRight.Add("Account Manager");
                            allRight.Add("Package Config");
                            allRight.Add("Database Config");
                            allRight.Add("Replace Module");
                            allRight.Add("Unpack Load");
                            
                            
                            //string sql = "select FuncItemNM from FunctionItem where FuncGroupInterID=" + read.GetInt32(2);
                            //System.Data.SqlClient.SqlDataReader rd =ToolsClass.GetDataReader(sql);
                            //while (rd.Read())
                            //{
                            //    allRight.Add(rd.GetString(0));
                            //}
                            //rd.Close();
                        }
                        currUserName = read.GetString(0);
                        currUserWorkID = user;
                        curPWD = pwd;

                        //Todo 检查用户所有权限
                       
                        //if (read.GetInt32(2) != 3)
                        //    canUse=true;
						   #region checking user authority 20151109
                        if (read.GetInt32(2) == 9)//admin
                        {
                        	isManager = true;
                        	allRight.Add("Package");
                        	allRight.Add("Quality Inspection");
                        	allRight.Add("EL Defect");
                        	allRight.Add("Rework");
                        	allRight.Add("Rework QA");
                        	allRight.Add("MRB");                        	
                        	allRight.Add("Stringer/Bussing Defect");
                        	allRight.Add("A- Marking");
                        	allRight.Add("Line A");
                        	allRight.Add("Line B");
                        }
                         if (read.GetInt32(2) == 10)//supervisor
                        {
                        	isManager = true;
                        	allRight.Add("Package");
                        	allRight.Add("Unpack");
                        	 allRight.Add("EL Defect");
                        	//allRight.Add("MRB");
                        	allRight.Add("A- Marking");                        	
                        	allRight.Add("Line A");
                        	allRight.Add("Line B");
                        } 
                         if (read.GetInt32(2) == 11)//commonuser
                        {
                        	isManager = true;
                        	allRight.Add("Package");
                        	 allRight.Add("EL Defect");
                         	//IsCommonUser =true;
                        	allRight.Add("A- Marking");
                        	allRight.Add("Line A");
                        	allRight.Add("Line B");
                        	
                        }
                            
                        if (read.GetInt32(2) == 12)//quality
                        {
                        	allRight.Add("Unpack");
                        	allRight.Add("Rework QA");
                        	allRight.Add("Quality Inspection");                        	
                        	allRight.Add("EL Defect");
                        	allRight.Add("Rework");
                        	//allRight.Add("Package");
                        	allRight.Add("MRB");
                        	allRight.Add("A- Marking");
                        	allRight.Add("Line A");
                        	allRight.Add("Line B");
                        }
                        if(read.GetInt32(2) == 13)//rework
                        {
                        	allRight.Add("Package");
                        	allRight.Add("Rework");
                      		allRight.Add("Unpack");
                        	allRight.Add("EL Defect");
                        	allRight.Add("Line A");
                        	allRight.Add("Line B");
                      		//allRight.Add("MRB");
                        	//allRight.Add("A- Marking");
                        }
                        if(read.GetInt32(2) == 98)//NPC Defects / MCT
                        {
                        	allRight.Add("EL Defect");                        	
                        	allRight.Add("Stringer/Bussing Defect");
                        	allRight.Add("Line A");
                        	allRight.Add("Line B");
                        }
                        if(read.GetInt32(2) == 97)//Advanced Rework
                        {
                        	allRight.Add("Package");
                        	allRight.Add("Rework");
                      		allRight.Add("Unpack");
                        	allRight.Add("EL Defect");
                        	allRight.Add("Line A");
                        	allRight.Add("Line B");
                        }
                        if(read.GetInt32(2) == 99)//Batch Modification
                        {
                        	allRight.Add("Package");
                        	allRight.Add("Rework");
                      		allRight.Add("Unpack");
                        	allRight.Add("EL Defect");
                      		//allRight.Add("MRB");
                        	//allRight.Add("A- Marking");
                        }
                       #endregion
                        read.Close();
                    }
                    //if (canUse)
                    //{
                    //    cmd.CommandText = "select FuncItemNM from FunctionItem where FuncGroupInterID=" + read.GetInt32(2);
                      
                    //}

                    if (currUserName == "")
                        return false;
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
        }
        public static bool checkUser(string user,string pwd)
        {
            if (curPWD == pwd && currUserWorkID == user)
                return true;
            else
            return false;
        }
        private void textBox_Enter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.Color.Yellow;
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.SystemColors.Window;
        }
        private void btQuit_Click(object sender, EventArgs e)
        {
            //FormMain.showTipMSG("不能退出啊", this.btQuit, "biaoti", ToolTipIcon.Error);
            //Application.Exit();
            System.Environment.Exit(0);
        }
        
        public static void getRegistryKeys()
        {

            using (RegistryKey Key = Registry.CurrentUser.OpenSubKey(@"Software\SCI_solar\CPMSystem", false))
            {
                if (Key == null)
                {
                    //WriteValue("SOAP", "http://58.211.79.6:8910/bsdws/CommServer.jws");
                    //WriteValue("AutoUpDate", "0");
                    //new FormAccount().ShowDialog();
                    dbServer = "CA01S0015";
                    dbName = "CSILabelPrintDB";
                    dbUser = "user";
                    dbPwd = "12345";
                    connectionBase = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, dbName, dbUser, dbPwd);
                }
                else
                {
                    
                	dbServer = "CA01S0015";
                    dbName = "CSILabelPrintDB";
                    dbUser = "user";
                    dbPwd = "12345";
//                	dbServer = Key.GetValue("DBServer", "").ToString();
//                    dbName = Key.GetValue("DBName", "").ToString();
//                    dbUser = Key.GetValue("DBUser", "").ToString();
//                    dbPwd = Key.GetValue("DBPWD", "").ToString();
                    //FormPrint.printLogo = "true".Equals(Key.GetValue("PrintLogo", "").ToString(), StringComparison.OrdinalIgnoreCase);
                    //FormPrint.printTicket = "true".Equals(Key.GetValue("PrintTicket", "").ToString(), StringComparison.OrdinalIgnoreCase);
                    //BaseClass.SOAPURL = Key.GetValue("SOAP", "").ToString();
                    //if (BaseClass.DBServer.Contains("local"))
                    //    BaseClass.connectionBase = string.Format("Data Source={0};Initial Catalog=posbase;Integrated Security=SSPI;", BaseClass.DBServer);
                    //else
                    connectionBase = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, dbName, dbUser, dbPwd);
                    //Key.Close();
                }

            }

        }
 
        public static bool saveRegistryKeys(string[,] keyValueArr)
        {
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"Software\SCI_solar\CPMSystem", RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                try
                {
                    for (int i = 0; i < keyValueArr.GetLength(0); i++)
                    {
                        Key.SetValue(keyValueArr[i, 0], keyValueArr[i, 1], RegistryValueKind.String);
                        //Key.SetValue("DBServer", dbServer, RegistryValueKind.String);
                        //Key.SetValue("DBName", dbName, RegistryValueKind.String);
                        //Key.SetValue("DBPWD", dbPwd, RegistryValueKind.String);
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }

        }
        
        public static bool WriteValue(string ValueName, object ValueData)
        {
            if (ValueName.Length == 0) return false;

            try
            {
                RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"Software\SCI_solar\CPMSystem", RegistryKeyPermissionCheck.ReadWriteSubTree);
                Key.SetValue(ValueName, ValueData, RegistryValueKind.String);
                Key.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string ReadValue( string ValueName)
        {

            if (ValueName.Length == 0) return "";//[ERROR]
            try
            {
                RegistryKey Key = Registry.CurrentUser.OpenSubKey(@"Software\SCI_solar\CPMSystem", false);
                string strKey = Key.GetValue(ValueName, "").ToString();
                Key.Close();
                return strKey;
            }
            catch
            {
                return "";//[ERROR]
            }
        }

        private void btSetUp_Click(object sender, EventArgs e)
        {
            if (verifyUser(tbName.Text.Trim(), tbPWD.Text.Trim()))
            {
                if (canUse)
                {
                    this.Hide();
                    new FormAccount().ShowDialog();
                    this.Show();
                }
                else
                    MessageBox.Show("You hava no right to config database setting!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                FormMain.showTipMSG("Account or Password incorrect!", this.tbPWD, "Warning Message", ToolTipIcon.Warning);
        }
        private static string connStringBase;
        private static string dbServer;
        private static string dbName;
        private static string dbUser;
        private static string dbPwd;
        //private static string currUserName;
        public static string currUserName;
        private static string currUserWorkID;
        private static List<string> allRight;
        private static bool canUse;
        public static bool isManager;
        private static string curPWD;
        /// <summary>
        /// 权限检查
        /// </summary>
        /// <param name="excName">操作名称</param>
        /// <returns>bool 是否有权限</returns>
        public static bool HasPower(string excName)
        {
            // TO DO
            //set { dbServer = value; }&& allRight.Contains(excName)
            if (allRight.Contains(excName)) return true;
            else
            {
                MessageBox.Show("Sorry, you have no right to use“" + excName + "”!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        /// <summary>
        /// 用户名称
        /// </summary>
        public static string CurrUserName
        {
            //set { dbServer = value; }
            get { return currUserName; }
        }
        /// <summary>
        /// 用户工号
        /// </summary>
        public static string CurrUserWorkID
        {
            //set { dbServer = value; }
            get { return currUserWorkID; }
        }
        /// <summary>
        /// 数据库服务器
        /// </summary>
        public static string DBServer
        {
            set { dbServer = value; }
            get { return dbServer; }
        }
        /// <summary>
        /// 数据库账号
        /// </summary>
        public static string DBUser
        {
            set { dbUser = value; }
            get { return dbUser; }
        }
        /// <summary>
        /// 数据库名
        /// </summary>
        public static string DBName
        {
            set { dbName = value; }
            get { return dbName; }
        }
        /// <summary>
        /// 数据库密码
        /// </summary>
        public static string DBPWD
        {
            set { dbPwd = value; }
            get { return dbPwd; }
        }
        /// <summary>
        /// 基础资料库连接字符串
        /// </summary>
        public static string connectionBase
        {
            get { return connStringBase; }
            set { connStringBase = value; }
        }
        /// <summary>
        /// MD5 32位加密
        /// </summary>
        public static string GetMd5Str(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            //string t2 = BitConverter.ToString(md5.ComputeHash(System.Text.Encoding.Default.GetBytes(str)), 4, 8);
            //t2 = t2.Replace("-", "");
            //return t2;
            byte[] data = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(str));
            System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public Mutex objMutex;
        private void FormCover_Load(object sender, EventArgs e)
        {
            objMutex = new Mutex(false, this.Text + "_Mutex");

            if (objMutex.WaitOne(0, false) == false)
            {
                objMutex.Close();
                objMutex = null;
                MessageBox.Show("Cannot start same Package System twice!! Process End!!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                System.Environment.Exit(0);
            }
        }

      
    }
}

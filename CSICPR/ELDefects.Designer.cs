﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 10-03-16
 * Time: 18:00
 * 
 */
namespace CSICPR
{
	partial class ELDefects
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.snTB = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.dt1CB = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.btClear = new System.Windows.Forms.Button();
			this.btSubmit = new System.Windows.Forms.Button();
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.label20 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tBFlexMatch = new System.Windows.Forms.TextBox();
			this.cbMod = new System.Windows.Forms.ComboBox();
			this.label27 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.tbCell6 = new System.Windows.Forms.TextBox();
			this.tbCell5 = new System.Windows.Forms.TextBox();
			this.cbStr6 = new System.Windows.Forms.ComboBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.cbStr5 = new System.Windows.Forms.ComboBox();
			this.dnPanel2 = new System.Windows.Forms.Label();
			this.dnPanel1 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.cbDefectF = new System.Windows.Forms.ComboBox();
			this.tbCell4 = new System.Windows.Forms.TextBox();
			this.tbCell3 = new System.Windows.Forms.TextBox();
			this.cbStr4 = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.cbStr3 = new System.Windows.Forms.ComboBox();
			this.tbCell2 = new System.Windows.Forms.TextBox();
			this.tbCell1 = new System.Windows.Forms.TextBox();
			this.cbStr2 = new System.Windows.Forms.ComboBox();
			this.tbStr = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.tbLine = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.cbStr1 = new System.Windows.Forms.ComboBox();
			this.dt2CB = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.label13 = new System.Windows.Forms.Label();
			this.cbLine = new System.Windows.Forms.ComboBox();
			this.btRefresh = new System.Windows.Forms.Button();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.dt3CB = new System.Windows.Forms.ComboBox();
			this.label21 = new System.Windows.Forms.Label();
			this.dataGridView2 = new System.Windows.Forms.DataGridView();
			this.label4 = new System.Windows.Forms.Label();
			this.btRefresh2 = new System.Windows.Forms.Button();
			this.cbLine2 = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label1.Location = new System.Drawing.Point(3, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(128, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Serial/Stringer";
			// 
			// snTB
			// 
			this.snTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.snTB.Location = new System.Drawing.Point(111, 8);
			this.snTB.Name = "snTB";
			this.snTB.Size = new System.Drawing.Size(187, 23);
			this.snTB.TabIndex = 1;
			this.snTB.TextChanged += new System.EventHandler(this.snTBTextChanged);
			this.snTB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.snTB_KeyPress);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label2.Location = new System.Drawing.Point(3, 57);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(105, 18);
			this.label2.TabIndex = 2;
			this.label2.Text = "Defect Type 1";
			// 
			// dt1CB
			// 
			this.dt1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt1CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt1CB.FormattingEnabled = true;
			this.dt1CB.Items.AddRange(new object[] {
			"Cracked/Chipped Cell Handling",
			"Cracked/Chipped Cell Machine",
			"Unsoldered Busbar Easy Side",
			"Unsoldered Busbar Hard Side",
			"Dead Cell",
			"Polarity",
			"Paper",
			"Piece of Cell",
			"Piece of Ribbon",
			"Hair",
			"Low Efficiency",
			"Missing or Extra Barcode",
			"Busbar Showing",
			"Ribbon Misalignment",
			"Missing Ribbon",
			"Glass Defect",
			"String to String Gap",
			"Cell to Cell Gap",
			"Dirty Square or Strip",
			"No Image",
			"Half Image",
			"Other Machine Failure",
			"Dark Spot",
			"Flux",
			"Change Back Sheet EVA",
			"Reject No Repair",
			"Offset Insulator Strip",
			"Blue Side Unsoldered ",
			"Grey Side Unsoldered",
			"Other"});
			this.dt1CB.Location = new System.Drawing.Point(111, 54);
			this.dt1CB.Name = "dt1CB";
			this.dt1CB.Size = new System.Drawing.Size(215, 24);
			this.dt1CB.TabIndex = 2;
			this.dt1CB.SelectedIndexChanged += new System.EventHandler(this.dt1CB_SelectedIndexChanged);
			this.dt1CB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Dt1CBSelectedIndexChanged);
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label7.Location = new System.Drawing.Point(14, 360);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 23);
			this.label7.TabIndex = 12;
			this.label7.Text = "Remarks";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(135, 323);
			this.textBox2.Multiline = true;
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(746, 90);
			this.textBox2.TabIndex = 6;
			// 
			// btClear
			// 
			this.btClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.btClear.Location = new System.Drawing.Point(325, 425);
			this.btClear.Name = "btClear";
			this.btClear.Size = new System.Drawing.Size(75, 36);
			this.btClear.TabIndex = 36;
			this.btClear.Text = "Clear";
			this.btClear.UseVisualStyleBackColor = true;
			this.btClear.Click += new System.EventHandler(this.btClearClick);
			// 
			// btSubmit
			// 
			this.btSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.btSubmit.Location = new System.Drawing.Point(535, 425);
			this.btSubmit.Name = "btSubmit";
			this.btSubmit.Size = new System.Drawing.Size(75, 36);
			this.btSubmit.TabIndex = 37;
			this.btSubmit.Text = "Submit";
			this.btSubmit.UseVisualStyleBackColor = true;
			this.btSubmit.Click += new System.EventHandler(this.btSubmitClick);
			// 
			// dataGrid1
			// 
			this.dataGrid1.AlternatingBackColor = System.Drawing.SystemColors.ActiveBorder;
			this.dataGrid1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.dataGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
			this.dataGrid1.DataMember = "";
			this.dataGrid1.ForeColor = System.Drawing.Color.YellowGreen;
			this.dataGrid1.GridLineColor = System.Drawing.SystemColors.ControlDark;
			this.dataGrid1.HeaderBackColor = System.Drawing.SystemColors.ControlDark;
			this.dataGrid1.HeaderForeColor = System.Drawing.Color.Cornsilk;
			this.dataGrid1.LinkColor = System.Drawing.SystemColors.InactiveCaption;
			this.dataGrid1.Location = new System.Drawing.Point(0, 1);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.ParentRowsBackColor = System.Drawing.SystemColors.ControlDark;
			this.dataGrid1.ParentRowsForeColor = System.Drawing.Color.Yellow;
			this.dataGrid1.SelectionBackColor = System.Drawing.Color.AliceBlue;
			this.dataGrid1.Size = new System.Drawing.Size(1652, 798);
			this.dataGrid1.TabIndex = 1;
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label20.ForeColor = System.Drawing.Color.Transparent;
			this.label20.Location = new System.Drawing.Point(9, 56);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(1023, 35);
			this.label20.TabIndex = 46;
			this.label20.Text = "EL Defects";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label20.UseCompatibleTextRendering = true;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
			this.panel1.Controls.Add(this.tBFlexMatch);
			this.panel1.Controls.Add(this.cbMod);
			this.panel1.Controls.Add(this.label27);
			this.panel1.Controls.Add(this.textBox1);
			this.panel1.Controls.Add(this.btClear);
			this.panel1.Controls.Add(this.btSubmit);
			this.panel1.Controls.Add(this.tbCell6);
			this.panel1.Controls.Add(this.tbCell5);
			this.panel1.Controls.Add(this.cbStr6);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.label17);
			this.panel1.Controls.Add(this.cbStr5);
			this.panel1.Controls.Add(this.dnPanel2);
			this.panel1.Controls.Add(this.dnPanel1);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.label14);
			this.panel1.Controls.Add(this.label12);
			this.panel1.Controls.Add(this.cbDefectF);
			this.panel1.Controls.Add(this.tbCell4);
			this.panel1.Controls.Add(this.tbCell3);
			this.panel1.Controls.Add(this.cbStr4);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.cbStr3);
			this.panel1.Controls.Add(this.tbCell2);
			this.panel1.Controls.Add(this.tbCell1);
			this.panel1.Controls.Add(this.cbStr2);
			this.panel1.Controls.Add(this.tbStr);
			this.panel1.Controls.Add(this.label11);
			this.panel1.Controls.Add(this.tbLine);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.cbStr1);
			this.panel1.Controls.Add(this.dt2CB);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.textBox2);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.dt1CB);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.snTB);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.panel1.Location = new System.Drawing.Point(9, 96);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1023, 464);
			this.panel1.TabIndex = 48;
			// 
			// tBFlexMatch
			// 
			this.tBFlexMatch.Enabled = false;
			this.tBFlexMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tBFlexMatch.Location = new System.Drawing.Point(111, 31);
			this.tBFlexMatch.Name = "tBFlexMatch";
			this.tBFlexMatch.Size = new System.Drawing.Size(187, 23);
			this.tBFlexMatch.TabIndex = 136;
			this.tBFlexMatch.Visible = false;
			// 
			// cbMod
			// 
			this.cbMod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbMod.FormattingEnabled = true;
			this.cbMod.Items.AddRange(new object[] {
			"6K-P",
			"6K-M",
			"6K-MS",
			"6U-P",
			"6U-M",
			"6X-P",
			"6X-PN",
			"6X-M",
			"6P-P",
			"6P-M"});
			this.cbMod.Location = new System.Drawing.Point(412, 14);
			this.cbMod.Name = "cbMod";
			this.cbMod.Size = new System.Drawing.Size(75, 24);
			this.cbMod.TabIndex = 135;
			// 
			// label27
			// 
			this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label27.Location = new System.Drawing.Point(315, 17);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(95, 20);
			this.label27.TabIndex = 134;
			this.label27.Text = "Module Type:";
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.textBox1.Location = new System.Drawing.Point(967, 13);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(44, 23);
			this.textBox1.TabIndex = 81;
			this.textBox1.Visible = false;
			// 
			// tbCell6
			// 
			this.tbCell6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbCell6.Location = new System.Drawing.Point(888, 222);
			this.tbCell6.Name = "tbCell6";
			this.tbCell6.Size = new System.Drawing.Size(56, 23);
			this.tbCell6.TabIndex = 78;
			// 
			// tbCell5
			// 
			this.tbCell5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbCell5.Location = new System.Drawing.Point(779, 222);
			this.tbCell5.Name = "tbCell5";
			this.tbCell5.Size = new System.Drawing.Size(56, 23);
			this.tbCell5.TabIndex = 76;
			// 
			// cbStr6
			// 
			this.cbStr6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStr6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStr6.FormattingEnabled = true;
			this.cbStr6.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.cbStr6.Location = new System.Drawing.Point(888, 179);
			this.cbStr6.Name = "cbStr6";
			this.cbStr6.Size = new System.Drawing.Size(56, 24);
			this.cbStr6.TabIndex = 77;
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label16.Location = new System.Drawing.Point(725, 225);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(47, 18);
			this.label16.TabIndex = 80;
			this.label16.Text = "Cell";
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label17.Location = new System.Drawing.Point(725, 182);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(47, 18);
			this.label17.TabIndex = 79;
			this.label17.Text = "String";
			// 
			// cbStr5
			// 
			this.cbStr5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStr5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStr5.FormattingEnabled = true;
			this.cbStr5.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.cbStr5.Location = new System.Drawing.Point(779, 179);
			this.cbStr5.Name = "cbStr5";
			this.cbStr5.Size = new System.Drawing.Size(56, 24);
			this.cbStr5.TabIndex = 75;
			// 
			// dnPanel2
			// 
			this.dnPanel2.BackColor = System.Drawing.SystemColors.Window;
			this.dnPanel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dnPanel2.Location = new System.Drawing.Point(456, 92);
			this.dnPanel2.Name = "dnPanel2";
			this.dnPanel2.Size = new System.Drawing.Size(216, 23);
			this.dnPanel2.TabIndex = 74;
			this.dnPanel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.dnPanel2.Visible = false;
			// 
			// dnPanel1
			// 
			this.dnPanel1.BackColor = System.Drawing.SystemColors.Window;
			this.dnPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dnPanel1.Location = new System.Drawing.Point(111, 91);
			this.dnPanel1.Name = "dnPanel1";
			this.dnPanel1.Size = new System.Drawing.Size(215, 23);
			this.dnPanel1.TabIndex = 73;
			this.dnPanel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.dnPanel1.Visible = false;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label15.Location = new System.Drawing.Point(351, 96);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(105, 18);
			this.label15.TabIndex = 72;
			this.label15.Text = "Defect Name 2";
			this.label15.Visible = false;
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label14.Location = new System.Drawing.Point(3, 96);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(105, 18);
			this.label14.TabIndex = 71;
			this.label14.Text = "Defect Name 1";
			this.label14.Visible = false;
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label12.Location = new System.Drawing.Point(14, 279);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(119, 18);
			this.label12.TabIndex = 70;
			this.label12.Text = "Defect Cause";
			this.label12.Visible = false;
			// 
			// cbDefectF
			// 
			this.cbDefectF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDefectF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbDefectF.FormattingEnabled = true;
			this.cbDefectF.Items.AddRange(new object[] {
			"Machine",
			"Manual"});
			this.cbDefectF.Location = new System.Drawing.Point(135, 276);
			this.cbDefectF.Name = "cbDefectF";
			this.cbDefectF.Size = new System.Drawing.Size(143, 24);
			this.cbDefectF.TabIndex = 5;
			this.cbDefectF.Visible = false;
			// 
			// tbCell4
			// 
			this.tbCell4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbCell4.Location = new System.Drawing.Point(547, 222);
			this.tbCell4.Name = "tbCell4";
			this.tbCell4.Size = new System.Drawing.Size(56, 23);
			this.tbCell4.TabIndex = 13;
			// 
			// tbCell3
			// 
			this.tbCell3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbCell3.Location = new System.Drawing.Point(438, 222);
			this.tbCell3.Name = "tbCell3";
			this.tbCell3.Size = new System.Drawing.Size(56, 23);
			this.tbCell3.TabIndex = 11;
			// 
			// cbStr4
			// 
			this.cbStr4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStr4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStr4.FormattingEnabled = true;
			this.cbStr4.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.cbStr4.Location = new System.Drawing.Point(547, 179);
			this.cbStr4.Name = "cbStr4";
			this.cbStr4.Size = new System.Drawing.Size(56, 24);
			this.cbStr4.TabIndex = 12;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label5.Location = new System.Drawing.Point(384, 225);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(47, 18);
			this.label5.TabIndex = 65;
			this.label5.Text = "Cell";
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label8.Location = new System.Drawing.Point(384, 182);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(47, 18);
			this.label8.TabIndex = 64;
			this.label8.Text = "String";
			// 
			// cbStr3
			// 
			this.cbStr3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStr3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStr3.FormattingEnabled = true;
			this.cbStr3.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.cbStr3.Location = new System.Drawing.Point(438, 179);
			this.cbStr3.Name = "cbStr3";
			this.cbStr3.Size = new System.Drawing.Size(56, 24);
			this.cbStr3.TabIndex = 10;
			// 
			// tbCell2
			// 
			this.tbCell2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbCell2.Location = new System.Drawing.Point(214, 226);
			this.tbCell2.Name = "tbCell2";
			this.tbCell2.Size = new System.Drawing.Size(56, 23);
			this.tbCell2.TabIndex = 8;
			// 
			// tbCell1
			// 
			this.tbCell1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbCell1.Location = new System.Drawing.Point(105, 226);
			this.tbCell1.Name = "tbCell1";
			this.tbCell1.Size = new System.Drawing.Size(56, 23);
			this.tbCell1.TabIndex = 4;
			// 
			// cbStr2
			// 
			this.cbStr2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStr2.FormattingEnabled = true;
			this.cbStr2.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.cbStr2.Location = new System.Drawing.Point(214, 183);
			this.cbStr2.Name = "cbStr2";
			this.cbStr2.Size = new System.Drawing.Size(56, 24);
			this.cbStr2.TabIndex = 7;
			// 
			// tbStr
			// 
			this.tbStr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbStr.Location = new System.Drawing.Point(698, 14);
			this.tbStr.Name = "tbStr";
			this.tbStr.ReadOnly = true;
			this.tbStr.Size = new System.Drawing.Size(69, 23);
			this.tbStr.TabIndex = 58;
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label11.Location = new System.Drawing.Point(640, 17);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(60, 20);
			this.label11.TabIndex = 57;
			this.label11.Text = "Stringer";
			// 
			// tbLine
			// 
			this.tbLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbLine.Location = new System.Drawing.Point(581, 14);
			this.tbLine.Name = "tbLine";
			this.tbLine.ReadOnly = true;
			this.tbLine.Size = new System.Drawing.Size(44, 23);
			this.tbLine.TabIndex = 56;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label10.Location = new System.Drawing.Point(546, 16);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(45, 20);
			this.label10.TabIndex = 55;
			this.label10.Text = "Line";
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label9.Location = new System.Drawing.Point(46, 229);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(59, 18);
			this.label9.TabIndex = 54;
			this.label9.Text = "Cell";
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label6.Location = new System.Drawing.Point(46, 186);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(59, 18);
			this.label6.TabIndex = 50;
			this.label6.Text = "String";
			// 
			// cbStr1
			// 
			this.cbStr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStr1.FormattingEnabled = true;
			this.cbStr1.Items.AddRange(new object[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F"});
			this.cbStr1.Location = new System.Drawing.Point(105, 183);
			this.cbStr1.Name = "cbStr1";
			this.cbStr1.Size = new System.Drawing.Size(56, 24);
			this.cbStr1.TabIndex = 3;
			// 
			// dt2CB
			// 
			this.dt2CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt2CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt2CB.FormattingEnabled = true;
			this.dt2CB.Items.AddRange(new object[] {
			"Cracked/Chipped Cell Handling",
			"Cracked/Chipped Cell Machine",
			"Unsoldered Busbar Easy Side",
			"Unsoldered Busbar Hard Side",
			"Dead Cell",
			"Polarity",
			"Paper",
			"Piece of Cell",
			"Piece of Ribbon",
			"Hair",
			"Low Efficiency",
			"Missing or Extra Barcode",
			"Busbar Showing",
			"Ribbon Misalignment",
			"Missing Ribbon",
			"Glass Defect",
			"String to String Gap",
			"Cell to Cell Gap",
			"Dirty Square or Strip",
			"No Image",
			"Half Image",
			"Other Machine Failure",
			"Dark Spot",
			"Flux",
			"Change Back Sheet EVA",
			"Reject No Repair",
			"Offset Insulator Strip",
			"Blue Side Unsoldered ",
			"Grey Side Unsoldered",
			"Other"});
			this.dt2CB.Location = new System.Drawing.Point(457, 54);
			this.dt2CB.Name = "dt2CB";
			this.dt2CB.Size = new System.Drawing.Size(215, 24);
			this.dt2CB.TabIndex = 9;
			this.dt2CB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Dt2CBSelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label3.Location = new System.Drawing.Point(351, 57);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(105, 18);
			this.label3.TabIndex = 43;
			this.label3.Text = "Defect Type 2";
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(1038, 96);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.Size = new System.Drawing.Size(596, 464);
			this.dataGridView1.TabIndex = 49;
			// 
			// label13
			// 
			this.label13.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label13.ForeColor = System.Drawing.Color.Transparent;
			this.label13.Location = new System.Drawing.Point(1038, 56);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(596, 35);
			this.label13.TabIndex = 50;
			this.label13.Text = "Summary";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label13.UseCompatibleTextRendering = true;
			// 
			// cbLine
			// 
			this.cbLine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbLine.FormattingEnabled = true;
			this.cbLine.Items.AddRange(new object[] {
			"A",
			"B",
			"D"});
			this.cbLine.Location = new System.Drawing.Point(1540, 61);
			this.cbLine.Name = "cbLine";
			this.cbLine.Size = new System.Drawing.Size(85, 26);
			this.cbLine.TabIndex = 51;
			this.cbLine.SelectedIndexChanged += new System.EventHandler(this.CbLineSelectedIndexChanged);
			// 
			// btRefresh
			// 
			this.btRefresh.Location = new System.Drawing.Point(1430, 61);
			this.btRefresh.Name = "btRefresh";
			this.btRefresh.Size = new System.Drawing.Size(104, 26);
			this.btRefresh.TabIndex = 52;
			this.btRefresh.Text = "Refresh";
			this.btRefresh.UseVisualStyleBackColor = true;
			this.btRefresh.Click += new System.EventHandler(this.BtRefreshClick);
			// 
			// label18
			// 
			this.label18.BackColor = System.Drawing.SystemColors.Window;
			this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label18.Location = new System.Drawing.Point(804, 188);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(216, 23);
			this.label18.TabIndex = 84;
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.label18.Visible = false;
			// 
			// label19
			// 
			this.label19.BackColor = System.Drawing.SystemColors.ControlDark;
			this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label19.Location = new System.Drawing.Point(699, 192);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(105, 18);
			this.label19.TabIndex = 83;
			this.label19.Text = "Defect Name 3";
			this.label19.Visible = false;
			// 
			// dt3CB
			// 
			this.dt3CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt3CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt3CB.FormattingEnabled = true;
			this.dt3CB.Items.AddRange(new object[] {
			"Cracked/Chipped Cell Handling",
			"Cracked/Chipped Cell Machine",
			"Unsoldered Busbar Easy Side",
			"Unsoldered Busbar Hard Side",
			"Dead Cell",
			"Polarity",
			"Paper",
			"Piece of Cell",
			"Piece of Ribbon",
			"Hair",
			"Low Efficiency",
			"Missing or Extra Barcode",
			"Busbar Showing",
			"Ribbon Misalignment",
			"Missing Ribbon",
			"Glass Defect",
			"String to String Gap",
			"Cell to Cell Gap",
			"Dirty Square or Strip",
			"No Image",
			"Half Image",
			"Other Machine Failure",
			"Dark Spot",
			"Flux",
			"Change Back Sheet EVA",
			"Reject No Repair",
			"Offset Insulator Strip",
			"Blue Side Unsoldered ",
			"Grey Side Unsoldered",
			"Other"});
			this.dt3CB.Location = new System.Drawing.Point(805, 150);
			this.dt3CB.Name = "dt3CB";
			this.dt3CB.Size = new System.Drawing.Size(215, 24);
			this.dt3CB.TabIndex = 81;
			// 
			// label21
			// 
			this.label21.BackColor = System.Drawing.SystemColors.ControlDark;
			this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label21.Location = new System.Drawing.Point(699, 153);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(105, 18);
			this.label21.TabIndex = 82;
			this.label21.Text = "Defect Type 3";
			// 
			// dataGridView2
			// 
			this.dataGridView2.AllowUserToAddRows = false;
			this.dataGridView2.AllowUserToDeleteRows = false;
			this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView2.Location = new System.Drawing.Point(9, 609);
			this.dataGridView2.Name = "dataGridView2";
			this.dataGridView2.ReadOnly = true;
			this.dataGridView2.Size = new System.Drawing.Size(1625, 175);
			this.dataGridView2.TabIndex = 85;
			this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView2_CellClick);
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label4.ForeColor = System.Drawing.Color.Transparent;
			this.label4.Location = new System.Drawing.Point(9, 571);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(1625, 35);
			this.label4.TabIndex = 86;
			this.label4.Text = "Recent Entries";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label4.UseCompatibleTextRendering = true;
			// 
			// btRefresh2
			// 
			this.btRefresh2.Location = new System.Drawing.Point(1430, 576);
			this.btRefresh2.Name = "btRefresh2";
			this.btRefresh2.Size = new System.Drawing.Size(104, 26);
			this.btRefresh2.TabIndex = 87;
			this.btRefresh2.Text = "Refresh";
			this.btRefresh2.UseVisualStyleBackColor = true;
			this.btRefresh2.Click += new System.EventHandler(this.BtRefresh2Click);
			// 
			// cbLine2
			// 
			this.cbLine2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLine2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbLine2.FormattingEnabled = true;
			this.cbLine2.Items.AddRange(new object[] {
			"A",
			"B",
			"D"});
			this.cbLine2.Location = new System.Drawing.Point(1540, 576);
			this.cbLine2.Name = "cbLine2";
			this.cbLine2.Size = new System.Drawing.Size(85, 26);
			this.cbLine2.TabIndex = 88;
			this.cbLine2.SelectedIndexChanged += new System.EventHandler(this.CbLine2SelectedIndexChanged);
			// 
			// ELDefects
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.ClientSize = new System.Drawing.Size(1654, 799);
			this.Controls.Add(this.cbLine2);
			this.Controls.Add(this.btRefresh2);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.dataGridView2);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.btRefresh);
			this.Controls.Add(this.label19);
			this.Controls.Add(this.cbLine);
			this.Controls.Add(this.dt3CB);
			this.Controls.Add(this.label21);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label20);
			this.Controls.Add(this.dataGrid1);
			this.Name = "ELDefects";
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
			this.ResumeLayout(false);

		}
		
		private System.Windows.Forms.ComboBox cbStr2;
		private System.Windows.Forms.ComboBox cbStr3;
		private System.Windows.Forms.TextBox tbCell3;
		private System.Windows.Forms.TextBox tbCell4;
		private System.Windows.Forms.ComboBox cbDefectF;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox dt2CB;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbCell1;
		private System.Windows.Forms.ComboBox cbLine;
		private System.Windows.Forms.Button btRefresh;
		private System.Windows.Forms.ComboBox cbStr1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox tbCell2;
		private System.Windows.Forms.ComboBox cbStr4;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox tbLine;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox tbStr;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Button btSubmit;
		private System.Windows.Forms.Button btClear;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox dt1CB;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox snTB;
		private System.Windows.Forms.Label label1;	
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label dnPanel2;
		private System.Windows.Forms.Label dnPanel1;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox tbCell6;
		private System.Windows.Forms.TextBox tbCell5;
		private System.Windows.Forms.ComboBox cbStr6;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.ComboBox cbStr5;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.ComboBox dt3CB;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.DataGridView dataGridView2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btRefresh2;
		private System.Windows.Forms.ComboBox cbLine2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.ComboBox cbMod;
		private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tBFlexMatch;
    }
}

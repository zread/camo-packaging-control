﻿/*
 * Created by SharpDevelop.
 * User: Shen.Yu
 * Date: 3/14/2012
 * Time: 10:38 AM
 * 
 */
using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
	/// <summary>
	/// Description of FormQualityCheck.
	/// </summary>
	public partial class FormAMinusMarking : Form
	{
        DateTime LastKeyPress;
		public FormAMinusMarking()
		{
			InitializeComponent();
			string line = ToolsClass.getConfig("StationNo").Replace('\n', ' ').Trim();			
			string station = ToolsClass.getConfig("KUKASta").Replace('\n', ' ').Trim();       
            label7.Text = "Station: " + station;
			
			switch (line)
			{
				case "6":
					label4.Text = "A";
					break;
				case "7":
					label4.Text = "B";
					break;
				case "9":
					label4.Text = "D";
					break;
				default:
					label4.Text = "T";
					break;
			}
		}
		
		// This is to ensure only one instance of this form is loaded
		private static FormAMinusMarking theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormAMinusMarking();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		void Button1Click(object sender, EventArgs e)
		{
			markasAMinus();
		}
		
		void markasAMinus(){
			string sn = textBox1.Text.ToString().Trim();
            string Remark = label5.Text;
            string defectcode = textBox2.Text.ToString().Trim();
			string station = ToolsClass.getConfig("KUKASta").Replace('\n', ' ').Trim();
				
			if(sn.Length != 13 && sn.Length != 14)
			{
				label6.Text = ("Serial number "+sn+" does not match the correct length.");
				MessageBox.Show("Serial number "+sn+" is invalid, please try scanning again.");
				label6.BackColor = System.Drawing.Color.Yellow;
				textBox1.Clear();
				return;
			}
			if(sn.Substring(0,1) != "1")
			{
				label6.Text = ("Serial number "+sn+" does not begin with a 1, please re enter.");
				MessageBox.Show("Serial number "+sn+" is invalid, please try scanning again.");
				label6.BackColor = System.Drawing.Color.Yellow;
				textBox1.Clear();
				return;
			}
			if(sn.Substring(5,1) != "4")
			{
				label6.Text = ("Serial number "+sn+" does not have the correct format, please re enter.");
				MessageBox.Show("Serial number "+sn+" is invalid, please try scanning again.");
				label6.BackColor = System.Drawing.Color.Yellow;
				textBox1.Clear();
				return;
			}
			foreach(char digit in sn){
				if(!Char.IsDigit(digit)){
					label6.Text = ("Serial number "+sn+" is invalid, please try scanning again.");
					MessageBox.Show("Serial number "+sn+" is invalid, please try scanning again.");
					label6.BackColor = System.Drawing.Color.Yellow;
					textBox1.Clear();
					return;
				}
			}
			
            if (!isPacked(textBox1.Text.ToString().Trim()))
            {
            	label6.Text = ("Module "+sn+" has been packaged, its class cannot be changed.");
				label6.BackColor = System.Drawing.Color.Yellow;
				textBox1.Clear();				
                return;
            }
            
            string sql = "insert into QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark, station) values ('"+sn+"', 'A-', getdate(), '"+FormCover.CurrUserWorkID+"', '" + comboBox1.Text + "', '" + Remark + "', '" + station + "')";
            ToolsClass.ExecuteNonQuery(sql);
           
            label6.Text = ("Module "+sn+" has been graded as class A-.");
            label6.BackColor = System.Drawing.SystemColors.AppWorkspace;
            
            textBox1.Clear();
            textBox2.Clear();
		}
		
		void Button2MouseClick(object sender, MouseEventArgs e)
		{
			string sn = textBox1.Text.ToString().Trim();
			string moduleClass = "";
			
			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length.");
				return;
			}
			
			string sql = "select moduleclass from qualityjudge where id = (select max(ID) from qualityjudge where sn = '"+sn+"')";
			SqlDataReader sdr = ToolsClass.GetDataReader(sql);
			if(sdr != null && sdr.Read())
			{
				moduleClass = sdr.GetString(0);
			}
			else
			{
				moduleClass = "A";
			}
			sdr.Close();
            sdr = null;
            
            MessageBox.Show("Module "+sn+" is class "+moduleClass);
		}
		
		private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            string connection = ToolsClass.NEWPACKINGConnStr();
            sql = string.Format(sql, sSerialNumber);
            SqlDataReader sdr = ToolsClass.GetDataReader(sql,connection);
            if(sdr != null && sdr.Read())
            {
            	return false;
            }
            sdr.Close();
            sdr = null;
            return true;
        }
		
		void TextBox1Enter(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.Color.Yellow;
			
		}
		
		void TextBox1Leave(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.SystemColors.Window;
		}
		
		void ComboBox1KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r' && textBox1.Text.Length != 0)
			{				
				string sql = "Select Name from DefectName where DefectCode = '" + comboBox1.Text.ToString() + "'";
				SqlDataReader sdr = ToolsClass.GetDataReader(sql);
				if(sdr != null && sdr.Read())
				{
					label5.Text = sdr.GetString(0);					
					markasAMinus();
					textBox1.Text = "";
					textBox1.Select();					
					comboBox1.Text = "";
				}
				else{
					label6.Text = "That is not a valid Defect Code";
				}
			}			
		}
		void TextBox1KeyPress(object sender, KeyPressEventArgs e)
		{
            DateTime NowTime = DateTime.Now;

            this.SuspendLayout();
            if (e.KeyChar == '\r' && textBox1.Text.Length != 0)
            {
                label6.Text = "";
                label5.Text = "";
                comboBox1.Select();
            }
            else
            {
                if (textBox1.Text.Length < 0)
                {
                    LastKeyPress = NowTime;
                }
                else
                {
                    TimeSpan Diff = NowTime - LastKeyPress;
                    Int32 ms = (Int32)Diff.TotalMilliseconds;
                    if (ms > 150)
                        textBox1.Text = "";

                    LastKeyPress = DateTime.Now;
                }
            }
            this.ResumeLayout();
               
        }

        private void TextBox2KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' )
            {
                markasAMinus();
            }
        }
    }
}
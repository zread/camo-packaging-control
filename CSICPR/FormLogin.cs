﻿using System;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormLogin : Form
    {
        public FormLogin(bool isLock = false)
        {
            InitializeComponent();
            if (isLock)
            {
                this.tbName.Enabled = false;
                this.tbName.Text = FormCover.CurrUserWorkID;
            }
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            string user,pwd;
            user = this.tbName.Text.Trim();
            if (user.Length == 0)
            {
                //显示Prompting Message
                FormMain.showTipMSG("Account can not be empty", this.tbName, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            
            pwd = this.tbPWD.Text.Trim();
            if (pwd.Length == 0)
            {
                //显示Prompting Message
                FormMain.showTipMSG("Password can not be empty", this.tbPWD, "Prompting Message", ToolTipIcon.Warning);
                return;
            }


            if (FormCover.checkUser(user, pwd))
            {
                this.DialogResult = DialogResult.OK;
                this.Owner.Close();
            }
            else
            {
                FormMain.showTipMSG("Account or Password incorrect!", this.tbPWD, "Prompting Message", ToolTipIcon.Warning);
            }

        }

        private void btQuit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void textBox_Enter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.Color.Yellow;
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.SystemColors.Window;
        }
    }
}

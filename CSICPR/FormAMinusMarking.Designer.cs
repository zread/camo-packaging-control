﻿/*
 * Created in SharpDevelop.
 * User: Shen.Yu
 * Create Date: 3/14/2012
 * Time: 10:38 AM
 *  
 */
namespace CSICPR
{
	partial class FormAMinusMarking
	{

		/// Designer variable used to keep track of non-visual components.
		private System.ComponentModel.IContainer components = null;
		

		/// Disposes resources used by the form.
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}		
		
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button2 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox1.Location = new System.Drawing.Point(286, 172);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(313, 44);
			this.textBox1.TabIndex = 0;
			this.textBox1.Enter += new System.EventHandler(this.TextBox1Enter);
			this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox1KeyPress);
			this.textBox1.Leave += new System.EventHandler(this.TextBox1Leave);
			// 
			// button2
			// 
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.Location = new System.Drawing.Point(356, 302);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(172, 46);
			this.button2.TabIndex = 2;
			this.button2.Text = "Check";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button2MouseClick);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(186, 175);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(77, 38);
			this.label1.TabIndex = 6;
			this.label1.Text = "SN:";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(142, 249);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(121, 35);
			this.label2.TabIndex = 7;
			this.label2.Text = "Defect:";
			// 
			// comboBox1
			// 
			this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.ImeMode = System.Windows.Forms.ImeMode.On;
			this.comboBox1.Items.AddRange(new object[] {
			"DEF0101",
			"DEF0102",
			"DEF0103",
			"DEF0104",
			"DEF0105",
			"DEF0106",
			"DEF0107",
			"DEF0108",
			"DEF0109",
			"DEF0110",
			"DEF0111",
			"DEF0112",
			"DEF0113",
			"DEF0114",
			"DEF0115",
			"DEF0116",
			"DEF0117",
			"DEF0118",
			"DEF0119",
			"DEF0120",
			"DEF0121",
			"DEF0122",
			"DEF0123",
			"DEF0124",
			"DEF0125",
			"DEF0126",
			"DEF0127",
			"DEF0128",
			"DEF0129",
			"",
			""});
			this.comboBox1.Location = new System.Drawing.Point(286, 246);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(313, 45);
			this.comboBox1.TabIndex = 1;
			this.comboBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox1KeyPress);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(305, 25);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(189, 79);
			this.label3.TabIndex = 8;
			this.label3.Text = "Line:";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(490, 25);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(91, 79);
			this.label4.TabIndex = 9;
			this.label4.Text = "X";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(732, 175);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(377, 119);
			this.label5.TabIndex = 10;
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(94, 423);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(688, 177);
			this.label6.TabIndex = 11;
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(291, 104);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(292, 40);
			this.label7.TabIndex = 12;
			this.label7.Text = "X";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textBox2
			// 
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox2.Location = new System.Drawing.Point(286, 246);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(313, 44);
			this.textBox2.TabIndex = 1;
			this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox2KeyPress);
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.Location = new System.Drawing.Point(356, 356);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(172, 46);
			this.button1.TabIndex = 2;
			this.button1.Text = "Grade";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// FormAMinusMarking
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.ClientSize = new System.Drawing.Size(1109, 609);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.textBox2);
			this.Name = "FormAMinusMarking";
			this.Text = "FormQualityInspection";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button button1;
	}
}


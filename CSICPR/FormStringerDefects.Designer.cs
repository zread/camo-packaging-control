﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 12-08-16
 * Time: 10:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CSICPR
{
	partial class FormStringerDefects
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox snTB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cbStr1;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.ComboBox cbLine;
		private System.Windows.Forms.ComboBox dt1CB;
		private System.Windows.Forms.ComboBox dt2CB;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ComboBox cbC1;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox tbQTY2;
		private System.Windows.Forms.TextBox tbQTY1;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbMO;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cbC10;
		private System.Windows.Forms.ComboBox cbC9;
		private System.Windows.Forms.TextBox tbQTY10;
		private System.Windows.Forms.TextBox tbQTY9;
		private System.Windows.Forms.ComboBox dt10CB;
		private System.Windows.Forms.ComboBox dt9CB;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.ComboBox cbC8;
		private System.Windows.Forms.ComboBox cbC7;
		private System.Windows.Forms.TextBox tbQTY8;
		private System.Windows.Forms.TextBox tbQTY7;
		private System.Windows.Forms.ComboBox dt8CB;
		private System.Windows.Forms.ComboBox dt7CB;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.ComboBox cbC6;
		private System.Windows.Forms.ComboBox cbC5;
		private System.Windows.Forms.TextBox tbQTY6;
		private System.Windows.Forms.TextBox tbQTY5;
		private System.Windows.Forms.ComboBox dt6CB;
		private System.Windows.Forms.ComboBox dt5CB;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.ComboBox cbC4;
		private System.Windows.Forms.ComboBox cbC3;
		private System.Windows.Forms.TextBox tbQTY4;
		private System.Windows.Forms.TextBox tbQTY3;
		private System.Windows.Forms.ComboBox dt4CB;
		private System.Windows.Forms.ComboBox dt3CB;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox cbC2;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button btSubmit;
		private System.Windows.Forms.Button btClear;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label lbRepaired;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Button btRepair;
		private System.Windows.Forms.NumericUpDown numRepair;
		private System.Windows.Forms.ComboBox cbStringer1;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ComboBox cbStringer10;
		private System.Windows.Forms.ComboBox cbStringer3;
		private System.Windows.Forms.ComboBox cbStringer5;
		private System.Windows.Forms.ComboBox cbStringer7;
		private System.Windows.Forms.ComboBox cbStringer9;
		private System.Windows.Forms.ComboBox cbStringer8;
		private System.Windows.Forms.ComboBox cbStringer6;
		private System.Windows.Forms.ComboBox cbStringer4;
		private System.Windows.Forms.ComboBox cbStringer2;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.ComboBox cbLine2;
		private System.Windows.Forms.DataGridView dataGridView2;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Button btRefresh;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.ComboBox cbMod;
		private System.Windows.Forms.Label label27;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label20 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.snTB = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.cbStr1 = new System.Windows.Forms.ComboBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.cbLine = new System.Windows.Forms.ComboBox();
			this.dt1CB = new System.Windows.Forms.ComboBox();
			this.dt2CB = new System.Windows.Forms.ComboBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.cbMod = new System.Windows.Forms.ComboBox();
			this.label27 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.cbStringer10 = new System.Windows.Forms.ComboBox();
			this.cbStringer3 = new System.Windows.Forms.ComboBox();
			this.cbStringer5 = new System.Windows.Forms.ComboBox();
			this.cbStringer7 = new System.Windows.Forms.ComboBox();
			this.cbStringer9 = new System.Windows.Forms.ComboBox();
			this.cbStringer8 = new System.Windows.Forms.ComboBox();
			this.cbStringer6 = new System.Windows.Forms.ComboBox();
			this.cbStringer4 = new System.Windows.Forms.ComboBox();
			this.cbStringer2 = new System.Windows.Forms.ComboBox();
			this.label25 = new System.Windows.Forms.Label();
			this.cbStringer1 = new System.Windows.Forms.ComboBox();
			this.cbC10 = new System.Windows.Forms.ComboBox();
			this.cbC9 = new System.Windows.Forms.ComboBox();
			this.tbQTY10 = new System.Windows.Forms.TextBox();
			this.tbQTY9 = new System.Windows.Forms.TextBox();
			this.dt10CB = new System.Windows.Forms.ComboBox();
			this.dt9CB = new System.Windows.Forms.ComboBox();
			this.label19 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.cbC8 = new System.Windows.Forms.ComboBox();
			this.cbC7 = new System.Windows.Forms.ComboBox();
			this.tbQTY8 = new System.Windows.Forms.TextBox();
			this.tbQTY7 = new System.Windows.Forms.TextBox();
			this.dt8CB = new System.Windows.Forms.ComboBox();
			this.dt7CB = new System.Windows.Forms.ComboBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.cbC6 = new System.Windows.Forms.ComboBox();
			this.cbC5 = new System.Windows.Forms.ComboBox();
			this.tbQTY6 = new System.Windows.Forms.TextBox();
			this.tbQTY5 = new System.Windows.Forms.TextBox();
			this.dt6CB = new System.Windows.Forms.ComboBox();
			this.dt5CB = new System.Windows.Forms.ComboBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.cbC4 = new System.Windows.Forms.ComboBox();
			this.cbC3 = new System.Windows.Forms.ComboBox();
			this.tbQTY4 = new System.Windows.Forms.TextBox();
			this.tbQTY3 = new System.Windows.Forms.TextBox();
			this.dt4CB = new System.Windows.Forms.ComboBox();
			this.dt3CB = new System.Windows.Forms.ComboBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.cbC2 = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.cbC1 = new System.Windows.Forms.ComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.tbQTY2 = new System.Windows.Forms.TextBox();
			this.tbQTY1 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.tbMO = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btSubmit = new System.Windows.Forms.Button();
			this.btClear = new System.Windows.Forms.Button();
			this.label22 = new System.Windows.Forms.Label();
			this.lbRepaired = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.btRepair = new System.Windows.Forms.Button();
			this.numRepair = new System.Windows.Forms.NumericUpDown();
			this.label23 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.dataGridView2 = new System.Windows.Forms.DataGridView();
			this.label26 = new System.Windows.Forms.Label();
			this.btRefresh = new System.Windows.Forms.Button();
			this.cbLine2 = new System.Windows.Forms.ComboBox();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numRepair)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
			this.SuspendLayout();
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label20.ForeColor = System.Drawing.Color.Transparent;
			this.label20.Location = new System.Drawing.Point(12, 31);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(1230, 35);
			this.label20.TabIndex = 49;
			this.label20.Text = "Stringer/Bussing Defects";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label20.UseCompatibleTextRendering = true;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label1.Location = new System.Drawing.Point(14, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(128, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Stringer/Bussing:";
			// 
			// snTB
			// 
			this.snTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.snTB.Location = new System.Drawing.Point(712, 15);
			this.snTB.Name = "snTB";
			this.snTB.Size = new System.Drawing.Size(187, 23);
			this.snTB.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label2.Location = new System.Drawing.Point(18, 78);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(115, 18);
			this.label2.TabIndex = 2;
			this.label2.Text = "Defect Type 1:";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label7.Location = new System.Drawing.Point(14, 276);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 23);
			this.label7.TabIndex = 12;
			this.label7.Text = "Remarks:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(135, 239);
			this.textBox2.Multiline = true;
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(921, 90);
			this.textBox2.TabIndex = 93;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label3.Location = new System.Drawing.Point(646, 78);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(105, 18);
			this.label3.TabIndex = 43;
			this.label3.Text = "Defect Type 2:";
			// 
			// cbStr1
			// 
			this.cbStr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStr1.FormattingEnabled = true;
			this.cbStr1.Items.AddRange(new object[] {
			"Stringer",
			"Bussing"});
			this.cbStr1.Location = new System.Drawing.Point(135, 14);
			this.cbStr1.Name = "cbStr1";
			this.cbStr1.Size = new System.Drawing.Size(155, 24);
			this.cbStr1.TabIndex = 1;
			this.cbStr1.SelectedIndexChanged += new System.EventHandler(this.CbStr1SelectedIndexChanged);
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label10.Location = new System.Drawing.Point(319, 16);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(45, 20);
			this.label10.TabIndex = 55;
			this.label10.Text = "Line:";
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label11.Location = new System.Drawing.Point(439, 51);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(65, 20);
			this.label11.TabIndex = 57;
			this.label11.Text = "Stringer";
			// 
			// cbLine
			// 
			this.cbLine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbLine.FormattingEnabled = true;
			this.cbLine.Items.AddRange(new object[] {
			"A",
			"B",
			"D"});
			this.cbLine.Location = new System.Drawing.Point(365, 14);
			this.cbLine.Name = "cbLine";
			this.cbLine.Size = new System.Drawing.Size(56, 24);
			this.cbLine.TabIndex = 2;
			this.cbLine.SelectedIndexChanged += new System.EventHandler(this.CbLineSelectedIndexChanged);
			// 
			// dt1CB
			// 
			this.dt1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt1CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt1CB.FormattingEnabled = true;
			this.dt1CB.Location = new System.Drawing.Point(139, 75);
			this.dt1CB.Name = "dt1CB";
			this.dt1CB.Size = new System.Drawing.Size(231, 24);
			this.dt1CB.TabIndex = 6;
			// 
			// dt2CB
			// 
			this.dt2CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt2CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt2CB.FormattingEnabled = true;
			this.dt2CB.Location = new System.Drawing.Point(761, 75);
			this.dt2CB.Name = "dt2CB";
			this.dt2CB.Size = new System.Drawing.Size(231, 24);
			this.dt2CB.TabIndex = 9;
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.cbMod);
			this.panel1.Controls.Add(this.label27);
			this.panel1.Controls.Add(this.textBox1);
			this.panel1.Controls.Add(this.cbStringer10);
			this.panel1.Controls.Add(this.cbStringer3);
			this.panel1.Controls.Add(this.cbStringer5);
			this.panel1.Controls.Add(this.cbStringer7);
			this.panel1.Controls.Add(this.cbStringer9);
			this.panel1.Controls.Add(this.cbStringer8);
			this.panel1.Controls.Add(this.cbStringer6);
			this.panel1.Controls.Add(this.cbStringer4);
			this.panel1.Controls.Add(this.cbStringer2);
			this.panel1.Controls.Add(this.label25);
			this.panel1.Controls.Add(this.cbStringer1);
			this.panel1.Controls.Add(this.cbC10);
			this.panel1.Controls.Add(this.cbC9);
			this.panel1.Controls.Add(this.tbQTY10);
			this.panel1.Controls.Add(this.tbQTY9);
			this.panel1.Controls.Add(this.dt10CB);
			this.panel1.Controls.Add(this.dt9CB);
			this.panel1.Controls.Add(this.label19);
			this.panel1.Controls.Add(this.label21);
			this.panel1.Controls.Add(this.cbC8);
			this.panel1.Controls.Add(this.cbC7);
			this.panel1.Controls.Add(this.tbQTY8);
			this.panel1.Controls.Add(this.tbQTY7);
			this.panel1.Controls.Add(this.dt8CB);
			this.panel1.Controls.Add(this.dt7CB);
			this.panel1.Controls.Add(this.label17);
			this.panel1.Controls.Add(this.label18);
			this.panel1.Controls.Add(this.cbC6);
			this.panel1.Controls.Add(this.cbC5);
			this.panel1.Controls.Add(this.tbQTY6);
			this.panel1.Controls.Add(this.tbQTY5);
			this.panel1.Controls.Add(this.dt6CB);
			this.panel1.Controls.Add(this.dt5CB);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.cbC4);
			this.panel1.Controls.Add(this.cbC3);
			this.panel1.Controls.Add(this.tbQTY4);
			this.panel1.Controls.Add(this.tbQTY3);
			this.panel1.Controls.Add(this.dt4CB);
			this.panel1.Controls.Add(this.dt3CB);
			this.panel1.Controls.Add(this.label13);
			this.panel1.Controls.Add(this.label14);
			this.panel1.Controls.Add(this.cbC2);
			this.panel1.Controls.Add(this.label12);
			this.panel1.Controls.Add(this.cbC1);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.tbQTY2);
			this.panel1.Controls.Add(this.tbQTY1);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.tbMO);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.dt2CB);
			this.panel1.Controls.Add(this.dt1CB);
			this.panel1.Controls.Add(this.cbLine);
			this.panel1.Controls.Add(this.label11);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.cbStr1);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.textBox2);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.snTB);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.panel1.Location = new System.Drawing.Point(12, 71);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1230, 340);
			this.panel1.TabIndex = 50;
			// 
			// cbMod
			// 
			this.cbMod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbMod.FormattingEnabled = true;
			this.cbMod.Items.AddRange(new object[] {
			"6U-P",
			"6U-M",
			"6K-P",
			"6K-M",
			"6K-MS",
			"6X-P",
			"6X-PN",
			"6X-M",
			"6P-P",
			"6P-M"});
			this.cbMod.Location = new System.Drawing.Point(1026, 15);
			this.cbMod.Name = "cbMod";
			this.cbMod.Size = new System.Drawing.Size(77, 24);
			this.cbMod.TabIndex = 133;
			// 
			// label27
			// 
			this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label27.Location = new System.Drawing.Point(928, 18);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(95, 20);
			this.label27.TabIndex = 132;
			this.label27.Text = "Module Type:";
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.textBox1.Location = new System.Drawing.Point(1121, 15);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(103, 23);
			this.textBox1.TabIndex = 131;
			this.textBox1.Visible = false;
			// 
			// cbStringer10
			// 
			this.cbStringer10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer10.FormattingEnabled = true;
			this.cbStringer10.Location = new System.Drawing.Point(1056, 196);
			this.cbStringer10.Name = "cbStringer10";
			this.cbStringer10.Size = new System.Drawing.Size(56, 24);
			this.cbStringer10.TabIndex = 130;
			// 
			// cbStringer3
			// 
			this.cbStringer3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer3.FormattingEnabled = true;
			this.cbStringer3.Location = new System.Drawing.Point(439, 106);
			this.cbStringer3.Name = "cbStringer3";
			this.cbStringer3.Size = new System.Drawing.Size(56, 24);
			this.cbStringer3.TabIndex = 129;
			// 
			// cbStringer5
			// 
			this.cbStringer5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer5.FormattingEnabled = true;
			this.cbStringer5.Location = new System.Drawing.Point(439, 136);
			this.cbStringer5.Name = "cbStringer5";
			this.cbStringer5.Size = new System.Drawing.Size(56, 24);
			this.cbStringer5.TabIndex = 128;
			// 
			// cbStringer7
			// 
			this.cbStringer7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer7.FormattingEnabled = true;
			this.cbStringer7.Location = new System.Drawing.Point(439, 166);
			this.cbStringer7.Name = "cbStringer7";
			this.cbStringer7.Size = new System.Drawing.Size(56, 24);
			this.cbStringer7.TabIndex = 127;
			// 
			// cbStringer9
			// 
			this.cbStringer9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer9.FormattingEnabled = true;
			this.cbStringer9.Location = new System.Drawing.Point(439, 196);
			this.cbStringer9.Name = "cbStringer9";
			this.cbStringer9.Size = new System.Drawing.Size(56, 24);
			this.cbStringer9.TabIndex = 126;
			// 
			// cbStringer8
			// 
			this.cbStringer8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer8.FormattingEnabled = true;
			this.cbStringer8.Location = new System.Drawing.Point(1056, 166);
			this.cbStringer8.Name = "cbStringer8";
			this.cbStringer8.Size = new System.Drawing.Size(56, 24);
			this.cbStringer8.TabIndex = 125;
			// 
			// cbStringer6
			// 
			this.cbStringer6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer6.FormattingEnabled = true;
			this.cbStringer6.Location = new System.Drawing.Point(1056, 136);
			this.cbStringer6.Name = "cbStringer6";
			this.cbStringer6.Size = new System.Drawing.Size(56, 24);
			this.cbStringer6.TabIndex = 124;
			// 
			// cbStringer4
			// 
			this.cbStringer4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer4.FormattingEnabled = true;
			this.cbStringer4.Location = new System.Drawing.Point(1056, 106);
			this.cbStringer4.Name = "cbStringer4";
			this.cbStringer4.Size = new System.Drawing.Size(56, 24);
			this.cbStringer4.TabIndex = 123;
			// 
			// cbStringer2
			// 
			this.cbStringer2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer2.FormattingEnabled = true;
			this.cbStringer2.Location = new System.Drawing.Point(1056, 75);
			this.cbStringer2.Name = "cbStringer2";
			this.cbStringer2.Size = new System.Drawing.Size(56, 24);
			this.cbStringer2.TabIndex = 122;
			// 
			// label25
			// 
			this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label25.Location = new System.Drawing.Point(1053, 50);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(66, 20);
			this.label25.TabIndex = 121;
			this.label25.Text = "Stringer";
			// 
			// cbStringer1
			// 
			this.cbStringer1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStringer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbStringer1.FormattingEnabled = true;
			this.cbStringer1.Location = new System.Drawing.Point(439, 75);
			this.cbStringer1.Name = "cbStringer1";
			this.cbStringer1.Size = new System.Drawing.Size(56, 24);
			this.cbStringer1.TabIndex = 5;
			// 
			// cbC10
			// 
			this.cbC10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC10.FormattingEnabled = true;
			this.cbC10.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC10.Location = new System.Drawing.Point(1128, 196);
			this.cbC10.Name = "cbC10";
			this.cbC10.Size = new System.Drawing.Size(84, 24);
			this.cbC10.TabIndex = 120;
			this.cbC10.Visible = false;
			// 
			// cbC9
			// 
			this.cbC9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC9.FormattingEnabled = true;
			this.cbC9.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC9.Location = new System.Drawing.Point(515, 196);
			this.cbC9.Name = "cbC9";
			this.cbC9.Size = new System.Drawing.Size(84, 24);
			this.cbC9.TabIndex = 119;
			this.cbC9.Visible = false;
			// 
			// tbQTY10
			// 
			this.tbQTY10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY10.Location = new System.Drawing.Point(1009, 197);
			this.tbQTY10.Name = "tbQTY10";
			this.tbQTY10.Size = new System.Drawing.Size(29, 23);
			this.tbQTY10.TabIndex = 118;
			this.tbQTY10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbQTY9
			// 
			this.tbQTY9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY9.Location = new System.Drawing.Point(392, 197);
			this.tbQTY9.Name = "tbQTY9";
			this.tbQTY9.Size = new System.Drawing.Size(29, 23);
			this.tbQTY9.TabIndex = 117;
			this.tbQTY9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// dt10CB
			// 
			this.dt10CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt10CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt10CB.FormattingEnabled = true;
			this.dt10CB.Location = new System.Drawing.Point(761, 196);
			this.dt10CB.Name = "dt10CB";
			this.dt10CB.Size = new System.Drawing.Size(231, 24);
			this.dt10CB.TabIndex = 116;
			// 
			// dt9CB
			// 
			this.dt9CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt9CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt9CB.FormattingEnabled = true;
			this.dt9CB.Location = new System.Drawing.Point(139, 196);
			this.dt9CB.Name = "dt9CB";
			this.dt9CB.Size = new System.Drawing.Size(231, 24);
			this.dt9CB.TabIndex = 115;
			// 
			// label19
			// 
			this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label19.Location = new System.Drawing.Point(646, 199);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(122, 18);
			this.label19.TabIndex = 114;
			this.label19.Text = "Defect Type 10:";
			// 
			// label21
			// 
			this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label21.Location = new System.Drawing.Point(18, 199);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(115, 18);
			this.label21.TabIndex = 113;
			this.label21.Text = "Defect Type 9:";
			// 
			// cbC8
			// 
			this.cbC8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC8.FormattingEnabled = true;
			this.cbC8.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC8.Location = new System.Drawing.Point(1128, 166);
			this.cbC8.Name = "cbC8";
			this.cbC8.Size = new System.Drawing.Size(84, 24);
			this.cbC8.TabIndex = 112;
			this.cbC8.Visible = false;
			// 
			// cbC7
			// 
			this.cbC7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC7.FormattingEnabled = true;
			this.cbC7.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC7.Location = new System.Drawing.Point(515, 166);
			this.cbC7.Name = "cbC7";
			this.cbC7.Size = new System.Drawing.Size(84, 24);
			this.cbC7.TabIndex = 111;
			this.cbC7.Visible = false;
			// 
			// tbQTY8
			// 
			this.tbQTY8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY8.Location = new System.Drawing.Point(1009, 167);
			this.tbQTY8.Name = "tbQTY8";
			this.tbQTY8.Size = new System.Drawing.Size(29, 23);
			this.tbQTY8.TabIndex = 110;
			this.tbQTY8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbQTY7
			// 
			this.tbQTY7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY7.Location = new System.Drawing.Point(392, 167);
			this.tbQTY7.Name = "tbQTY7";
			this.tbQTY7.Size = new System.Drawing.Size(29, 23);
			this.tbQTY7.TabIndex = 109;
			this.tbQTY7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// dt8CB
			// 
			this.dt8CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt8CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt8CB.FormattingEnabled = true;
			this.dt8CB.Location = new System.Drawing.Point(761, 166);
			this.dt8CB.Name = "dt8CB";
			this.dt8CB.Size = new System.Drawing.Size(231, 24);
			this.dt8CB.TabIndex = 108;
			// 
			// dt7CB
			// 
			this.dt7CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt7CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt7CB.FormattingEnabled = true;
			this.dt7CB.Location = new System.Drawing.Point(139, 166);
			this.dt7CB.Name = "dt7CB";
			this.dt7CB.Size = new System.Drawing.Size(231, 24);
			this.dt7CB.TabIndex = 107;
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label17.Location = new System.Drawing.Point(646, 169);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(105, 18);
			this.label17.TabIndex = 106;
			this.label17.Text = "Defect Type 8:";
			// 
			// label18
			// 
			this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label18.Location = new System.Drawing.Point(18, 169);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(115, 18);
			this.label18.TabIndex = 105;
			this.label18.Text = "Defect Type 7:";
			// 
			// cbC6
			// 
			this.cbC6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC6.FormattingEnabled = true;
			this.cbC6.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC6.Location = new System.Drawing.Point(1128, 136);
			this.cbC6.Name = "cbC6";
			this.cbC6.Size = new System.Drawing.Size(84, 24);
			this.cbC6.TabIndex = 104;
			this.cbC6.Visible = false;
			// 
			// cbC5
			// 
			this.cbC5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC5.FormattingEnabled = true;
			this.cbC5.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC5.Location = new System.Drawing.Point(515, 136);
			this.cbC5.Name = "cbC5";
			this.cbC5.Size = new System.Drawing.Size(84, 24);
			this.cbC5.TabIndex = 103;
			this.cbC5.Visible = false;
			// 
			// tbQTY6
			// 
			this.tbQTY6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY6.Location = new System.Drawing.Point(1009, 137);
			this.tbQTY6.Name = "tbQTY6";
			this.tbQTY6.Size = new System.Drawing.Size(29, 23);
			this.tbQTY6.TabIndex = 102;
			this.tbQTY6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbQTY5
			// 
			this.tbQTY5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY5.Location = new System.Drawing.Point(392, 137);
			this.tbQTY5.Name = "tbQTY5";
			this.tbQTY5.Size = new System.Drawing.Size(29, 23);
			this.tbQTY5.TabIndex = 101;
			this.tbQTY5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// dt6CB
			// 
			this.dt6CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt6CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt6CB.FormattingEnabled = true;
			this.dt6CB.Location = new System.Drawing.Point(761, 136);
			this.dt6CB.Name = "dt6CB";
			this.dt6CB.Size = new System.Drawing.Size(231, 24);
			this.dt6CB.TabIndex = 100;
			// 
			// dt5CB
			// 
			this.dt5CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt5CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt5CB.FormattingEnabled = true;
			this.dt5CB.Location = new System.Drawing.Point(139, 136);
			this.dt5CB.Name = "dt5CB";
			this.dt5CB.Size = new System.Drawing.Size(231, 24);
			this.dt5CB.TabIndex = 99;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label15.Location = new System.Drawing.Point(646, 139);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(105, 18);
			this.label15.TabIndex = 98;
			this.label15.Text = "Defect Type 6:";
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label16.Location = new System.Drawing.Point(18, 139);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(115, 18);
			this.label16.TabIndex = 97;
			this.label16.Text = "Defect Type 5:";
			// 
			// cbC4
			// 
			this.cbC4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC4.FormattingEnabled = true;
			this.cbC4.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC4.Location = new System.Drawing.Point(1128, 106);
			this.cbC4.Name = "cbC4";
			this.cbC4.Size = new System.Drawing.Size(84, 24);
			this.cbC4.TabIndex = 17;
			this.cbC4.Visible = false;
			// 
			// cbC3
			// 
			this.cbC3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC3.FormattingEnabled = true;
			this.cbC3.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC3.Location = new System.Drawing.Point(515, 106);
			this.cbC3.Name = "cbC3";
			this.cbC3.Size = new System.Drawing.Size(84, 24);
			this.cbC3.TabIndex = 14;
			this.cbC3.Visible = false;
			// 
			// tbQTY4
			// 
			this.tbQTY4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY4.Location = new System.Drawing.Point(1009, 107);
			this.tbQTY4.Name = "tbQTY4";
			this.tbQTY4.Size = new System.Drawing.Size(29, 23);
			this.tbQTY4.TabIndex = 16;
			this.tbQTY4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbQTY3
			// 
			this.tbQTY3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY3.Location = new System.Drawing.Point(392, 107);
			this.tbQTY3.Name = "tbQTY3";
			this.tbQTY3.Size = new System.Drawing.Size(29, 23);
			this.tbQTY3.TabIndex = 13;
			this.tbQTY3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// dt4CB
			// 
			this.dt4CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt4CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt4CB.FormattingEnabled = true;
			this.dt4CB.Location = new System.Drawing.Point(761, 106);
			this.dt4CB.Name = "dt4CB";
			this.dt4CB.Size = new System.Drawing.Size(231, 24);
			this.dt4CB.TabIndex = 15;
			// 
			// dt3CB
			// 
			this.dt3CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dt3CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dt3CB.FormattingEnabled = true;
			this.dt3CB.ItemHeight = 16;
			this.dt3CB.Location = new System.Drawing.Point(139, 106);
			this.dt3CB.Name = "dt3CB";
			this.dt3CB.Size = new System.Drawing.Size(231, 24);
			this.dt3CB.TabIndex = 12;
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label13.Location = new System.Drawing.Point(646, 109);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(105, 18);
			this.label13.TabIndex = 90;
			this.label13.Text = "Defect Type 4:";
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label14.Location = new System.Drawing.Point(18, 109);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(115, 18);
			this.label14.TabIndex = 89;
			this.label14.Text = "Defect Type 3:";
			// 
			// cbC2
			// 
			this.cbC2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC2.FormattingEnabled = true;
			this.cbC2.ItemHeight = 16;
			this.cbC2.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC2.Location = new System.Drawing.Point(1128, 75);
			this.cbC2.Name = "cbC2";
			this.cbC2.Size = new System.Drawing.Size(84, 24);
			this.cbC2.TabIndex = 11;
			this.cbC2.Visible = false;
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label12.Location = new System.Drawing.Point(1146, 51);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(49, 20);
			this.label12.TabIndex = 87;
			this.label12.Text = "Cause";
			this.label12.Visible = false;
			// 
			// cbC1
			// 
			this.cbC1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbC1.FormattingEnabled = true;
			this.cbC1.Items.AddRange(new object[] {
			"Manual",
			"Machine"});
			this.cbC1.Location = new System.Drawing.Point(515, 75);
			this.cbC1.Name = "cbC1";
			this.cbC1.Size = new System.Drawing.Size(84, 24);
			this.cbC1.TabIndex = 8;
			this.cbC1.Visible = false;
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label9.Location = new System.Drawing.Point(533, 50);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(49, 20);
			this.label9.TabIndex = 85;
			this.label9.Text = "Cause";
			this.label9.Visible = false;
			// 
			// tbQTY2
			// 
			this.tbQTY2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY2.Location = new System.Drawing.Point(1009, 76);
			this.tbQTY2.Name = "tbQTY2";
			this.tbQTY2.Size = new System.Drawing.Size(29, 23);
			this.tbQTY2.TabIndex = 10;
			this.tbQTY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbQTY1
			// 
			this.tbQTY1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbQTY1.Location = new System.Drawing.Point(392, 76);
			this.tbQTY1.Name = "tbQTY1";
			this.tbQTY1.Size = new System.Drawing.Size(29, 23);
			this.tbQTY1.TabIndex = 7;
			this.tbQTY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label8.Location = new System.Drawing.Point(1009, 50);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(35, 20);
			this.label8.TabIndex = 82;
			this.label8.Text = "Qty";
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label6.Location = new System.Drawing.Point(392, 50);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(35, 20);
			this.label6.TabIndex = 81;
			this.label6.Text = "Qty";
			// 
			// tbMO
			// 
			this.tbMO.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.tbMO.Location = new System.Drawing.Point(541, 15);
			this.tbMO.Name = "tbMO";
			this.tbMO.Size = new System.Drawing.Size(103, 23);
			this.tbMO.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label5.Location = new System.Drawing.Point(449, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(89, 20);
			this.label5.TabIndex = 79;
			this.label5.Text = "MO Number:";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label4.Location = new System.Drawing.Point(671, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(35, 20);
			this.label4.TabIndex = 77;
			this.label4.Text = "SN:";
			// 
			// btSubmit
			// 
			this.btSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.btSubmit.Location = new System.Drawing.Point(773, 417);
			this.btSubmit.Name = "btSubmit";
			this.btSubmit.Size = new System.Drawing.Size(75, 36);
			this.btSubmit.TabIndex = 52;
			this.btSubmit.Text = "Submit";
			this.btSubmit.UseVisualStyleBackColor = true;
			this.btSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// btClear
			// 
			this.btClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.btClear.Location = new System.Drawing.Point(339, 417);
			this.btClear.Name = "btClear";
			this.btClear.Size = new System.Drawing.Size(75, 36);
			this.btClear.TabIndex = 51;
			this.btClear.Text = "Clear";
			this.btClear.UseVisualStyleBackColor = true;
			this.btClear.Click += new System.EventHandler(this.BtClearClick);
			// 
			// label22
			// 
			this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label22.Location = new System.Drawing.Point(15, 507);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(190, 20);
			this.label22.TabIndex = 80;
			this.label22.Text = "Number Waiting For Repair:";
			// 
			// lbRepaired
			// 
			this.lbRepaired.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.lbRepaired.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbRepaired.Location = new System.Drawing.Point(230, 539);
			this.lbRepaired.Name = "lbRepaired";
			this.lbRepaired.Size = new System.Drawing.Size(70, 23);
			this.lbRepaired.TabIndex = 81;
			this.lbRepaired.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label24
			// 
			this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label24.Location = new System.Drawing.Point(15, 541);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(190, 20);
			this.label24.TabIndex = 82;
			this.label24.Text = "Number Repaired This Shift:";
			// 
			// btRepair
			// 
			this.btRepair.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.btRepair.Location = new System.Drawing.Point(328, 507);
			this.btRepair.Name = "btRepair";
			this.btRepair.Size = new System.Drawing.Size(75, 24);
			this.btRepair.TabIndex = 83;
			this.btRepair.Text = "Submit";
			this.btRepair.UseVisualStyleBackColor = true;
			this.btRepair.Click += new System.EventHandler(this.BtRepairClick);
			// 
			// numRepair
			// 
			this.numRepair.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.numRepair.Location = new System.Drawing.Point(230, 507);
			this.numRepair.Name = "numRepair";
			this.numRepair.Size = new System.Drawing.Size(70, 24);
			this.numRepair.TabIndex = 84;
			// 
			// label23
			// 
			this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
			this.label23.Location = new System.Drawing.Point(15, 473);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(276, 18);
			this.label23.TabIndex = 114;
			this.label23.Text = "End of Shift Statistics";
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Location = new System.Drawing.Point(11, 463);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(403, 110);
			this.panel2.TabIndex = 115;
			// 
			// dataGridView2
			// 
			this.dataGridView2.AllowUserToAddRows = false;
			this.dataGridView2.AllowUserToDeleteRows = false;
			this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView2.Location = new System.Drawing.Point(11, 624);
			this.dataGridView2.Name = "dataGridView2";
			this.dataGridView2.ReadOnly = true;
			this.dataGridView2.Size = new System.Drawing.Size(1283, 187);
			this.dataGridView2.TabIndex = 116;
			this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView2_CellClick);
			// 
			// label26
			// 
			this.label26.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label26.ForeColor = System.Drawing.Color.Transparent;
			this.label26.Location = new System.Drawing.Point(11, 586);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(1283, 35);
			this.label26.TabIndex = 117;
			this.label26.Text = "Recent Entries";
			this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label26.UseCompatibleTextRendering = true;
			// 
			// btRefresh
			// 
			this.btRefresh.Location = new System.Drawing.Point(856, 592);
			this.btRefresh.Name = "btRefresh";
			this.btRefresh.Size = new System.Drawing.Size(104, 26);
			this.btRefresh.TabIndex = 118;
			this.btRefresh.Text = "Refresh";
			this.btRefresh.UseVisualStyleBackColor = true;
			this.btRefresh.Click += new System.EventHandler(this.BtRefreshClick);
			// 
			// cbLine2
			// 
			this.cbLine2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLine2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbLine2.FormattingEnabled = true;
			this.cbLine2.Items.AddRange(new object[] {
			"A",
			"B",
			"D"});
			this.cbLine2.Location = new System.Drawing.Point(966, 592);
			this.cbLine2.Name = "cbLine2";
			this.cbLine2.Size = new System.Drawing.Size(85, 26);
			this.cbLine2.TabIndex = 119;
			// 
			// FormStringerDefects
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(1306, 896);
			this.Controls.Add(this.cbLine2);
			this.Controls.Add(this.btRefresh);
			this.Controls.Add(this.label26);
			this.Controls.Add(this.dataGridView2);
			this.Controls.Add(this.label23);
			this.Controls.Add(this.numRepair);
			this.Controls.Add(this.btRepair);
			this.Controls.Add(this.label24);
			this.Controls.Add(this.lbRepaired);
			this.Controls.Add(this.label22);
			this.Controls.Add(this.btSubmit);
			this.Controls.Add(this.btClear);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label20);
			this.Controls.Add(this.panel2);
			this.Name = "FormStringerDefects";
			this.Text = "FormStringerDefects";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numRepair)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
			this.ResumeLayout(false);

		}
	}
}

﻿namespace CSICPR
{
    partial class FormAccountManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.txtAccount = new System.Windows.Forms.TextBox();
        	this.label1 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	this.txtPsw = new System.Windows.Forms.TextBox();
        	this.label3 = new System.Windows.Forms.Label();
        	this.txtUserName = new System.Windows.Forms.TextBox();
        	this.rBtnYes = new System.Windows.Forms.RadioButton();
        	this.rBtnNo = new System.Windows.Forms.RadioButton();
        	this.label4 = new System.Windows.Forms.Label();
        	this.AccountList = new System.Windows.Forms.DataGridView();
        	this.COL0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.COL0002 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.COL0003 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.COL0004 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.btnAdd = new System.Windows.Forms.Button();
        	this.btnModify = new System.Windows.Forms.Button();
        	this.btnDel = new System.Windows.Forms.Button();
        	this.btnReturn = new System.Windows.Forms.Button();
        	this.label5 = new System.Windows.Forms.Label();
        	this.txtCRM = new System.Windows.Forms.TextBox();
        	this.rBtnSuper = new System.Windows.Forms.RadioButton();
        	this.rBQuality = new System.Windows.Forms.RadioButton();
        	this.rBRework = new System.Windows.Forms.RadioButton();
        	this.rbtnArework = new System.Windows.Forms.RadioButton();
        	this.rbtnRepair = new System.Windows.Forms.RadioButton();
        	((System.ComponentModel.ISupportInitialize)(this.AccountList)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// txtAccount
        	// 
        	this.txtAccount.Location = new System.Drawing.Point(116, 5);
        	this.txtAccount.Name = "txtAccount";
        	this.txtAccount.Size = new System.Drawing.Size(75, 20);
        	this.txtAccount.TabIndex = 0;
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(51, 9);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(54, 13);
        	this.label1.TabIndex = 1;
        	this.label1.Tag = "LBL0005";
        	this.label1.Text = "Account *";
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(45, 39);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(60, 13);
        	this.label2.TabIndex = 2;
        	this.label2.Tag = "LBL0006";
        	this.label2.Text = "Password *";
        	// 
        	// txtPsw
        	// 
        	this.txtPsw.Location = new System.Drawing.Point(116, 35);
        	this.txtPsw.Name = "txtPsw";
        	this.txtPsw.PasswordChar = '*';
        	this.txtPsw.Size = new System.Drawing.Size(75, 20);
        	this.txtPsw.TabIndex = 3;
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.Location = new System.Drawing.Point(291, 10);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(30, 13);
        	this.label3.TabIndex = 4;
        	this.label3.Tag = "LBL0007";
        	this.label3.Text = "Note";
        	// 
        	// txtUserName
        	// 
        	this.txtUserName.Location = new System.Drawing.Point(340, 7);
        	this.txtUserName.Name = "txtUserName";
        	this.txtUserName.Size = new System.Drawing.Size(75, 20);
        	this.txtUserName.TabIndex = 2;
        	// 
        	// rBtnYes
        	// 
        	this.rBtnYes.AutoSize = true;
        	this.rBtnYes.Location = new System.Drawing.Point(116, 69);
        	this.rBtnYes.Name = "rBtnYes";
        	this.rBtnYes.Size = new System.Drawing.Size(73, 17);
        	this.rBtnYes.TabIndex = 6;
        	this.rBtnYes.TabStop = true;
        	this.rBtnYes.Text = "Administer";
        	this.rBtnYes.UseVisualStyleBackColor = true;
        	// 
        	// rBtnNo
        	// 
        	this.rBtnNo.AutoSize = true;
        	this.rBtnNo.Location = new System.Drawing.Point(282, 69);
        	this.rBtnNo.Name = "rBtnNo";
        	this.rBtnNo.Size = new System.Drawing.Size(91, 17);
        	this.rBtnNo.TabIndex = 8;
        	this.rBtnNo.TabStop = true;
        	this.rBtnNo.Text = "Common User";
        	this.rBtnNo.UseVisualStyleBackColor = true;
        	// 
        	// label4
        	// 
        	this.label4.AutoSize = true;
        	this.label4.Location = new System.Drawing.Point(33, 69);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(68, 13);
        	this.label4.TabIndex = 9;
        	this.label4.Tag = "LBL0008";
        	this.label4.Text = "User Group *";
        	// 
        	// AccountList
        	// 
        	this.AccountList.AllowUserToAddRows = false;
        	this.AccountList.AllowUserToResizeRows = false;
        	this.AccountList.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.AccountList.ColumnHeadersHeight = 30;
        	this.AccountList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        	this.AccountList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.COL0001,
        	        	        	this.COL0002,
        	        	        	this.COL0003,
        	        	        	this.COL0004});
        	this.AccountList.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.AccountList.Location = new System.Drawing.Point(0, 165);
        	this.AccountList.Name = "AccountList";
        	this.AccountList.ReadOnly = true;
        	this.AccountList.RowHeadersWidth = 16;
        	this.AccountList.RowTemplate.Height = 23;
        	this.AccountList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        	this.AccountList.Size = new System.Drawing.Size(510, 152);
        	this.AccountList.TabIndex = 16;
        	this.AccountList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AccountList_CellClick);
        	this.AccountList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AccountList_CellDoubleClick);
        	// 
        	// COL0001
        	// 
        	this.COL0001.HeaderText = "Account";
        	this.COL0001.Name = "COL0001";
        	this.COL0001.ReadOnly = true;
        	// 
        	// COL0002
        	// 
        	this.COL0002.HeaderText = "Password";
        	this.COL0002.Name = "COL0002";
        	this.COL0002.ReadOnly = true;
        	// 
        	// COL0003
        	// 
        	this.COL0003.HeaderText = "Explanation";
        	this.COL0003.Name = "COL0003";
        	this.COL0003.ReadOnly = true;
        	// 
        	// COL0004
        	// 
        	this.COL0004.HeaderText = "UserGroup";
        	this.COL0004.Name = "COL0004";
        	this.COL0004.ReadOnly = true;
        	// 
        	// btnAdd
        	// 
        	this.btnAdd.Location = new System.Drawing.Point(61, 123);
        	this.btnAdd.Name = "btnAdd";
        	this.btnAdd.Size = new System.Drawing.Size(75, 25);
        	this.btnAdd.TabIndex = 10;
        	this.btnAdd.Tag = "BTN0004";
        	this.btnAdd.Text = "Add";
        	this.btnAdd.UseVisualStyleBackColor = true;
        	this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
        	// 
        	// btnModify
        	// 
        	this.btnModify.Location = new System.Drawing.Point(152, 123);
        	this.btnModify.Name = "btnModify";
        	this.btnModify.Size = new System.Drawing.Size(75, 25);
        	this.btnModify.TabIndex = 11;
        	this.btnModify.Tag = "BTN0005";
        	this.btnModify.Text = "Modify";
        	this.btnModify.UseVisualStyleBackColor = true;
        	this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
        	// 
        	// btnDel
        	// 
        	this.btnDel.Location = new System.Drawing.Point(247, 123);
        	this.btnDel.Name = "btnDel";
        	this.btnDel.Size = new System.Drawing.Size(75, 25);
        	this.btnDel.TabIndex = 12;
        	this.btnDel.Tag = "BTN0006";
        	this.btnDel.Text = "Delete";
        	this.btnDel.UseVisualStyleBackColor = true;
        	this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
        	// 
        	// btnReturn
        	// 
        	this.btnReturn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        	this.btnReturn.Location = new System.Drawing.Point(338, 123);
        	this.btnReturn.Name = "btnReturn";
        	this.btnReturn.Size = new System.Drawing.Size(75, 25);
        	this.btnReturn.TabIndex = 13;
        	this.btnReturn.Tag = "BTN0007";
        	this.btnReturn.Text = "Return";
        	this.btnReturn.UseVisualStyleBackColor = true;
        	// 
        	// label5
        	// 
        	this.label5.AutoSize = true;
        	this.label5.Location = new System.Drawing.Point(219, 40);
        	this.label5.Name = "label5";
        	this.label5.Size = new System.Drawing.Size(98, 13);
        	this.label5.TabIndex = 14;
        	this.label5.Tag = "LBL0006";
        	this.label5.Text = "Confirm Password *";
        	// 
        	// txtCRM
        	// 
        	this.txtCRM.Location = new System.Drawing.Point(340, 36);
        	this.txtCRM.Name = "txtCRM";
        	this.txtCRM.PasswordChar = '*';
        	this.txtCRM.Size = new System.Drawing.Size(75, 20);
        	this.txtCRM.TabIndex = 4;
        	// 
        	// rBtnSuper
        	// 
        	this.rBtnSuper.AutoSize = true;
        	this.rBtnSuper.Location = new System.Drawing.Point(199, 69);
        	this.rBtnSuper.Name = "rBtnSuper";
        	this.rBtnSuper.Size = new System.Drawing.Size(75, 17);
        	this.rBtnSuper.TabIndex = 7;
        	this.rBtnSuper.TabStop = true;
        	this.rBtnSuper.Text = "Supervisor";
        	this.rBtnSuper.UseVisualStyleBackColor = true;
        	// 
        	// rBQuality
        	// 
        	this.rBQuality.AutoSize = true;
        	this.rBQuality.Location = new System.Drawing.Point(379, 69);
        	this.rBQuality.Name = "rBQuality";
        	this.rBQuality.Size = new System.Drawing.Size(57, 17);
        	this.rBQuality.TabIndex = 17;
        	this.rBQuality.TabStop = true;
        	this.rBQuality.Text = "Quality\r\n";
        	this.rBQuality.UseVisualStyleBackColor = true;
        	// 
        	// rBRework
        	// 
        	this.rBRework.AutoSize = true;
        	this.rBRework.Location = new System.Drawing.Point(441, 69);
        	this.rBRework.Name = "rBRework";
        	this.rBRework.Size = new System.Drawing.Size(62, 17);
        	this.rBRework.TabIndex = 18;
        	this.rBRework.TabStop = true;
        	this.rBRework.Text = "Rework\r\n";
        	this.rBRework.UseVisualStyleBackColor = true;
        	// 
        	// rbtnArework
        	// 
        	this.rbtnArework.AutoSize = true;
        	this.rbtnArework.Location = new System.Drawing.Point(178, 92);
        	this.rbtnArework.Name = "rbtnArework";
        	this.rbtnArework.Size = new System.Drawing.Size(114, 17);
        	this.rbtnArework.TabIndex = 20;
        	this.rbtnArework.TabStop = true;
        	this.rbtnArework.Text = "Advanced Rework\r\n";
        	this.rbtnArework.UseVisualStyleBackColor = true;
        	// 
        	// rbtnRepair
        	// 
        	this.rbtnRepair.AutoSize = true;
        	this.rbtnRepair.Location = new System.Drawing.Point(116, 92);
        	this.rbtnRepair.Name = "rbtnRepair";
        	this.rbtnRepair.Size = new System.Drawing.Size(56, 17);
        	this.rbtnRepair.TabIndex = 19;
        	this.rbtnRepair.TabStop = true;
        	this.rbtnRepair.Text = "Repair";
        	this.rbtnRepair.UseVisualStyleBackColor = true;
        	// 
        	// FormAccountManager
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(510, 317);
        	this.Controls.Add(this.rbtnArework);
        	this.Controls.Add(this.rbtnRepair);
        	this.Controls.Add(this.rBRework);
        	this.Controls.Add(this.rBQuality);
        	this.Controls.Add(this.rBtnSuper);
        	this.Controls.Add(this.txtCRM);
        	this.Controls.Add(this.label5);
        	this.Controls.Add(this.btnReturn);
        	this.Controls.Add(this.btnDel);
        	this.Controls.Add(this.btnModify);
        	this.Controls.Add(this.btnAdd);
        	this.Controls.Add(this.AccountList);
        	this.Controls.Add(this.label4);
        	this.Controls.Add(this.rBtnNo);
        	this.Controls.Add(this.rBtnYes);
        	this.Controls.Add(this.txtUserName);
        	this.Controls.Add(this.label3);
        	this.Controls.Add(this.txtPsw);
        	this.Controls.Add(this.label2);
        	this.Controls.Add(this.label1);
        	this.Controls.Add(this.txtAccount);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.Name = "FormAccountManager";
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Tag = "FRM0002";
        	this.Text = "Account Manager";
        	this.Load += new System.EventHandler(this.FormAccountManager_Load);
        	((System.ComponentModel.ISupportInitialize)(this.AccountList)).EndInit();
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }
        private System.Windows.Forms.RadioButton rbtnRepair;
        private System.Windows.Forms.RadioButton rbtnArework;
        private System.Windows.Forms.RadioButton rBRework;
        private System.Windows.Forms.RadioButton rBQuality;

        #endregion

        private System.Windows.Forms.TextBox txtAccount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPsw;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.RadioButton rBtnYes;
        private System.Windows.Forms.RadioButton rBtnNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView AccountList;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCRM;
        private System.Windows.Forms.RadioButton rBtnSuper;
        private System.Windows.Forms.DataGridViewTextBoxColumn COL0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn COL0002;
        private System.Windows.Forms.DataGridViewTextBoxColumn COL0003;
        private System.Windows.Forms.DataGridViewTextBoxColumn COL0004;
    }
}
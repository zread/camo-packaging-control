﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormReworkQA : Form
    {
        public FormReworkQA()
        {
            InitializeComponent();
        }

        private static FormReworkQA theSingleton = null;

        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormReworkQA();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                {
                    theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }


        private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            sql = string.Format(sql, sSerialNumber);
            SqlDataReader sdr = ToolsClass.GetDataReader(sql);
            if (sdr != null && sdr.Read())
            {
                return false;
            }
            sdr.Close();
            sdr = null;
            return true;
        }

        public void batchGrade(string sn)
        {
            string remark = "";

            if (string.IsNullOrEmpty(comboBox2.Text.Trim()))
            {
                remark = comboBox1.Text.ToString() + " " + textBox1.Text.ToString();
            }
            else if (string.IsNullOrEmpty(comboBox1.Text.Trim()))
            {
                remark = comboBox2.Text.ToString() + " " + textBox2.Text.ToString();
            }
            else
            {
                remark = comboBox1.Text.ToString() + " " + textBox1.Text.ToString() + ". "
                + comboBox2.Text.ToString() + " " + textBox2.Text.ToString();
            }


            if (sn.Length != 13 && sn.Length != 14)
            {
                MessageBox.Show("Serial number " + sn + " does not match the correct length.");
                return;
            }

            if (!isPacked(sn))
            {
                MessageBox.Show("Module " + sn + " has already been packed.");
                return;
            }

            string sql = "INSERT INTO ReworkPerformed VALUES ('" + sn + "', '" + remark + "', '" + textBox3.Text.ToString() + "', getdate())";
            ToolsClass.ExecuteNonQuery(sql);

            sql = "INSERT INTO QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark) VALUES ('" + sn + "', '" + comboBox3.Text.ToString().Trim() + "', getdate(), '" + FormCover.CurrUserWorkID + "', '', '" + remark + "')";
            ToolsClass.ExecuteNonQuery(sql);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comboBox1.Text.Trim()) && string.IsNullOrEmpty(comboBox2.Text.Trim()))
            {
                MessageBox.Show("Please ensure that either the rework, quality or both sections are filled in.");
                return;
            }
            if (!string.IsNullOrEmpty(comboBox1.Text.Trim()) && string.IsNullOrEmpty(textBox1.Text.Trim()))
            {
                MessageBox.Show("The rework area needs a individuals name.");
                return;
            }
            if (!string.IsNullOrEmpty(comboBox2.Text.Trim()) && string.IsNullOrEmpty(textBox2.Text.Trim()))
            {
                MessageBox.Show("The quality area needs a individuals name.");
                return;
            }
            if (textBox1.Text.Trim().ToString() == textBox2.Text.Trim().ToString())
            {
                MessageBox.Show("The rework and quality personnel name cannot be the same.");
                return;
            }
            if (string.IsNullOrEmpty(textBox4.Text.Trim()))
            {
                MessageBox.Show("A serial number is required.");
                return;
            }
            if (string.IsNullOrEmpty(comboBox3.Text.Trim()))
            {
                MessageBox.Show("A module class is required.");
                return;
            }            

            string strTemp = textBox4.Text;
            string[] sDataSet = strTemp.Split('\n');
            for (int i = 0; i < sDataSet.Length; i++)
            {
                sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                if (!string.IsNullOrEmpty(sDataSet[i])){
                	if (!ToolsClass.GradeIfBFClass(sDataSet[i])){
						return;	                	
                	}
                	batchGrade(sDataSet[i]);
                }
            }
            textBox4.Clear();
            textBox3.Clear();
            comboBox1.SelectedItem = null;
            comboBox2.SelectedItem = null;
            comboBox3.SelectedItem = null;
            textBox2.Clear();
            textBox1.Clear();
        }
    }
}

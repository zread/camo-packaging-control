﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormRework : Form
    {
        public FormRework()
        {
            InitializeComponent();
        }

        private static FormRework theSingleton = null;

        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormRework();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                {
                    theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }

        private void cate1CB_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateCategory2();
        }
        public void PopulateCategory2()
        {
            cate2CB.Items.Clear();
            if (cate1CB.Text.ToString() == "B Class Rework")
            {
                cate2CB.Items.AddRange(new object[] {
                                    "Backsheet Scratch. Rwk Done. B Class",
                                    "Glass Scratch. Rwk Done. B Class",
                                    "MRB Review"});
            }
            else if (cate1CB.Text.ToString() == "Cleaning Rework")
            {
                this.cate2CB.Items.AddRange(new object[] {
                                    "Cleaning Issue. Cleaned. Rwk Done",
                                    "EVA on Glass.  Cleaned. Rwk Done",
                                    "Foreign Object. Removed. Rwk Done",
                                    "Silicon Cleaning Issue. Cleaned. Rwk Done",
                                    "Trimming Under Frame. Removed. Rwk Done"});
            }
            else if (cate1CB.Text.ToString() == "Frame Rework")
            {
                this.cate2CB.Items.AddRange(new object[] {
                                    "Cross Bar Missing. Installed. Rwk Done",
                                    "Cross Bar Scratch. Changed. Rwk Done",
                                    "Frame to Busbar <2mm. Reframed. Rwk Done",
                                    "Frame to Cell <2mm. Reframed. Rwk Done",
                                    "Long Frame Scratch. Changed. Rwk Done",
                                    "L Frame Scratch & Screw Damage. Rwk Done",
                                    "L & S Frame Scratch. Changed. Rwk Done",
                                    "Screw Damage. Frame Changed. Rwk Done",
                                    "Screw Damage. Screw Changed. Rwk Done",
                                    "Short Frame Scratch. Changed. Rwk Done",
                                    "S Frame Scratch & Screw Damage. Rwk Done",
            	                    "Frame Corner Gap <1mm, Easy Rwk Done",
            	                    "Frame Corner Gap >1mm, Hard Rwk Done"});
            }
            else if (cate1CB.Text.ToString() == "Jbox Rework")
            {
                this.cate2CB.Items.AddRange(new object[] {
                                    "Jbox Silicon Overfill. Cleaned. Rwk Done",
                                    "Jbox Silicon Underfill. Filled. Rwk Done",
                                    "Missing Jbox Lid. Installed. Rwk Done",
                                    "Uncured Jbox Silicon. Repotted. Rwk Done",
                                    "Unpotted Jbox Silicon. Potted. Rwk Done",
                                    "Unsoldered Busbars. Soldered. Rwk Done"});
            }
            else if (cate1CB.Text.ToString() == "Label/Barcode Rework")
            {
                this.cate2CB.Items.AddRange(new object[] {
                                    "Duplicate Barcode. Changed. Rwk Done",
                                    "Upside Down Power Label. Fixed. Rwk Done",
                                    "Wrong Power Label. Replaced. Rwk Done"});
            }
            else if (cate1CB.Text.ToString() == "Laminate Rework")
            {
                this.cate2CB.Items.AddRange(new object[] {
                                    "Foreign Object in Laminate. Removed. Rwk Done",
            	                            "Hot Plate Rework. Fixed. Rwk Done"});
            }
            else if (cate1CB.Text.ToString() == "No Rework")
            {
                this.cate2CB.Items.AddRange(new object[] {
                                    "No Defects Found",
                                    "Temporary A Class"});
            }
            else if (cate1CB.Text.ToString() == "Sorting")
            {
                this.cate2CB.Items.AddRange(new object[] {
                                    "Sort. Suspect Backsheet Scratch",
                                    "Sort. Suspect Frame Scratch",
                                    "Sort. Suspect Glass Scratch",
                                    "Sort. Suspect Incorrect Jbox",
                                    "Sort. Suspect Unsoldered Busbars"});
            }



        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cate1CB.Text))
                if (cate1CB.SelectedItem == null || cate2CB.SelectedItem == null)
                {
                    MessageBox.Show("Please ensure that both category boxes have been filled in.");
                    return;
                }

            if (string.IsNullOrEmpty(textBox3.Text.Trim()))
            {
                MessageBox.Show("A serial number is required.");
                return;
            }

            string strTemp = textBox3.Text;
            string[] sDataSet = strTemp.Split('\n');
            for (int i = 0; i < sDataSet.Length; i++)
            {
                sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
				if (!string.IsNullOrEmpty(sDataSet[i]))
					batchGrade(sDataSet[i]);
            }
            textBox3.Clear();
            cate1CB.SelectedItem = null;
            cate2CB.SelectedItem = null;
            cate2CB.Text = "";
            textBox1.Clear();
        }

        private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            sql = string.Format(sql, sSerialNumber);
            SqlDataReader sdr = ToolsClass.GetDataReader(sql);
            if (sdr != null && sdr.Read())
            {
                return false;
            }
            sdr.Close();
            sdr = null;
            return true;
        }

        public void batchGrade(string sn)
        {
            string category = "";
            category = cate2CB.Text.ToString();

            if (sn.Length != 13 && sn.Length != 14)
            {
                MessageBox.Show("Serial number " + sn + " does not match the correct length.");
                return;
            }

            if (!isPacked(sn))
            {
                MessageBox.Show("Module " + sn + " has already been packed.");
                return;
            }

            string sql = "INSERT INTO Rework (SN,Category,Comments,ReworkTime) Values ('" + sn + "', '" + category + "', '" + textBox1.Text.ToString() + "', getdate())";
            ToolsClass.ExecuteNonQuery(sql);

            sql = "INSERT INTO QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark) VALUES ('" + sn + "', 'A-', getdate(), '" + FormCover.CurrUserWorkID + "', '', '" + category + "')";
            ToolsClass.ExecuteNonQuery(sql);
        }
    }
}

# CAMO Packaging Control's Readme

### Program Created: 
Was originally created by China team and was modified and brought to Canada when the plant initially opened.

### Created By;
Originally China but all the old code is almost gone.

### Program Used By: 
Quality, Rework, Repair, EL1, EL2, Sta 370, Sta 390, Hoist and EVA stations

### Program's Purpose: 
Used to manage the class of serial numbers. Used to provide more traceability of a panel. Used to track rework progress. Used to track repair progress. Used to track defects. Used to log hoist and eva data.

### Deployed:
Deployed under: \\ca01s0015\AutoUpdate\updates. This program is setup such that when run it first checks for updates to be downloaded.

### Referneces:
None.

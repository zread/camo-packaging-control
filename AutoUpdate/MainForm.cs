﻿using System;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace CPRAutoUpdate
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            if (!updating())
            {
                lblUpdate.Text = "Update Unsuccessful, please contact administrator.";
            }
            else
            {
                lblUpdate.Text = "Update successful, Click OK to open program.";
            }
        }

        bool updating()
        {

            string CurrentFileLoc = Application.StartupPath.ToString();
            Version newVersion = null;
            string xmlurl = "";
            
            xmlurl = @"\\ca01s0015\AutoUpdate\update.xml";
            XmlTextReader reader = null;
            try
            {
                reader = new XmlTextReader(xmlurl);

                reader.MoveToContent();
                string elementName = "";
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "CSICPR"))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            elementName = reader.Name;
                        }
                        else
                        {
                            if ((reader.NodeType == XmlNodeType.Text) && (reader.HasValue))
                            {
                                switch (elementName)
                                {
                                    case "version":
                                        newVersion = new Version(reader.Value);
                                        break;                                    
                                    case "CSICRPexeconfig":
                                        File.Copy(reader.Value.ToString(), CurrentFileLoc + @"\ExcelManagement.exe.config", true);
                                        break;
                                    default:
                                        File.Copy(reader.Value.ToString(), CurrentFileLoc + @"\" + elementName.Substring(0, elementName.Length - 3) + "." + elementName.Substring(elementName.Length - 3, 3), true);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Exception caught: " + e.Message.ToString());
                return false;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return true;

        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("CSICPR.exe");
            Close();
            Application.Exit();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }
    }
}

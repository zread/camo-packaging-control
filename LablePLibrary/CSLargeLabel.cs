using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;


namespace LablePLibrary
{
    public delegate void ProgressHandler();

    /// <summary>
    /// 常熟组件厂区Excel竖包装大标签
    /// </summary>
    public class CSLargeLabel
    {
        private FileInfo template;
        private Application app;
        private object missing = Missing.Value;//用于调用带默认参数的方法
        public event ProgressHandler SavedEvent;
        private string sDirectory;
        private string sSourceFile;

        /// <summary>
        /// Initial
        /// </summary>
        /// <param name="sDirectory">Excel文件备份目录</param>
        /// <param name="sSourceFile">Excel模板文件</param>
        public CSLargeLabel(string sDirectory, string sSourceFile)
        {
            this.app = new Application();
            this.app.Visible = false;//设置Excel窗口不可见
            this.app.DisplayAlerts = false;//
            this.app.AlertBeforeOverwriting = false;//关闭修改之后是否保存
            if (sDirectory == "" || sDirectory == null)
            {
                this.sDirectory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory()) + "竖插包装";
            }
            else
                this.sDirectory = sDirectory;
            this.sSourceFile = sSourceFile;
        }


        public bool Write2Excel(int x, int y, LargeLabel2 largeLabel, string labelPath, String Market, string cert, int iSheet = 1)
        {
            bool isWriteOK = false;
            Workbook book = null;
            
            try
            {
//                string filePath = CopyFile(sDirectory, sSourceFile, largeLabel.CartonNumber);
				if (largeLabel.Market == "E" || largeLabel.Market == "U")
				{
					if(cert == "2"){
						book = app.Workbooks.Open(System.Windows.Forms.Application.StartupPath + "\\newlabel_US_No_VDE.xls", missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
					}
					else{
						book = app.Workbooks.Open(System.Windows.Forms.Application.StartupPath + "\\newlabel.xls", missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
					}
				}
				else if (largeLabel.Market == "C")
				{
					book = app.Workbooks.Open(labelPath, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
				}
				//else if (largeLabel.Market == "C")
				//{
				//	book = app.Workbooks.Open(System.Windows.Forms.Application.StartupPath + "\\newlabelCanadaIEC", missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
				//}
               	
				Worksheet sheet = book.Sheets[iSheet] as Worksheet;


                for (int i = 0; i < largeLabel.ListInfo.Count;i++ )
                {
                    sheet.Cells[x + i, y] = largeLabel.ListInfo[i][0];
                    sheet.Cells[x + i, y + 2] = largeLabel.ListInfo[i][1];
                    sheet.Cells[x + i, y + 3] = largeLabel.ListInfo[i][2];
                    sheet.Cells[x + i, y + 4] = largeLabel.ListInfo[i][3];
                    sheet.Cells[x + i, y + 5] = largeLabel.ListInfo[i][4];
                    sheet.Cells[x + i, y + 6] = largeLabel.ListInfo[i][5];
                }
				x = x + 2;
				sheet.Cells[x + 24, y] = largeLabel.Model.Insert(largeLabel.Model.IndexOf('-') + 1, largeLabel.Power);
                sheet.Cells[x + 24 + 1, y] = largeLabel.CellType;
                sheet.Cells[x + 24 + 2, y] = largeLabel.Size;
                sheet.Cells[x + 24 + 3, y] = largeLabel.Colour;
				sheet.Cells[x + 24 + 4, y] = largeLabel.CartonNumber;
				sheet.Cells[x + 24 + 5, y] = largeLabel.Jbox;
				sheet.Cells[x + 24 + 7, y] = largeLabel.NetWeight;
				sheet.Cells[x + 24 + 8, y] = largeLabel.GrossWeight;
				
				
				sheet.Cells[x + 24 + 13, y] = largeLabel.frame;
				sheet.Cells[x + 24 + 14, y] = largeLabel.Cable;
				sheet.Cells[x + 24 + 15, y] = largeLabel.Market;
				sheet.Cells[x + 24 + 16, y] = largeLabel.MaterialCode;
				sheet.Cells[x + 24 + 17, y] = largeLabel.BusBar; //Adding BusBar as Selections
				sheet.Cells[x + 24 + 18, y] = largeLabel.Quantity; // Quantity
				sheet.Cells[x + 24 + 19, y] = largeLabel.CellVendor;//Assembled in from -- Cells
				if(largeLabel.Model.Substring(0,4) == "CS6P"){
					sheet.Cells[x + 24 + 6, y] = sheet.Cells[x + 24 + 6, y + 1];
					//if(largeLabel.frame == "Black "){
					sheet.Cells[x + 24 + 11, y] = sheet.Cells[x + 24 + 11, y + 1];
					sheet.Cells[x + 24 + 9, y] = sheet.Cells[x + 24 + 9, y + 1];
					sheet.Cells[x + 24 + 10, y] = sheet.Cells[x + 24 + 10, y + 1];					
					sheet.Cells[x + 24 + 12, y] = sheet.Cells[x + 24 + 12, y + 1];
					//sheet.Cells[x + 24 + 14, y] = sheet.Cells[x + 24 + 14, y + 1];
				}
				else if(largeLabel.Model.Substring(0,4) == "CS6K"){
					sheet.Cells[x + 24 + 6, y] = sheet.Cells[x + 24 + 6, y + 3];
					sheet.Cells[x + 24 + 11, y] = sheet.Cells[x + 24 + 11, y + 3];
					sheet.Cells[x + 24 + 9, y] = sheet.Cells[x + 24 + 9, y + 3];
					sheet.Cells[x + 24 + 10, y] = sheet.Cells[x + 24 + 10, y + 3];					
					sheet.Cells[x + 24 + 12, y] = sheet.Cells[x + 24 + 12, y + 3];
					//sheet.Cells[x + 24 + 14, y] = sheet.Cells[x + 24 + 14, y + 1];
				}
				else{
					sheet.Cells[x + 24 + 6, y] = sheet.Cells[x + 24 + 6, y + 2];					
					sheet.Cells[x + 24 + 9, y] = sheet.Cells[x + 24 + 9, y + 2];
					sheet.Cells[x + 24 + 10, y] = sheet.Cells[x + 24 + 10, y + 2];
					sheet.Cells[x + 24 + 11, y] = sheet.Cells[x + 24 + 11, y + 2];
					sheet.Cells[x + 24 + 12, y] = sheet.Cells[x + 24 + 12, y + 2];
					//sheet.Cells[x + 24 + 14, y] = sheet.Cells[x + 24 + 14, y + 2];
				}				
				
                //app.Visible = true;
                //sheet.PrintPreview(false);
                sheet.PrintOut();
				//book.Save();
                app.Visible = false;
                app.Workbooks.Close();
                isWriteOK = true;
            }
            catch(Exception ex)
            {
                isWriteOK = false;
            }
            finally
            {
                this.app.Quit();//退出Excel，结束进程
                System.GC.Collect();//回收资源
            }
            return isWriteOK;
        }

        /// <summary>
        /// 复制Excel文件，放在备份目录下
        /// </summary>
        /// <param name="directory">目标目录</param>
        /// <param name="souceFileName">Excel源文件</param>
        /// <param name="sCartonNo">箱号</param>
        /// <returns>目标文件路径</returns>
        private string CopyFile(string directory,string souceFileName,string sCartonNo)
        {
            string destFileName = "";
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            destFileName = directory + "\\" + System.IO.Path.GetFileName(souceFileName).Insert(System.IO.Path.GetFileName(souceFileName).LastIndexOf('.'), "_" + sCartonNo);
            File.Copy(souceFileName, destFileName);
            return destFileName;
        }

    }
}
